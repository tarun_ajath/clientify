import React, { Component } from 'react';
import { View, Modal, Text, Dimensions } from 'react-native';
import InputBox from '../component/common/InputBox';
import { ScrollView } from 'react-native-gesture-handler';
import Container from '../component/common/Container';
import Header from '../component/common/Header';
import ButtonBox from '../component/common/ButtonBox';
import Spacer from '../component/common/Spacer';
import Icons from '../component/common/Icons';
import SplashScreen from 'react-native-splash-screen';
import { Dropdown } from 'react-native-material-dropdown';
import BackButtonHandler from '../utilities/BackButtonHandler';
import styles from '../component/common/styles';
import Picker_dropdown from '../component/common/Picker_dropdown';
import { connect } from 'react-redux';
import CompanyForm from '../component/CompanyForm';
import {
  addContactSubmit,
  updateAddContactState,
  addContactUpdate
} from '../Actions/addContactAction';

const { height } = Dimensions.get('window');
const { width } = Dimensions.get('window');

class AddNewContacts extends Component {
  constructor() {
    super();
    this.state = {
      language: 'Select a language',
      modalVisibleCompany: false
    };
  }
  firstArrayElement(ArrayElement, arrayType) {
    if (ArrayElement == null || ArrayElement == '') {
      // console.log('Obj is null', ArrayElement);
    } else {
      // console.log('val of elm is  ', ArrayElement);
      if (arrayType == 'first_name') {
        this.props.updateAddContactState('first_name', ArrayElement);
      } else if (arrayType == 'last_name') {
        this.props.updateAddContactState('last_name', ArrayElement);
      } else if (arrayType == 'emails') {
        this.props.updateAddContactState('email', ArrayElement[0].email);
      } else if (arrayType == 'phones') {
        this.props.updateAddContactState('phone', ArrayElement[0].phone);
      } else if (arrayType == 'title') {
        this.props.updateAddContactState('title', ArrayElement);
      } else if (arrayType == 'company') {
        this.props.updateAddContactState('company', ArrayElement);
      }
    }
  }
  componentDidMount() {
    const { ToastProvider } = this.props.screenProps;
    BackButtonHandler.mount(
      true,
      this.props.navigation,
      ToastProvider.showToast
    );
    const contactInfo = this.props.navigation.state.params.contactInfo;
    if (!this.props.navigation.state.params.isComeFromContact) {
      this.firstArrayElement(contactInfo.first_name, 'first_name');
      this.firstArrayElement(contactInfo.last_name, 'last_name');
      this.firstArrayElement(contactInfo.emails, 'emails');
      this.firstArrayElement(contactInfo.phones, 'phones');
      this.firstArrayElement(contactInfo.title, 'title');
      this.firstArrayElement(contactInfo.company, 'company');
    }
  }

  componentWillUnmount() {
    BackButtonHandler.unmount();
  }

  goToHome = () => {
    this.props.updateAddContactState('first_name', '');
    this.props.updateAddContactState('last_name', '');
    this.props.updateAddContactState('email', '');
    this.props.updateAddContactState('phone', '');
    this.props.updateAddContactState('title', '');
    this.props.updateAddContactState('company', []);

    this.props.navigation.navigate('ContactForm');
  };

  submitAddContactData = () => {
    const { ToastProvider } = this.props.screenProps;
    if (
      this.props.addContactState.first_name == '' ||
      this.props.addContactState.last_name == ''
      // || this.props.addContactState.email == '' ||
      // this.props.addContactState.phone == '' ||
      // this.props.addContactState.title == '' ||
      // this.props.addContactState.company == ''
    ) {
      alert('please fill all the details');
    } else {
      if (this.props.navigation.state.params.isComeFromContact) {
        const reqBody = {
          first_name: this.props.addContactState.first_name,
          last_name: this.props.addContactState.last_name,
          email: this.props.addContactState.email,
          phone: this.props.addContactState.phone,
          title: this.props.addContactState.title,
          company: this.props.addContactState.company.name
        };
        console.log('comp det', reqBody);
        this.props.addContactSubmit(reqBody, ToastProvider.showToast);
      } else if (!this.props.navigation.state.params.isComeFromContact) {
        const contactInfo = this.props.navigation.state.params.contactInfo;
        const reqBody = {
          first_name: this.props.addContactState.first_name,
          last_name: this.props.addContactState.last_name,
          email: this.props.addContactState.email,
          phone: this.props.addContactState.phone,
          title: this.props.addContactState.title,
          company: this.props.addContactState.company.name
        };
        this.props.updateAddContactState('first_name', '');
        this.props.updateAddContactState('last_name', '');
        this.props.updateAddContactState('email', '');
        this.props.updateAddContactState('phone', '');
        this.props.updateAddContactState('title', '');
        this.props.updateAddContactState('company', []);

        this.props.addContactUpdate(
          reqBody,
          contactInfo.id,
          ToastProvider.showToast
        );
      }
    }
  };

  closeModelFromCompanyModel = selectedCompanyDetalis => {
    this.setState({ modalVisibleCompany: false });
    this.props.updateAddContactState('company', selectedCompanyDetalis);
  };

  render() {
    return (
      <Container>
        <View style={{ flex: 1 }}>
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisibleCompany}
            onRequestClose={() => {
              this.setState({ modalVisibleCompany: false });
            }}
            BackButtonHandler={true}
          >
            <Header
              titleName="Company Modal"
              leftTitleImg={Icons.notification_light()}
              RightTitleImg={Icons.notification_light()}
            />
            <View style={{ flex: 9 }}>
              <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                <View style={{ flex: 9, paddingTop: 10, flexDirection: 'row' }}>
                  <CompanyForm
                    isComeFromModel={false}
                    isComeFromAddContact={true}
                    closeModel={this.closeModelFromCompanyModel}
                  />
                </View>
              </ScrollView>
            </View>
          </Modal>
          <Header
            titleName={
              this.props.navigation.state.params.isComeFromContact == false
                ? 'Update Contact'
                : 'Add New Contact'
            }
            leftTitleImg={Icons.profile_back}
            RightTitleImg={Icons.notification_light()}
            onPressBack={this.goToHome}
          />
          <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <View
              style={{
                flex: 8,
                marginTop: 15
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Regular,
                      {
                        color: '#000',
                        alignItems: 'center',
                        fontSize: 14
                      }
                    ]}
                  >
                    First Name
                  </Text>
                </View>
                <View
                  style={{
                    flex: 2,
                    height: 40,
                    padding: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginLeft: 15
                  }}
                >
                  <InputBox
                    placeholder="First Name"
                    value={this.props.addContactState.first_name}
                    name="first_name"
                    updateLocalState={this.props.updateAddContactState}
                  />
                </View>
              </View>
              <Spacer />
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Regular,
                      {
                        color: '#000',
                        alignItems: 'center',
                        fontSize: 14
                      }
                    ]}
                  >
                    Last Name
                  </Text>
                </View>
                <View
                  style={{
                    flex: 2,
                    height: 40,
                    padding: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginLeft: 15
                  }}
                >
                  <InputBox
                    placeholder="Enter Last Name"
                    value={this.props.addContactState.last_name}
                    name="last_name"
                    updateLocalState={this.props.updateAddContactState}
                  />
                </View>
              </View>
              <Spacer />
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Regular,
                      {
                        color: '#000',
                        alignItems: 'center',
                        fontSize: 14
                      }
                    ]}
                  >
                    Email ID
                  </Text>
                </View>
                <View
                  style={{
                    flex: 2,
                    height: 40,
                    padding: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginLeft: 15
                  }}
                >
                  <InputBox
                    placeholder="Enter Email Name"
                    value={this.props.addContactState.email}
                    name="email"
                    updateLocalState={this.props.updateAddContactState}
                  />
                </View>
              </View>
              <Spacer />

              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Regular,
                      {
                        color: '#000',
                        alignItems: 'center',
                        fontSize: 14
                      }
                    ]}
                  >
                    Phone no.
                  </Text>
                </View>
                <View
                  style={{
                    flex: 2,
                    height: 40,
                    padding: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginLeft: 15
                    // backgroundColor: 'green'
                  }}
                >
                  <InputBox
                    placeholder="Enter Phone no."
                    value={this.props.addContactState.phone}
                    name="phone"
                    updateLocalState={this.props.updateAddContactState}
                  />
                </View>
              </View>
              <Spacer />
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Regular,
                      {
                        color: '#000',
                        alignItems: 'center',
                        fontSize: 14
                      }
                    ]}
                  >
                    Title
                  </Text>
                </View>
                <View
                  style={{
                    flex: 2,
                    height: 40,
                    padding: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginLeft: 15
                  }}
                >
                  <InputBox
                    placeholder="Enter Title"
                    value={this.props.addContactState.title}
                    name="title"
                    updateLocalState={this.props.updateAddContactState}
                  />
                </View>
              </View>
              <Spacer />
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Regular,
                      {
                        color: '#000',
                        alignItems: 'center',
                        fontSize: 14
                      }
                    ]}
                  >
                    Company
                  </Text>
                </View>
                <View
                  onStartShouldSetResponder={() => {
                    this.setState({
                      modalVisibleCompany: true
                    });
                  }}
                  style={{
                    flex: 2,
                    height: 40,
                    padding: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginLeft: 15,
                    borderColor: 'grey',
                    borderWidth: 2,
                    borderRadius: 30
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Bold,
                      styles.textSize14,
                      {
                        color: '#7f7f7f',
                        alignItems: 'center'
                      }
                    ]}
                  >
                    {this.props.addContactState.company == [] ||
                    this.props.addContactState.company == ''
                      ? 'select here'
                      : this.props.addContactState.company.name}
                  </Text>
                </View>
              </View>

              <Spacer size={20} />

              <Spacer />
              <View
                style={{
                  marginRight: 15,
                  marginLeft: 15,
                  flexDirection: 'row'
                }}
              >
                <View
                  style={{
                    flex: 1,
                    marginRight: 5,
                    borderRadius: 30,
                    borderWidth: 1,
                    borderColor: 'grey'
                  }}
                >
                  <ButtonBox
                    btnText="Cancel"
                    btnColor="white"
                    btnTextColor="#000"
                    onPress={this.goToHome}
                  />
                </View>
                <View style={{ flex: 1, marginLeft: 5 }}>
                  <ButtonBox
                    btnText="Save "
                    btnColor="#f5921e"
                    btnTextColor="#fff"
                    onPress={this.submitAddContactData}
                  />
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return state;
};
const mapDispatchToProps = {
  addContactSubmit,
  updateAddContactState,
  addContactUpdate
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddNewContacts);
