import React, { Component, Fragment } from 'react';
import {
  View,
  StyleSheet,
  Text,
  Dimensions,
  Image,
  FlatList,
  ActivityIndicator,
  TouchableOpacity
} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import { BottomNavigation, FAB, Portal } from 'react-native-paper';
import Container from '../component/common/Container';
import InputBox from '../component/common/InputBox';
import CompanyForm from '../component/CompanyForm';
import Header from '../component/common/Header';
import Icons from '../component/common/Icons';
import styless from '../component/common/styles';
import SplashScreen from 'react-native-splash-screen';
import { connect } from 'react-redux';
import BackButtonHandler from '../utilities/BackButtonHandler';
import HttpClient from '../utilities/HttpClient';
import {
  getContactList,
  updateContactState
} from '../Actions/contactFormAction';
import {
  getProfileDetails,
  updateProfileState
} from '../Actions/profileScreenAction';
let contactListPageNo = 1;
class ContactForm extends Component {
  constructor() {
    super();
    this.state = {
      open: false,
      tabPosition: 1,
      showFab: false,
      refresh: false,
      searchContactResult: []
    };
  }
  componentDidMount() {
    SplashScreen.hide();
    this.props.getContactList(
      contactListPageNo,
      this.props.contactFormState.contactListArray
    );
    this.props.navigation.addListener('didFocus', () =>
      this.setState({
        showFab: true
      })
    );
    this.props.navigation.addListener('willBlur', () =>
      this.setState({
        showFab: false
      })
    );
    const { ToastProvider } = this.props.screenProps;
    BackButtonHandler.mount(
      false,
      this.props.navigation,
      ToastProvider.showToast
    );
  }
  componentWillUnmount() {
    BackButtonHandler.unmount();
  }

  searchContact = async text => {
    try {
      let res = await HttpClient.contactSearch(text);
      this.setState({
        searchContactResult: res.data.results
      });
    } catch (error) {
      console.log(error.response);
    }
  };

  nextPageData = async () => {
    contactListPageNo = contactListPageNo + 1;
    // alert('comes to end' + contactListPageNo);
    this.props.getContactList(
      contactListPageNo,
      this.props.contactFormState.contactListArray
    );
  };

  checkForImageOrFirstLetterName(first_Name, last_name, picture_url, i) {
    if (picture_url == null && (first_Name == '' || first_Name == null)) {
    } else if (picture_url == null) {
      const first = this.getFirstLetterOfContact(first_Name);
      const last = this.getFirstLetterOfContact(last_name);
      return (
        <View>
          <Text
            style={[
              styles.fontPrimary_monsMedium,
              { color: '#fff', fontSize: 17 }
            ]}
          >
            {first.substr(0, 1) + '' + last.substr(0, 1)}
          </Text>
        </View>
      );
    } else {
      const { contactFormState } = this.props;

      return (
        <View>
          <Image
            style={{ width: 50, height: 50, borderRadius: 25 }}
            resizeMode={'contain'}
            source={{
              uri: picture_url
            }}
            onError={() => {
              let newArr = [...contactFormState.contactListArray];
              newArr[i].picture_url = null;
              updateContactState('contactListArray', newArr);
              setTimeout(() => this.setState({ refresh: true }), 1000);
            }}
          />
        </View>
      );
    }
  }

  splittedName = '';
  getFirstLetterOfContact(Name) {
    this.splittedName = Name.split(/\W/)
      .map(item => {
        for (var i = 0; i < item.length; i++) {
          return item[i];
        }
      })
      .join('');
    firstLetterOfName = this.splittedName;
    return firstLetterOfName;
  }

  isEmailNull(emailArray) {
    if (emailArray == null) {
      return;
    } else {
      return (
        <Text
          style={[
            styless.fontPrimary_Bold,
            styless.textSize14,
            { color: '#7f7f7f' }
          ]}
        >
          {emailArray.email}
        </Text>
      );
    }
  }
  contactLists(item, i) {
    const {
      contactFormState,
      profileScreenState,
      updateProfileState
    } = this.props;
    return (
      <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 10 }}>
        <View style={{ flex: 2, alignContent: 'center', padding: 5 }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#00b2e2',
              height: 50,
              width: 50,
              borderRadius: 25
            }}
          >
            {this.checkForImageOrFirstLetterName(
              item.first_name,
              item.last_name,
              item.picture_url,
              i
            )}
          </View>
        </View>
        <View style={{ flex: 10, alignContent: 'center', padding: 5 }}>
          <TouchableOpacity
            onPress={() => {
              this.props.getProfileDetails(item.id);
            }}
          >
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'flex-start',
                height: 50
              }}
            >
              <Text
                style={[
                  styless.fontPrimary_monsMedium,
                  styless.textSize16,
                  { color: '#000' }
                ]}
                numberOfLines={1}
              >
                {item.first_name + ' ' + item.last_name}
              </Text>

              {this.isEmailNull(item.emails[0])}
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  LoadingContactListData() {
    const { contactFormState } = this.props;
    // if (contactFormState.isLoading) {
    //   return (
    //     <View
    //       style={{
    //         justifyContent: 'center',
    //         alignItems: 'center',
    //         flex: 1,
    //         alignSelf: 'center'
    //       }}
    //     >
    //       <ActivityIndicator size="large" color="#00b2e2" animating={true} />
    //     </View>
    //   );
    // } else {
    if (
      contactFormState.contactTextInput == '' ||
      !contactFormState.contactTextInput.trim().length
    ) {
      return (
        <Fragment>
          <FlatList
            style={{ flex: 0.9 }}
            data={contactFormState.contactListArray}
            extraData={this.state.refresh}
            renderItem={({ item, index }) => this.contactLists(item, index)}
            keyExtractor={(item, index) => index.toString()}
            onEndReached={({ distanceFromEnd }) => {
              this.nextPageData();
            }}
          />
          {this.props.contactFormState.isLoading == true ? (
            <View
              style={{
                height: 50,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <ActivityIndicator
                size="small"
                color="#00b2e2"
                animating={true}
              />
            </View>
          ) : (
            <Fragment />
          )}
        </Fragment>
      );
    } else {
      // let updatedContactList = contactFormState.contactListArray.filter(
      //   item => {
      //     return item['first_name']
      //       .toLowerCase()
      //       .includes(contactFormState.contactTextInput.trim().toLowerCase());
      //   }
      // );
      return (
        <FlatList
          data={this.state.searchContactResult}
          renderItem={({ item, index }) => this.contactLists(item, index)}
          keyExtractor={(item, index) => index.toString()}
        />
      );
      // }
    }
  }

  returnFun = () => {
    const { contactFormState } = this.props;

    if (this.state.tabPosition == 1) {
      return (
        <View style={{ flex: 9 }}>
          <View
            style={{
              height: 55,
              marginLeft: 25,
              marginRight: 25,
              paddingTop: 10,
              paddingBottom: 5
            }}
          >
            {this.fabFunctionContact()}
            <InputBox
              rightImageName={Icons.search}
              placeholder="Search by Contact Name"
              value={contactFormState.contactTextInput}
              name="contactTextInput"
              updateLocalState={this.props.updateContactState}
              onTextChangeMethod={this.searchContact}
            />
          </View>
          {/* <ScrollView contentContainerStyle={{ flexGrow: 1 }}> */}
          <View style={{ flex: 9, paddingTop: 10 }}>
            {this.LoadingContactListData()}
          </View>
          {/* </ScrollView> */}
        </View>
      );
    } else if (this.state.tabPosition == 2) {
      return (
        <View style={{ flex: 1 }}>
          {this.fabFunctionCompany()}
          <CompanyForm
            isComeFromModel={false}
            navigation={this.props.navigation}
          />
        </View>
      );
    }
  };

  fabFunctionCompany = () => {
    if (this.state.showFab) {
      return (
        <Portal>
          <FAB.Group
            color={'white'}
            style={{ marginBottom: 55 }}
            open={this.state.open}
            icon={this.state.open ? 'close' : 'add'}
            actions={[
              {
                icon: Icons.add_company_yellow,
                label: 'Add new Company',
                onPress: () => {
                  this.props.navigation.navigate('AddNewCompany', {
                    isComeFromCompany: true
                  });
                }
              }
            ]}
            onStateChange={({ open }) => this.setState({ open })}
            onPress={() => {
              if (this.state.open) {
                // do something if the speed dial is open
                // alert('Helllooooo');
              }
            }}
          />
        </Portal>
      );
    }
  };
  fabFunctionContact = () => {
    if (this.state.showFab) {
      return (
        <Portal>
          <FAB.Group
            color={'white'}
            style={{ paddingBottom: 70 }}
            open={this.state.open}
            icon={this.state.open ? 'close' : 'add'}
            actions={[
              {
                icon: Icons.contact_customer_yellow,
                label: 'Import Contacts',
                onPress: () => {
                  this.props.navigation.navigate('UserContactList');
                }
              },
              {
                icon: Icons.contact_customer_yellow,
                label: 'Add new Contacts',
                onPress: () => {
                  this.props.navigation.navigate('AddNewContacts', {
                    isComeFromContact: true
                  });
                }
              }
            ]}
            onStateChange={({ open }) => this.setState({ open })}
            onPress={() => {
              if (this.state.open) {
                // do something if the speed dial is open
                // alert('Helllooooo');
              }
            }}
          />
        </Portal>
      );
    }
  };

  render() {
    const { contactFormState, profileScreenState } = this.props;
    // console.log(
    //   'Hurrrrr    ' + JSON.stringify(contactFormState.contactListArray)
    // );

    // console.log(
    //   ' profile scree Purrrrr    ' +
    //     JSON.stringify(profileScreenState.profileListArray)
    // );
    return (
      <Container>
        <View style={{ flex: 1 }}>
          <Header
            titleName="Homescreen"
            leftTitleImg={Icons.notification_light()}
            RightTitleImg={Icons.notification_light}
          />
          <View style={{ height: 45, flexDirection: 'row' }}>
            <NavigationEvents
              onWillFocus={() => {
                contactListPageNo = 1;
                this.props.updateContactState('contactListArray', []);
                console.log('props on contact', this.props);
                setTimeout(() => {
                  this.props.getContactList(
                    contactListPageNo,
                    this.props.contactFormState.contactListArray
                  );
                }, 100);
              }}
            />
            <View
              onStartShouldSetResponder={() => {
                this.setState({
                  tabPosition: 1
                });
              }}
              style={
                this.state.tabPosition == 1 ? styles.btnActive : styles.btn
              }
            >
              <Text
                style={
                  this.state.tabPosition == 1
                    ? styles.textBtnActive
                    : styles.textBtn
                }
              >
                {' '}
                Contacts
              </Text>
            </View>
            <View
              onStartShouldSetResponder={() => {
                this.setState({
                  tabPosition: 2
                });
              }}
              style={
                this.state.tabPosition == 2 ? styles.btnActive : styles.btn
              }
            >
              <Text
                style={
                  this.state.tabPosition == 2
                    ? styles.textBtnActive
                    : styles.textBtn
                }
              >
                {' '}
                Company
              </Text>
            </View>
          </View>
          <View style={{ flex: 9 }}>
            <View style={{ flex: 9 }}>{this.returnFun()}</View>
          </View>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = {
  getContactList,
  getProfileDetails,
  updateContactState,
  updateProfileState
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContactForm);

const styles = StyleSheet.create({
  textBtn: {
    color: 'grey',
    fontSize: 13,
    fontFamily: 'Montserrat-Medium'
  },
  textBtnActive: {
    color: '#ffff',
    fontSize: 13,
    fontFamily: 'Montserrat-Medium'
  },
  btnActive: {
    flex: 1,
    backgroundColor: '#00b2e2',
    justifyContent: 'center',
    alignItems: 'center'
  },
  btn: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center'
  }
});

// MD5:  44:26:73:3D:C7:BE:A8:FA:19:73:60:04:46:99:82:3E
// 	 SHA1: 6C:FA:A7:AF:A8:00:71:BC:E3:97:20:AF:B2:31:F3:02:BF:40:4C:0D
// 	 SHA256: DD:58:BF:82:C1:76:87:49:47:F0:EC:B9:70:FB:04:43:60:20:E3:49:3B:CF:92:95:3F:C2:35:75:05:28:68:68

// "token": "b8e880393fa670f9598f08468fc13929c3b910a2"
