import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  Dimensions,
  ImageBackground,
  Modal
} from 'react-native';
import InputBox from '../component/common/InputBox';
import { ScrollView } from 'react-native-gesture-handler';
import Container from '../component/common/Container';
import Header from '../component/common/Header';
import ButtonBox from '../component/common/ButtonBox';
import Spacer from '../component/common/Spacer';
import Icons from '../component/common/Icons';
import BackButtonHandler from '../utilities/BackButtonHandler';
import styles from '../component/common/styles';
import Picker_dropdown from '../component/common/Picker_dropdown';
import { connect } from 'react-redux';
import {
  addCompanySubmit,
  updateAddCompanyState,
  addCompanyUpdate
} from '../Actions/addCompanyAction';
import CompanySectorList from '../component/CompanySectorList';

const { height } = Dimensions.get('window');

class AddNewCompany extends Component {
  constructor() {
    super();
    this.state = {
      modalVisibleCompanySector: false
    };
  }

  firstArrayElement(ArrayElement, arrayType) {
    if (ArrayElement == null || ArrayElement == '') {
      // console.log('Obj is null', ArrayElement);
    } else {
      // console.log('val of elm is  ', ArrayElement);
      if (arrayType == 'name') {
        this.props.updateAddCompanyState('name', ArrayElement);
      } else if (arrayType == 'company_sector') {
        this.props.updateAddCompanyState(
          'selectedCompanySectorDetails',
          ArrayElement
        );
      } else if (arrayType == 'phones') {
        this.props.updateAddCompanyState('phones', ArrayElement[0].phone);
      } else if (arrayType == 'websites') {
        this.props.updateAddCompanyState('websites', ArrayElement[0].website);
      }
    }
  }

  componentDidMount() {
    const { ToastProvider } = this.props.screenProps;
    BackButtonHandler.mount(
      true,
      this.props.navigation,
      ToastProvider.showToast
    );

    const companyInfo = this.props.navigation.state.params.companyInfo;
    if (!this.props.navigation.state.params.isComeFromCompany) {
      this.firstArrayElement(companyInfo.name, 'name');
      this.firstArrayElement(companyInfo.company_sector, 'company_sector');
      this.firstArrayElement(companyInfo.phones, 'phones');
      this.firstArrayElement(companyInfo.websites, 'websites');
    }
  }

  componentWillUnmount() {
    BackButtonHandler.unmount();
  }

  goToHome = () => {
    this.props.navigation.navigate('ContactForm');
  };

  closeModelFromCompanySectorModel = selectedCompanyDetalis => {
    this.setState({ modalVisibleCompanySector: false });
    this.props.updateAddCompanyState(
      'selectedCompanySectorDetails',
      selectedCompanyDetalis.name
    );
  };

  submitAddCompany = () => {
    const { ToastProvider } = this.props.screenProps;
    if (
      this.props.addCompanyState.name == '' ||
      this.props.addCompanyState.selectedCompanySectorDetails == '' ||
      this.props.addCompanyState.phones == '' ||
      this.props.addCompanyState.websites == ''
    ) {
      alert('please fill all the details');
    } else {
      if (!this.props.navigation.state.params.isComeFromCompany) {
        // alert(
        //   'in update company' + this.props.navigation.state.params.companyInfo
        // );
        const reqBody = {
          name: this.props.addCompanyState.name,
          company_sector: this.props.addCompanyState
            .selectedCompanySectorDetails.name,
          phones: [{ phone: this.props.addCompanyState.phones }],
          websites: [{ website: this.props.addCompanyState.websites }]
        };
        this.props.addCompanyUpdate(
          reqBody,
          this.props.navigation.state.params.companyInfo.id
        );
      } else {
        alert('in add company');
        const reqBody = {
          name: this.props.addCompanyState.name,
          company_sector: this.props.addCompanyState
            .selectedCompanySectorDetails.name,
          phones: [{ phone: this.props.addCompanyState.phones }],
          websites: [{ website: this.props.addCompanyState.websites }]
        };
        this.props.addCompanySubmit(reqBody, ToastProvider.showToast);
      }
    }
  };

  render() {
    console.log('add company props', this.props);
    return (
      <Container>
        <View style={{ flex: 1 }}>
          <Header
            titleName="Add New Company"
            leftTitleImg={Icons.profile_back}
            RightTitleImg={Icons.notification_light()}
            onPressBack={this.goToHome}
          />
          <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisibleCompanySector}
              onRequestClose={() => {
                this.setState({ modalVisibleCompanySector: false });
              }}
              BackButtonHandler={true}
            >
              <Header
                titleName="Company Sector Modal"
                leftTitleImg={Icons.notification_light()}
                RightTitleImg={Icons.notification_light()}
              />
              <View style={{ flex: 9 }}>
                <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                  <View
                    style={{ flex: 9, paddingTop: 10, flexDirection: 'row' }}
                  >
                    <CompanySectorList
                      isComeFromModel={true}
                      closeModel={this.closeModelFromCompanySectorModel}
                    />
                  </View>
                </ScrollView>
              </View>
            </Modal>
            <View
              style={{
                flex: 8,
                height: '85%',
                marginTop: 15
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 5
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Regular,
                      {
                        color: '#000',
                        alignItems: 'center',
                        fontSize: 14
                      }
                    ]}
                  >
                    Company Name
                  </Text>
                </View>
                <View
                  style={{
                    flex: 2,
                    height: 40,
                    padding: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginLeft: 15
                  }}
                >
                  <InputBox
                    placeholder="Enter Company Name"
                    value={this.props.addCompanyState.name}
                    name="name"
                    updateLocalState={this.props.updateAddCompanyState}
                  />
                </View>
              </View>
              <Spacer />
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Regular,
                      {
                        color: '#000',
                        alignItems: 'center',
                        fontSize: 14
                      }
                    ]}
                  >
                    Sector
                  </Text>
                </View>
                <View
                  onStartShouldSetResponder={() => {
                    this.setState({
                      modalVisibleCompanySector: true
                    });
                  }}
                  style={{
                    flex: 2,
                    height: 40,
                    padding: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginLeft: 15,
                    borderColor: 'grey',
                    borderWidth: 2,
                    borderRadius: 30
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Bold,
                      styles.textSize14,
                      {
                        color: '#7f7f7f',
                        alignItems: 'center'
                      }
                    ]}
                  >
                    {this.props.addCompanyState.selectedCompanySectorDetails ==
                      '' ||
                    this.props.addCompanyState.selectedCompanySectorDetails ==
                      null
                      ? 'select here'
                      : this.props.addCompanyState.selectedCompanySectorDetails}
                  </Text>
                </View>
              </View>
              <Spacer />
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Regular,
                      {
                        color: '#000',
                        alignItems: 'center',
                        fontSize: 14
                      }
                    ]}
                  >
                    Phone No.
                  </Text>
                </View>
                <View
                  style={{
                    flex: 2,
                    height: 40,
                    padding: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginLeft: 15
                  }}
                >
                  <InputBox
                    placeholder="Enter phone no."
                    value={this.props.addCompanyState.phones}
                    name="phones"
                    updateLocalState={this.props.updateAddCompanyState}
                  />
                </View>
              </View>
              <Spacer size={10} />
              <View
                style={{
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Regular,
                      {
                        color: '#000',
                        alignItems: 'center',
                        fontSize: 14
                      }
                    ]}
                  >
                    Add Company Website
                  </Text>
                </View>
                <Spacer size={3} />
                <View
                  style={{
                    flex: 2,
                    height: 40,
                    padding: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginLeft: 15,
                    marginTop: 5
                  }}
                >
                  <InputBox
                    placeholder="http://"
                    value={this.props.addCompanyState.websites}
                    name="websites"
                    updateLocalState={this.props.updateAddCompanyState}
                  />
                </View>
              </View>

              <Spacer size={20} />

              <Spacer size={15} />
            </View>

            <View style={{ height: '15%' }}>
              <View
                style={{
                  marginRight: 15,
                  marginLeft: 15,
                  flexDirection: 'row'
                }}
              >
                <View
                  style={{
                    flex: 1,
                    marginRight: 5,
                    borderRadius: 30,
                    borderWidth: 1,
                    borderColor: 'grey'
                  }}
                >
                  <ButtonBox
                    btnText="Cancel"
                    btnColor="white"
                    btnTextColor="#000"
                    onPress={this.goToHome}
                  />
                </View>
                <View style={{ flex: 1, marginLeft: 5 }}>
                  <ButtonBox
                    btnText="Save "
                    btnColor="#f5921e"
                    onPress={this.submitAddCompany}
                  />
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return state;
};
const mapDispatchToProps = {
  addCompanySubmit,
  updateAddCompanyState,
  addCompanyUpdate
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddNewCompany);
