import React, { Component } from 'react';
import {
  View,
  TextInput,
  Text,
  Dimensions,
  ImageBackground
} from 'react-native';
import InputBox from '../component/common/InputBox';
import { ScrollView } from 'react-native-gesture-handler';
import Container from '../component/common/Container';
import Header from '../component/common/Header';
import ButtonBox from '../component/common/ButtonBox';
import Spacer from '../component/common/Spacer';
import Icons from '../component/common/Icons';
import { connect } from 'react-redux';
import { addNoteSubmit, updateAddNoteState } from '../Actions/addNoteAction';

const { height } = Dimensions.get('window');

class AddNoteScreen extends Component {
  goToHome = () => {
    const { navigation } = this.props;
    const isComeFrom = navigation.getParam('isComeFrom');
    if (isComeFrom == 1) {
      this.props.navigation.navigate('Profile');
    } else if (isComeFrom == 2) {
      this.props.navigation.navigate('DealsDetailsScreen');
    } else if (isComeFrom == 3) {
      this.props.navigation.navigate('ActivitiesForm');
    }
  };

  submitAddNoteDetails = () => {
    const { navigation } = this.props;
    const isComeFrom = navigation.getParam('isComeFrom');
    let relatedId = navigation.getParam('Id');
    const { ToastProvider } = this.props.screenProps;
    const { updateAddNoteState } = this.props;
    if (
      this.props.AddNoteState.name == '' ||
      this.props.AddNoteState.comment == ''
    ) {
      alert('please fill all the details');
    } else {
      const reqBody = {
        name: this.props.AddNoteState.name,
        comment: this.props.AddNoteState.comment
      };
      if (isComeFrom == 1) {
        this.props.addNoteSubmit(
          relatedId,
          reqBody,
          ToastProvider.showToast,
          true
        );
      } else if (isComeFrom == 2) {
        this.props.addNoteSubmit(
          relatedId,
          reqBody,
          ToastProvider.showToast,
          false
        );
      }

      updateAddNoteState('name', '');
      updateAddNoteState('comment', '');
    }
  };

  render() {
    return (
      <Container>
        <Header
          titleName="Add Notes"
          leftTitleImg={Icons.profile_back}
          RightTitleImg={Icons.notification_light()}
          onPressBack={this.goToHome}
        />

        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <View style={{ flex: 1, height: '85%', padding: 20 }}>
            <Text> Full Name</Text>
            <InputBox
              placeholder="Enter Name"
              value={this.props.AddNoteState.name}
              name="name"
              updateLocalState={this.props.updateAddNoteState}
            />
            <Spacer />
            <Text> Comment </Text>
            <TextInput
              multiline={true}
              style={{
                marginTop: 10,
                borderWidth: 2,
                borderColor: 'grey',
                height: height / 2,
                textAlignVertical: 'top',
                padding: 15
              }}
              onChangeText={text => {
                this.props.updateAddNoteState('comment', text);
              }}
            />

            <Spacer size={15} />
          </View>
          <View style={{ height: '85%' }}>
            <View
              style={{
                marginRight: 15,
                marginLeft: 15,
                flexDirection: 'row'
              }}
            >
              <View
                style={{
                  flex: 1,
                  marginRight: 5,
                  borderRadius: 30,
                  borderWidth: 1,
                  borderColor: 'grey'
                }}
              >
                <ButtonBox
                  btnText="Cancel"
                  btnColor="white"
                  btnTextColor="#000"
                  onPress={this.goToHome}
                />
              </View>
              <View style={{ flex: 1, marginLeft: 5 }}>
                <ButtonBox
                  btnText="Save "
                  btnColor="#f5921e"
                  btnTextColor="#fff"
                  onPress={this.submitAddNoteDetails}
                />
              </View>
            </View>
          </View>
          <Spacer size={15} />
        </ScrollView>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return state;
};
const mapDispatchToProps = {
  addNoteSubmit,
  updateAddNoteState
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddNoteScreen);
