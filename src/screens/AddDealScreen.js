import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  Dimensions,
  ImageBackground,
  Modal,
  ActivityIndicator,
  FlatList,
  Image
} from 'react-native';
import InputBox from '../component/common/InputBox';
import { ScrollView } from 'react-native-gesture-handler';
import Container from '../component/common/Container';
import Header from '../component/common/Header';
import ButtonBox from '../component/common/ButtonBox';
import Spacer from '../component/common/Spacer';
import BackButtonHandler from '../utilities/BackButtonHandler';
import Icons from '../component/common/Icons';
import styles from '../component/common/styles';
import Picker_dropdown from '../component/common/Picker_dropdown';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { connect } from 'react-redux';
import {
  addDealDataSubmit,
  updateAddDeaState,
  addDealUpdateSubmit
} from '../Actions/addDealAction';
import {
  getContactList,
  updateContactState
} from '../Actions/contactFormAction';
import CompanyForm from '../component/CompanyForm';
import PipelineForm from '../component/PipelineForm';
import PipelineStageForm from '../component/PipelineStageForm';
import HttpClient from '../utilities/HttpClient';
months = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sept',
  'Oct',
  'Nov',
  'Dec'
];

const { height } = Dimensions.get('window');

class AddDealScreen extends Component {
  constructor() {
    super();
    this.state = {
      expectedCloseDate: 'YYYY-MM-DD',
      expectedcloseDateVisible: false,
      modalVisible: false,
      modalVisibleCompany: false,
      modalVisiblePipeline: false,
      modalVisiblePipelineStage: false
    };
  }
  getContactDetails = async ArrayElement => {
    const contactId = ArrayElement.split('/?')[0].split('contacts/')[1];
    try {
      const resp = await HttpClient.contactProfileData(contactId);
      this.props.updateAddDeaState('selectedContactDetails', resp.data);
    } catch {
      // console.log('contactdeta error', error.response);
    }
  };
  getCompanyDetails = async ArrayElement => {
    const companyId = ArrayElement.split('/?')[0].split('companies/')[1];
    try {
      const resp = await HttpClient.companyDetails(companyId);
      this.props.updateAddDeaState('selectedCompanyDetalis', resp.data);
    } catch {
      // console.log('companydeta error', error.response);
    }
  };
  getPipelineDetails = async ArrayElement => {
    const pipelineId = ArrayElement.split('/?')[0].split('pipelines/')[1];
    try {
      const resp = await HttpClient.userPipelineById(pipelineId);
      this.props.updateAddDeaState('selectedPipelineDetalis', resp.data);
    } catch {
      // console.log('contactdeta error', error.response);
    }
  };

  getPipelineStagesDetails = async ArrayElement => {
    const pipelineStagesId = ArrayElement.split('/?')[0].split(
      'pipelines/stages/'
    )[1];
    try {
      const resp = await HttpClient.userPipelineStageById(pipelineStagesId);
      this.props.updateAddDeaState('selectedPipelineStageDetalis', resp.data);
    } catch {
      // console.log('contactdeta error', error.response);
    }
  };

  firstArrayElement = async (ArrayElement, arrayType) => {
    if (ArrayElement == null || ArrayElement == '') {
      // console.log('Obj is null', ArrayElement);
    } else {
      // console.log('val of elm is  ', ArrayElement);
      if (arrayType == 'name') {
        this.props.updateAddDeaState('name', ArrayElement);
      } else if (arrayType == 'amount') {
        this.props.updateAddDeaState('amount', ArrayElement);
      } else if (arrayType == 'contact') {
        this.getContactDetails(ArrayElement);
      } else if (arrayType == 'expectedCloseDate') {
        this.setState({
          expectedCloseDate: ArrayElement
        });
        this.props.updateAddDeaState('expectedCloseDate', ArrayElement);
      } else if (arrayType == 'company') {
        this.getCompanyDetails(ArrayElement);
      } else if (arrayType == 'pipeline') {
        this.getPipelineDetails(ArrayElement);
      } else if (arrayType == 'pipeline_stage') {
        this.getPipelineStagesDetails(ArrayElement);
      }
    }
  };

  componentDidMount() {
    const { ToastProvider } = this.props.screenProps;
    BackButtonHandler.mount(
      true,
      this.props.navigation,
      ToastProvider.showToast
    );
    const dealInfo = this.props.navigation.state.params.dealInfo;
    if (!this.props.navigation.state.params.isComeFromDeal) {
      this.firstArrayElement(dealInfo.name, 'name');
      this.firstArrayElement(dealInfo.amount, 'amount');
      this.firstArrayElement(dealInfo.contact, 'contact');
      this.firstArrayElement(
        dealInfo.expected_closed_date,
        'expectedCloseDate'
      );
      this.firstArrayElement(dealInfo.company, 'company');
      this.firstArrayElement(dealInfo.pipeline, 'pipeline');
      this.firstArrayElement(dealInfo.pipeline_stage, 'pipeline_stage');
    }
  }

  submitAddDeal = () => {
    const { ToastProvider } = this.props.screenProps;
    if (
      this.props.addDealState.name == '' ||
      this.props.addDealState.amount == '' ||
      this.props.addDealState.expectedCloseDate == 'YYYY/MM/DD' ||
      this.props.addDealState.selectedContactDetails == '' ||
      this.props.addDealState.selectedCompanyDetalis == '' ||
      this.props.addDealState.selectedPipelineDetalis == '' ||
      this.props.addDealState.selectedPipelineStageDetalis == ''
    ) {
      alert('please fill all the details');
    } else {
      if (!this.props.navigation.state.params.isComeFromDeal) {
        const dealInfo = this.props.navigation.state.params.dealInfo;
        const reqBody = {
          name: this.props.addDealState.name,
          amount: this.props.addDealState.amount,
          expectedCloseDate: this.state.expectedCloseDate,
          contact: this.props.addDealState.selectedContactDetails.url.split(
            '?'
          )[0],
          company: this.props.addDealState.selectedCompanyDetalis.url.split(
            '?'
          )[0],
          pipeline: this.props.addDealState.selectedPipelineDetalis.url.split(
            '?'
          )[0],
          pipelineStage: this.props.addDealState.selectedPipelineStageDetalis.url.split(
            '?'
          )[0]
        };

        this.props.updateAddDeaState('name', '');
        this.props.updateAddDeaState('amount', '');
        this.props.updateAddDeaState('selectedContactDetails', []);
        this.props.updateAddDeaState('expectedCloseDate', 'YYYY/MM/DD');
        this.props.updateAddDeaState('selectedCompanyDetalis', []);
        this.props.updateAddDeaState('selectedPipelineDetalis', []);
        this.props.updateAddDeaState('selectedPipelineStageDetalis', []);
        // console.log('rq body', reqBody);
        this.props.addDealUpdateSubmit(
          reqBody,
          dealInfo.id,
          ToastProvider.showToast
        );
        setTimeout(() => {
          this.goToHome();
        }, 100);
      } else {
        const reqBody = {
          name: this.props.addDealState.name,
          amount: this.props.addDealState.amount,
          expectedCloseDate: this.state.expectedCloseDate,
          contact: this.props.addDealState.selectedContactDetails.url.split(
            '?'
          )[0],
          company: this.props.addDealState.selectedCompanyDetalis.url.split(
            '?'
          )[0],
          pipeline: this.props.addDealState.selectedPipelineDetalis.url.split(
            '?'
          )[0],
          pipelineStage: this.props.addDealState.selectedPipelineStageDetalis.url.split(
            '?'
          )[0]
        };
        console.log('rq body', reqBody);
        this.props.addDealDataSubmit(reqBody, ToastProvider.showToast);
        setTimeout(() => {
          this.goToHome();
        }, 100);
      }
    }
  };
  componentWillUnmount() {
    BackButtonHandler.unmount();
  }

  toggleModal(visible) {
    this.setState({ modalVisible: visible });
  }
  toggleModalCompany(visible) {
    this.setState({ modalVisibleCompany: visible });
  }

  _showExpectedcloseDate = () =>
    this.setState({ expectedcloseDateVisible: true });
  _hideExpectedcloseDateVisible = () =>
    this.setState({ expectedcloseDateVisible: false });
  _handleExpectedcloseDate = date => {
    const endDate = date.getDate().toString();
    // const endMonth = months[date.getMonth()];
    const endMonth = date.getMonth() + 1;
    const endYear = date.getFullYear().toString();
    this.setState({
      expectedCloseDate: endYear + '-' + endMonth + '-' + endDate
    });
    this.props.updateAddDeaState(
      'expectedCloseDate',
      this.state.expectedCloseDate
    );
    this._hideExpectedcloseDateVisible();
  };

  goToHome = () => {
    this.props.updateAddDeaState('name', '');
    this.props.updateAddDeaState('amount', '');
    this.props.updateAddDeaState('selectedContactDetails', []);
    this.props.updateAddDeaState('expectedCloseDate', 'YYYY/MM/DD');
    this.props.updateAddDeaState('selectedCompanyDetalis', []);
    this.props.updateAddDeaState('selectedPipelineDetalis', []);
    this.props.updateAddDeaState('selectedPipelineStageDetalis', []);

    if (this.props.navigation.state.params.type == 'Deal') {
      this.props.navigation.navigate('DealsForm');
    } else if (this.props.navigation.state.params.type == 'DealDetail') {
      this.props.navigation.navigate('DealsDetailsScreen');
    } else if (this.props.navigation.state.params.type == 'Profile') {
      this.props.navigation.navigate('Profile');
    }
  };

  LoadingContactListData() {
    const { contactFormState } = this.props;
    if (contactFormState.isLoading) {
      return (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
            alignSelf: 'center'
          }}
        >
          <ActivityIndicator size="large" color="#00b2e2" animating={true} />
        </View>
      );
    } else {
      if (
        contactFormState.contactTextInput == '' ||
        !contactFormState.contactTextInput.trim().length
      ) {
        return (
          <FlatList
            data={contactFormState.contactListArray}
            renderItem={({ item, index }) => this.contactLists(item, index)}
            keyExtractor={(item, index) => index.toString()}
          />
        );
      } else {
        let updatedContactList = contactFormState.contactListArray.filter(
          item => {
            return item['first_name']
              .toLowerCase()
              .includes(contactFormState.contactTextInput.trim().toLowerCase());
          }
        );
        return (
          <FlatList
            data={updatedContactList}
            renderItem={({ item, index }) => this.contactLists(item, index)}
            keyExtractor={(item, index) => index.toString()}
          />
        );
      }
    }
  }

  checkForImageOrFirstLetterName(first_Name, last_name, picture_url, i) {
    if (picture_url == null && (first_Name == '' || first_Name == null)) {
    } else if (picture_url == null) {
      const first = this.getFirstLetterOfContact(first_Name);
      const last = this.getFirstLetterOfContact(last_name);
      return (
        <View>
          <Text
            style={[
              styles.fontPrimary_monsMedium,
              { color: '#fff', fontSize: 20 }
            ]}
          >
            {first.substr(0, 1) + '' + last.substr(0, 1)}
          </Text>
        </View>
      );
    } else {
      const { contactFormState, getContactList } = this.props;
      return (
        <View>
          <Image
            style={{ width: 50, height: 50, borderRadius: 25 }}
            resizeMode={'contain'}
            source={{
              uri: picture_url
            }}
            onError={() => {
              let newArr = [...contactFormState.contactListArray];
              newArr[i].picture_url = null;
              updateContactState('contactListArray', newArr);
            }}
          />
        </View>
      );
    }
  }

  isEmailNull(emailArray) {
    if (emailArray == null) {
      return;
    } else {
      return (
        <Text
          style={[
            styles.fontPrimary_Bold,
            styles.textSize14,
            { color: '#7f7f7f' }
          ]}
        >
          {emailArray.email}
        </Text>
      );
    }
  }
  splittedName = '';
  getFirstLetterOfContact(Name) {
    this.splittedName = Name.split(/\W/)
      .map(item => {
        for (var i = 0; i < item.length; i++) {
          return item[i];
        }
      })
      .join('');
    firstLetterOfName = this.splittedName;
    return firstLetterOfName;
  }

  contactLists(item, i) {
    const {
      contactFormState,
      getContactList,
      profileScreenState,
      updateProfileState
    } = this.props;
    return (
      <View
        onStartShouldSetResponder={() => {
          this.setState({ modalVisible: false });
          this.props.updateAddDeaState('selectedContactDetails', item);
        }}
        style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 10 }}
      >
        <View style={{ flex: 2, alignContent: 'center', padding: 5 }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#00b2e2',
              height: 50,
              width: 50,
              borderRadius: 25
            }}
          >
            {this.checkForImageOrFirstLetterName(
              item.first_name,
              item.last_name,
              item.picture_url,
              i
            )}
          </View>
        </View>
        <View style={{ flex: 10, alignContent: 'center', padding: 5 }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'flex-start',
              height: 50
            }}
          >
            <Text
              style={[
                styles.fontPrimary_monsMedium,
                styles.textSize16,
                { color: '#000' }
              ]}
              numberOfLines={1}
            >
              {item.first_name + ' ' + item.last_name}
            </Text>
            {this.isEmailNull(item.emails[0])}
          </View>
        </View>
      </View>
    );
  }
  closeModelFromCompanyModel = selectedCompanyDetalis => {
    this.setState({ modalVisibleCompany: false });
    this.props.updateAddDeaState(
      'selectedCompanyDetalis',
      selectedCompanyDetalis
    );
  };

  closeModelFromPipelineModel = selectedPipelineDetalis => {
    this.setState({ modalVisiblePipeline: false });
    this.props.updateAddDeaState(
      'selectedPipelineDetalis',
      selectedPipelineDetalis
    );
  };

  closeModelFromPipelineStageModel = selectedPipelineStageDetalis => {
    this.setState({ modalVisiblePipelineStage: false });
    this.props.updateAddDeaState(
      'selectedPipelineStageDetalis',
      selectedPipelineStageDetalis
    );
  };

  render() {
    const { contactFormState } = this.props;
    return (
      <Container>
        <View style={{ flex: 1 }}>
          <Header
            titleName="Add Deal"
            leftTitleImg={Icons.profile_back}
            RightTitleImg={Icons.notification_light()}
            onPressBack={this.goToHome}
          />
          <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                this.setState({ modalVisible: false });
              }}
              BackButtonHandler={true}
            >
              <Container>
                <Header
                  titleName="Contact Modal"
                  leftTitleImg={Icons.notification_light()}
                  RightTitleImg={Icons.notification_light()}
                />
                <View style={{ flex: 8 }}>
                  <View
                    style={{
                      height: 55,
                      marginLeft: 25,
                      marginRight: 25,
                      paddingTop: 10,
                      paddingBottom: 5
                    }}
                  >
                    <InputBox
                      rightImageName={Icons.search}
                      placeholder="Search by Contact First Name"
                      value={contactFormState.contactTextInput}
                      name="contactTextInput"
                      updateLocalState={this.props.updateContactState}
                    />
                  </View>
                  <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                    <View
                      style={{ flex: 9, paddingTop: 10, flexDirection: 'row' }}
                    >
                      {this.LoadingContactListData()}
                    </View>
                  </ScrollView>
                </View>
              </Container>
            </Modal>

            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisibleCompany}
              onRequestClose={() => {
                this.setState({ modalVisibleCompany: false });
              }}
              BackButtonHandler={true}
            >
              <Container>
                <Header
                  titleName="Company Modal"
                  leftTitleImg={Icons.notification_light()}
                  RightTitleImg={Icons.notification_light()}
                />
                <View style={{ flex: 9 }}>
                  <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                    <View
                      style={{ flex: 9, paddingTop: 10, flexDirection: 'row' }}
                    >
                      <CompanyForm
                        isComeFromModel={true}
                        closeModel={this.closeModelFromCompanyModel}
                      />
                    </View>
                  </ScrollView>
                </View>
              </Container>
            </Modal>

            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisiblePipeline}
              onRequestClose={() => {
                this.setState({ modalVisiblePipeline: false });
              }}
              BackButtonHandler={true}
            >
              <Container>
                <Header
                  titleName="Pipeline Modal"
                  leftTitleImg={Icons.notification_light()}
                  RightTitleImg={Icons.notification_light()}
                />
                <View style={{ flex: 9 }}>
                  <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                    <View
                      style={{ flex: 9, paddingTop: 10, flexDirection: 'row' }}
                    >
                      <PipelineForm
                        isComeFromModel={true}
                        closePipelineModel={this.closeModelFromPipelineModel}
                      />
                    </View>
                  </ScrollView>
                </View>
              </Container>
            </Modal>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisiblePipelineStage}
              onRequestClose={() => {
                this.setState({ modalVisiblePipelineStage: false });
              }}
              BackButtonHandler={true}
            >
              <Container>
                <Header
                  titleName="Pipeline Stage Modal"
                  leftTitleImg={Icons.notification_light()}
                  RightTitleImg={Icons.notification_light()}
                />
                <View style={{ flex: 9 }}>
                  <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                    <View
                      style={{ flex: 9, paddingTop: 10, flexDirection: 'row' }}
                    >
                      <PipelineStageForm
                        isComeFromModel={true}
                        closePipelineStageModel={
                          this.closeModelFromPipelineStageModel
                        }
                      />
                    </View>
                  </ScrollView>
                </View>
              </Container>
            </Modal>

            <View
              style={{
                flex: 8,
                height: '85%',
                marginTop: 15
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Regular,
                      {
                        color: '#000',
                        alignItems: 'center',
                        fontSize: 14
                      }
                    ]}
                  >
                    Name
                  </Text>
                </View>
                <View
                  style={{
                    flex: 2,
                    height: 40,
                    padding: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: 15
                  }}
                >
                  <InputBox
                    placeholder="Name"
                    value={this.props.addDealState.name}
                    name="name"
                    updateLocalState={this.props.updateAddDeaState}
                  />
                </View>
              </View>
              <Spacer />
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Regular,
                      {
                        color: '#000',
                        alignItems: 'center',
                        fontSize: 14
                      }
                    ]}
                  >
                    Amount
                  </Text>
                </View>
                <View
                  style={{
                    flex: 2,
                    height: 40,
                    padding: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: 15
                  }}
                >
                  <InputBox
                    placeholder="Amount"
                    value={this.props.addDealState.amount}
                    name="amount"
                    updateLocalState={this.props.updateAddDeaState}
                  />
                </View>
              </View>
              <Spacer />
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Regular,
                      {
                        color: '#000',
                        alignItems: 'center',
                        fontSize: 14
                      }
                    ]}
                  >
                    Contact
                  </Text>
                </View>
                <View
                  onStartShouldSetResponder={() => {
                    this.setState({
                      modalVisible: true
                    });
                  }}
                  style={{
                    flex: 2,
                    height: 40,
                    padding: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: 15,
                    borderColor: 'grey',
                    borderWidth: 2,
                    borderRadius: 30
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Bold,
                      styles.textSize14,
                      {
                        color: '#7f7f7f',
                        alignItems: 'center'
                      }
                    ]}
                  >
                    {this.props.addDealState.selectedContactDetails == ''
                      ? 'select here'
                      : this.props.addDealState.selectedContactDetails
                          .first_name +
                        ' ' +
                        this.props.addDealState.selectedContactDetails
                          .last_name}
                  </Text>
                </View>
              </View>
              <Spacer />
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Regular,
                      {
                        color: '#000',
                        alignItems: 'center',
                        fontSize: 14
                      }
                    ]}
                  >
                    Expected close Date
                  </Text>
                </View>
                <View
                  style={{
                    flex: 2,
                    height: 40,
                    marginRight: 15
                  }}
                >
                  <View style={{ height: 40 }}>
                    <TouchableOpacity onPress={this._showExpectedcloseDate}>
                      <View
                        style={{
                          flexDirection: 'row',
                          borderColor: 'grey',
                          borderWidth: 2,
                          borderRadius: 30,
                          backgroundColor: '#eeeeee'
                        }}
                      >
                        <View
                          style={{
                            flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center',
                            paddingLeft: 10
                          }}
                        />
                        <View style={{ flex: 7, height: 35 }}>
                          <Text style={{ paddingTop: 8, color: 'grey' }}>
                            {this.state.expectedCloseDate}
                          </Text>
                        </View>
                        <View
                          style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            paddingRight: 10
                          }}
                        >
                          {Icons.calender()}
                        </View>
                      </View>
                    </TouchableOpacity>
                    <DateTimePicker
                      mode="date"
                      minimumDate={new Date()}
                      isVisible={this.state.expectedcloseDateVisible}
                      onConfirm={this._handleExpectedcloseDate}
                      onCancel={this._hideExpectedcloseDateVisible}
                      datePickerModeAndroid={'spinner'}
                    />
                  </View>
                </View>
              </View>
              <Spacer />
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Regular,
                      {
                        color: '#000',
                        alignItems: 'center',
                        fontSize: 14
                      }
                    ]}
                  >
                    Company
                  </Text>
                </View>
                <View
                  onStartShouldSetResponder={() => {
                    this.setState({
                      modalVisibleCompany: true
                    });
                  }}
                  style={{
                    flex: 2,
                    height: 40,
                    padding: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: 15,
                    borderColor: 'grey',
                    borderWidth: 2,
                    borderRadius: 30
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Bold,
                      styles.textSize14,
                      {
                        color: '#7f7f7f',
                        alignItems: 'center'
                      }
                    ]}
                  >
                    {this.props.addDealState.selectedCompanyDetalis == ''
                      ? 'select here'
                      : this.props.addDealState.selectedCompanyDetalis.name}
                  </Text>
                </View>
              </View>
              <Spacer />
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  onStartShouldSetResponder={() => {
                    this.setState({
                      modalVisiblePipeline: true
                    });
                  }}
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Regular,
                      {
                        color: '#000',
                        alignItems: 'center',
                        fontSize: 14
                      }
                    ]}
                  >
                    Pipeline
                  </Text>
                </View>
                <View
                  onStartShouldSetResponder={() => {
                    this.setState({
                      modalVisiblePipeline: true
                    });
                  }}
                  style={{
                    flex: 2,
                    height: 40,
                    padding: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: 15,
                    borderColor: 'grey',
                    borderWidth: 2,
                    borderRadius: 30
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Bold,
                      styles.textSize14,
                      {
                        color: '#7f7f7f',
                        alignItems: 'center'
                      }
                    ]}
                  >
                    {this.props.addDealState.selectedPipelineDetalis == ''
                      ? 'select here'
                      : this.props.addDealState.selectedPipelineDetalis.name}
                  </Text>
                </View>
              </View>
              <Spacer />
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Regular,
                      {
                        color: '#000',
                        alignItems: 'center',
                        fontSize: 14
                      }
                    ]}
                  >
                    Pipeline Stage
                  </Text>
                </View>
                <View
                  onStartShouldSetResponder={() => {
                    this.setState({
                      modalVisiblePipelineStage: true
                    });
                  }}
                  style={{
                    flex: 2,
                    height: 40,
                    padding: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: 15,
                    borderColor: 'grey',
                    borderWidth: 2,
                    borderRadius: 30
                  }}
                >
                  <Text
                    style={[
                      styles.fontPrimary_Bold,
                      styles.textSize14,
                      {
                        color: '#7f7f7f',
                        alignItems: 'center'
                      }
                    ]}
                  >
                    {this.props.addDealState.selectedPipelineStageDetalis == ''
                      ? 'select here'
                      : this.props.addDealState.selectedPipelineStageDetalis
                          .name}
                  </Text>
                </View>
              </View>

              <Spacer size={20} />
            </View>
            <View style={{ height: '15%' }}>
              <View
                style={{
                  marginRight: 15,
                  marginLeft: 15,
                  flexDirection: 'row'
                }}
              >
                <View
                  style={{
                    flex: 1,
                    marginRight: 5,
                    borderRadius: 30,
                    borderWidth: 1,
                    borderColor: 'grey'
                  }}
                >
                  <ButtonBox
                    btnText="Cancel"
                    btnColor="white"
                    btnTextColor="#000"
                    onPress={this.goToHome}
                  />
                </View>
                <View style={{ flex: 1, marginLeft: 5 }}>
                  <ButtonBox
                    btnText="Save"
                    btnColor="#f5921e"
                    btnTextColor="#fff"
                    onPress={this.submitAddDeal}
                  />
                </View>
              </View>
              <Spacer size={20} />
            </View>
          </ScrollView>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return state;
};
const mapDispatchToProps = {
  addDealUpdateSubmit,
  addDealDataSubmit,
  updateAddDeaState,
  updateContactState
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddDealScreen);
