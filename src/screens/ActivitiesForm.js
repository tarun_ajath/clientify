import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native';
import ActivitiesExpiredTaskForm from '../component/ActivitiesExpiredTaskForm';
import ActivitiesTodayActivity from '../component/ActivitiesTodayActivity';
import ActivitiesNextActivity from '../component/ActivitiesNextActivity';
import Header from '../component/common/Header';
import Icons from '../component/common/Icons';
import Container from '../component/common/Container';
import { connect } from 'react-redux';
import { BottomNavigation, FAB, Portal } from 'react-native-paper';
import {
  getActionList,
  updateActionState
} from '../Actions/activitiesFormAction';
class ActivitiesForm extends Component {
  constructor() {
    super();
    this.state = {
      x: 1,
      showFab: false,
      open: false
    };
  }

  componentDidMount() {
    this.props.getActionList();

    this.props.navigation.addListener('didFocus', () =>
      this.setState({
        showFab: true
      })
    );
    this.props.navigation.addListener('willBlur', () =>
      this.setState({
        showFab: false
      })
    );
  }

  goToAddTaskEvents = () => {
    this.props.navigation.navigate('AddNoteScreen', { isComeFrom: 1 });
  };

  renderHome() {
    const { activitiesFormState } = this.props;
    let dateZone = activitiesFormState.actionListArray;
    // console.log(' action form  ', dateZone);
    var d1 = new Date();
    // console.log('hello date', d1.toISOString());
    let todayActvity = activitiesFormState.actionListArray.filter(item => {
      return item['due_date'] == d1.toISOString();
    });
    // console.log('todayActvity form  ', todayActvity);

    let expiredtActivity = activitiesFormState.actionListArray.filter(item => {
      return item['due_date'] <= d1.toISOString();
    });
    // console.log('expiredtActivity form  ', expiredtActivity);

    let nextActivity = activitiesFormState.actionListArray.filter(item => {
      return item['due_date'] >= d1.toISOString();
    });
    // console.log('expiredtActivity form  ', nextActivity);

    if (activitiesFormState.isLoadingActionList) {
      return (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
            alignSelf: 'center'
          }}
        >
          <ActivityIndicator size="large" color="#00b2e2" animating={true} />
        </View>
      );
    } else {
      if (this.state.x == 1) {
        return (
          <ActivitiesExpiredTaskForm
            activityExpiredTaskList={expiredtActivity}
          />
        );
      } else if (this.state.x == 2) {
        return (
          <ActivitiesTodayActivity
            goToAddTaskEvents={this.goToAddTaskEvents}
            activityExpiredTaskList={todayActvity}
          />
        );
      } else if (this.state.x == 3) {
        return (
          <ActivitiesNextActivity activityExpiredTaskList={nextActivity} />
        );
      }
    }
  }

  goToHome = () => {
    this.props.navigation.navigate('ContactStack');
  };

  fabFunctionTaskEventAdd = () => {
    if (this.state.showFab) {
      return (
        <Portal>
          <FAB.Group
            color={'white'}
            style={{ marginBottom: 55 }}
            open={this.state.open}
            icon={this.state.open ? 'close' : 'add'}
            actions={[
              {
                icon: Icons.task_yellow,
                label: 'Add Task',
                onPress: () => {
                  this.props.navigation.navigate('AddTask', {
                    isComeFrom: 'activity'
                  });
                }
              },
              {
                icon: Icons.deal_tag_yellow,
                label: 'Add Event',
                onPress: () => {
                  this.props.navigation.navigate('AddEventActivity');
                }
              }
            ]}
            onStateChange={({ open }) => this.setState({ open })}
            onPress={() => {
              if (this.state.open) {
                // do something if the speed dial is open
                // alert('Helllooooo');
              }
            }}
          />
        </Portal>
      );
    }
  };

  render() {
    return (
      <Container>
        <View style={{ flex: 10 }}>
          <Header
            titleName="Activities"
            leftTitleImg={Icons.profile_back}
            RightTitleImg={Icons.notification_light}
            onPress={this.goToHome}
            onPressBack={this.goToHome}
          />

          <View style={{ flex: 1, height: 45, flexDirection: 'row' }}>
            <View
              onStartShouldSetResponder={() => {
                this.setState({
                  x: 1
                });
              }}
              style={this.state.x == 1 ? styles.btnActive : styles.btn}
            >
              {this.fabFunctionTaskEventAdd()}
              <Text
                style={
                  this.state.x == 1 ? styles.textBtnActive : styles.textBtn
                }
              >
                Expired Tasks
              </Text>
            </View>
            <View
              onStartShouldSetResponder={() => {
                this.setState({
                  x: 2
                });
              }}
              style={this.state.x == 2 ? styles.btnActive : styles.btn}
            >
              <Text
                style={
                  this.state.x == 2 ? styles.textBtnActive : styles.textBtn
                }
              >
                Today Activities
              </Text>
            </View>
            <View
              onStartShouldSetResponder={() => {
                this.setState({
                  x: 3
                });
              }}
              style={this.state.x == 3 ? styles.btnActive : styles.btn}
            >
              <Text
                style={
                  this.state.x == 3 ? styles.textBtnActive : styles.textBtn
                }
              >
                Next Activities
              </Text>
            </View>
          </View>
          <View style={styles.container}>
            <View style={{ flex: 8 }}>{this.renderHome()}</View>
          </View>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return state;
};
const mapDispatchToProps = dispatch => {
  return {
    getActionList: (name, value) => dispatch(getActionList(name, value)),
    updateActionState: (name, value) => dispatch(updateActionState(name, value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ActivitiesForm);

const styles = StyleSheet.create({
  container: {
    flex: 8,
    backgroundColor: '#F5FCFF'
  },
  textBtn: {
    color: 'grey',
    fontSize: 13,
    fontFamily: 'Montserrat-Medium'
  },
  textBtnActive: {
    color: '#fff',
    fontSize: 13,
    fontFamily: 'Montserrat-Medium'
  },
  btnActive: {
    flex: 1,
    backgroundColor: '#00b2e2',
    fontSize: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  btn: {
    flex: 1,
    backgroundColor: '#ffff',
    fontSize: 10,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
