import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  Linking,
  Platform
} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import Spacer from '../component/common/Spacer';
import Icons from '../component/common/Icons';
import BackButtonHandler from '../utilities/BackButtonHandler';
import { connect } from 'react-redux';
import ContactProfileTimline from '../component/ContactProfileTimline';
import ContactProfileContactInfo from '../component/ContactProfileContactInfo';
import {
  getProfileDetails,
  updateProfileState,
  deleteContactProfile
} from '../Actions/profileScreenAction';
import Container from '../component/common/Container';
import ContactProfileAdditionalInfo from '../component/ContactProfileAdditionalInfo';
import DealsOpenExpired from '../component/DealsOpenExpired';
import HttpClient from '../utilities/HttpClient';
import DataStore from '../utilities/DataStore';

class ProfileScreen extends Component {
  constructor() {
    super();
    this.state = {
      x: 1
    };
  }

  componentDidMount() {
    const { ToastProvider } = this.props.screenProps;
    BackButtonHandler.mount(
      true,
      this.props.navigation,
      ToastProvider.showToast
    );
  }

  componentWillUnmount() {
    BackButtonHandler.unmount();
  }

  checkForImageOrFirstLetterName(first_Name, last_name, picture_url) {
    if (picture_url == null && (first_Name == '' || first_Name == null)) {
    } else if (picture_url == null) {
      const first = this.getFirstLetterOfContact(first_Name);
      const last = this.getFirstLetterOfContact(last_name);
      return (
        <View>
          <Text
            style={[
              styles.fontPrimary_monsMedium,
              { color: '#fff', fontSize: 20 }
            ]}
          >
            {first.substr(0, 1) + '' + last.substr(0, 1)}
          </Text>
        </View>
      );
    } else {
      return (
        <View>
          <Image
            style={{ width: 50, height: 50, borderRadius: 25 }}
            resizeMode={'contain'}
            source={{
              uri: picture_url
            }}
            onError={() => {
              // let newArr = [...contactFormState.contactListArray];
              // newArr[i].picture_url = null;
              // updateContactState('contactListArray', newArr);
              // setTimeout(() => this.setState({ refresh: true }), 1000);
            }}
          />
        </View>
      );
    }
  }

  splittedName = '';
  getFirstLetterOfContact(Name) {
    this.splittedName = Name.split(/\W/)
      .map(item => {
        for (var i = 0; i < item.length; i++) {
          return item[i];
        }
      })
      .join('');
    firstLetterOfName = this.splittedName;
    return firstLetterOfName;
  }

  renderScreens() {
    const { contactFormState, profileScreenState } = this.props;
    const { ToastProvider } = this.props.screenProps;
    let newProfileArray = profileScreenState.profileListArray;
    if (this.state.x == 1) {
      return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <ContactProfileTimline newTimlineListArray={newProfileArray} />
        </ScrollView>
      );
    } else if (this.state.x == 2) {
      return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <ContactProfileAdditionalInfo
            newListArray={newProfileArray}
            ToastProvider={ToastProvider}
          />
        </ScrollView>
      );
    } else if (this.state.x == 3) {
      return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <ContactProfileContactInfo newListArray={newProfileArray} />
        </ScrollView>
      );
    }
  }
  checkNull(ArrayElement, arrayType) {
    if (ArrayElement == null || ArrayElement == '') {
      if (arrayType == 'emails') alert('there is no email');
      if (arrayType == 'phones') alert('there is no phone');
    } else {
      if (arrayType == 'emails') {
        Linking.openURL(
          'mailto:' + ArrayElement[0].email + '?subject=abcdefg&body=body'
        );
      } else if (arrayType == 'phones') {
        this.props.screenProps.ToastProvider.showToast(
          'Calling on...' + ArrayElement[0].phone,
          'sucess'
        );
        let phoneNumber = ArrayElement[0].phone;
        if (Platform.OS !== 'android') {
          phoneNumber = `telprompt:${phoneNumber}`;
        } else {
          phoneNumber = `tel:${phoneNumber}`;
        }
        Linking.canOpenURL(phoneNumber)
          .then(supported => {
            if (!supported) {
              Alert.alert('Phone number is not available');
            } else {
              return Linking.openURL(phoneNumber);
            }
          })
          .catch(err => console.log(err));
      }
    }
  }

  // companyDetails = async companyDetails => {
  //   if (companyDetails != null) {
  //     const companyUrl = companyDetails.url.split('?')[0];
  //     console.log('cmp url ', companyUrl);
  //     try {
  //       let responseData = await axios.get(
  //         companyUrl + '/?api_key=' + DataStore.token + '&format=json'
  //       );
  //       console.log('cmp details  ', responseData);
  //     } catch (error) {
  //       console.log('company details err', error);
  //     }
  //   }
  // };

  render() {
    console.log('profile propssssss ', this.props);
    const { contactFormState, getContactList, profileScreenState } = this.props;
    let newProfileArray = profileScreenState.profileListArray;
    return (
      <Container>
        <View style={{ flex: 1 }}>
          <View
            style={{ flex: 3.5, backgroundColor: '#00b2e2', paddingBottom: 25 }}
          >
            <NavigationEvents
              onWillFocus={() => {
                // alert('hello array  ' + profileScreenState.profileListArray);
                // alert('gshg  ' + profileScreenState.profileIdOfData);

                this.props.updateProfileState('profileListArray', []);
                // console.log('props on contact', this.props);
                setTimeout(() => {
                  this.props.getProfileDetails(
                    profileScreenState.profileIdOfData
                  );
                }, 100);
              }}
            />
            <View
              style={{
                // flex: 3,
                height: '15%',
                flexDirection: 'row',
                paddingTop: 10
              }}
            >
              <View style={{ flex: 1.5, paddingLeft: 13 }}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('ContactForm');
                  }}
                >
                  {Icons.profile_back()}
                </TouchableOpacity>
              </View>
              <View
                style={{ flex: 7.5, paddingLeft: 10, alignItems: 'center' }}
              >
                <Text
                  style={{
                    color: '#ffff',
                    fontSize: 15,
                    fontFamily: 'Raleway-SemiBold',
                    paddingLeft: 10
                  }}
                >
                  {newProfileArray.status}
                </Text>
              </View>
              <View style={{ flex: 1.5, paddingRight: 10 }}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('AddNewContacts', {
                      isComeFromContact: false,
                      contactInfo: newProfileArray
                    });
                  }}
                >
                  {Icons.profile_edit()}
                </TouchableOpacity>
              </View>
              <View style={{ flex: 1.5, paddingRight: 5 }}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.deleteContactProfile(newProfileArray.id);
                  }}
                >
                  {Icons.profile_delete()}
                </TouchableOpacity>
              </View>
            </View>
            <Spacer />
            <View style={{ flex: 12, paddingBottom: 5 }}>
              <View
                style={{
                  alignItems: 'center',
                  paddingBottom: 10,
                  paddingTop: 7
                }}
              >
                <Text
                  style={{
                    color: '#ffff',
                    fontSize: 16,
                    fontFamily: 'Raleway-SemiBold'
                  }}
                >
                  {newProfileArray == ''
                    ? 'loading...'
                    : newProfileArray.first_name +
                      ' ' +
                      newProfileArray.last_name}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    // this.companyDetails(newProfileArray.company);
                    console.log('cmp url ', newProfileArray.company);
                  }}
                >
                  <Text
                    style={{
                      color: '#ffff',
                      fontSize: 14,
                      fontFamily: 'Raleway-Regular'
                    }}
                  >
                    Company :{' '}
                    {newProfileArray.company == null ||
                    newProfileArray.company == ''
                      ? 'loading'
                      : newProfileArray.company}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <View
                  style={{
                    flex: 4,
                    alignItems: 'flex-end',
                    justifyContent: 'center'
                  }}
                >
                  <TouchableOpacity
                    style={styles.circleCallImage}
                    onPress={() => {
                      this.checkNull(newProfileArray.phones, 'phones');
                      // let phoneNumber = 1234567890;
                      // if (Platform.OS !== 'android') {
                      //   phoneNumber = `telprompt:${1234567890}`;
                      // } else {
                      //   phoneNumber = `tel:${1234567890}`;
                      // }
                      // Linking.canOpenURL(phoneNumber)
                      //   .then(supported => {
                      //     if (!supported) {
                      //       Alert.alert('Phone number is not available');
                      //     } else {
                      //       return Linking.openURL(phoneNumber);
                      //     }
                      //   })
                      //   .catch(err => console.log(err));
                    }}
                  >
                    <View style={styles.circleCallImage}>
                      {Icons.contact_customer()}
                    </View>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    flex: 5,
                    alignItems: 'center'
                  }}
                >
                  <View style={styles.circle}>
                    {this.checkForImageOrFirstLetterName(
                      newProfileArray.first_name,
                      newProfileArray.last_name,
                      newProfileArray.picture_url
                    )}
                  </View>
                </View>
                <View
                  style={{
                    flex: 4,
                    alignItems: 'flex-start',
                    justifyContent: 'center',
                    paddingTop: 10
                  }}
                >
                  <TouchableOpacity
                    style={styles.circleCallImage}
                    onPress={() => {
                      this.checkNull(newProfileArray.emails, 'emails');
                      // Linking.openURL(
                      //   'mailto:somethingemail@gmail.com?subject=abcdefg&body=body'
                      // );
                    }}
                  >
                    <View style={styles.circleCallImage}>
                      {Icons.msg_customer()}
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <Spacer />
            <View style={styles.footerStyle}>
              <View
                onStartShouldSetResponder={() => {}}
                style={{ flex: 3, alignItems: 'center' }}
              >
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('AddNoteScreen', {
                      isComeFrom: 1,
                      Id: newProfileArray.id
                    });
                  }}
                >
                  {Icons.profile_note()}
                </TouchableOpacity>
                <View style={{ paddingTop: 5 }}>
                  <Text style={styles.textNormal}>Note</Text>
                </View>
              </View>
              <View
                onStartShouldSetResponder={() => {
                  this.props.navigation.navigate('AddTask', {
                    isComeFrom: 'profile'
                  });
                }}
                style={{ flex: 3, alignItems: 'center' }}
              >
                {Icons.profile_task()}
                <View style={{ paddingTop: 5 }}>
                  <Text style={styles.textNormal}>Task</Text>
                </View>
              </View>
              <View
                onStartShouldSetResponder={() => {}}
                style={{ flex: 3, alignItems: 'center' }}
              >
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('AddEvents', {
                      isComeFrom: 'profile'
                    });
                  }}
                >
                  {Icons.profile_event()}
                </TouchableOpacity>
                <View style={{ paddingTop: 5 }}>
                  <Text style={styles.textNormal}>Event</Text>
                </View>
              </View>
              <TouchableOpacity
                style={{ flex: 3 }}
                onPress={() => {
                  // if (newProfileArray != '' || newProfileArray != []) {

                  //   console.log(
                  //     'open deals in profile are ',
                  //     newProfileArray.deals
                  //   );
                  //   this.props.navigation.navigate('ContactDealForm', {
                  //     dealsOpenExpiredList: newProfileArray.deals
                  //   });
                  // }

                  this.props.navigation.navigate('AddDealScreen', {
                    isComeFromDeal: true,
                    type: 'Profile'
                  });
                }}
              >
                <View style={{ flex: 3, alignItems: 'center', paddingTop: 10 }}>
                  {Icons.deal_custumer()}
                  <View style={{ paddingTop: 10 }}>
                    <Text style={styles.textNormal}>Deal</Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          </View>

          <View>
            <View
              style={{
                height: 50,
                flexDirection: 'row',
                borderBottomColor: '#eeee'
              }}
            >
              <View style={this.state.x == 1 ? styles.btnActive : styles.btn}>
                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      x: 1
                    });
                  }}
                >
                  <Text
                    style={
                      this.state.x == 1 ? styles.textBtnActive : styles.textBtn
                    }
                  >
                    {' '}
                    Timeline
                  </Text>
                </TouchableOpacity>
              </View>
              <View
                style={{ height: '100%', width: 1, backgroundColor: 'grey' }}
              />
              <View
                style={
                  (style = this.state.x == 2 ? styles.btnActive : styles.btn)
                }
              >
                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      x: 2
                    });
                  }}
                >
                  <Text
                    style={
                      this.state.x == 2 ? styles.textBtnActive : styles.textBtn
                    }
                  >
                    {' '}
                    Additional {'\n'} {'      '}info
                  </Text>
                </TouchableOpacity>
              </View>
              <View
                style={{ height: '100%', width: 1, backgroundColor: 'grey' }}
              />
              <View
                style={
                  (style = this.state.x == 3 ? styles.btnActive : styles.btn)
                }
              >
                <TouchableOpacity
                  onPress={() => {
                    this.setState({
                      x: 3
                    });
                  }}
                >
                  <Text
                    style={
                      this.state.x == 3 ? styles.textBtnActive : styles.textBtn
                    }
                  >
                    Contact {'\n'} {'    '}info
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{ height: 1, width: '100%', backgroundColor: 'grey' }}
            />
          </View>
          <View style={{ flex: 4, backgroundColor: '#ffff' }}>
            {this.renderScreens()}
          </View>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return state;
};
const mapDispatchToProps = {
  getProfileDetails,
  updateProfileState,
  deleteContactProfile
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileScreen);
const styles = StyleSheet.create({
  footerStyle: {
    flex: 10,
    flexDirection: 'row',
    paddingTop: 80,
    borderColor: '#dddd'
  },
  circleCallImage: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    height: 60,
    width: 60,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#fff',
    backgroundColor: '#ffff'
  },
  circle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    height: 80,
    width: 80,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#ffff',
    resizeMode: 'contain'
  },
  circleImage: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 70,
    width: 70,
    overflow: 'hidden',
    borderRadius: 50
  },
  text: {},
  textBtn: {
    color: 'grey',
    fontSize: 13,
    fontFamily: 'Raleway-SemiBold'
  },
  textBtnActive: {
    color: '#fff',
    fontSize: 13,
    fontFamily: 'Raleway-SemiBold'
  },
  btnActive: {
    flex: 1,
    backgroundColor: '#00b2e2',
    fontSize: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  btn: {
    flex: 1,
    backgroundColor: '#ffff',
    fontSize: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textNormal: {
    color: '#ffff',
    fontSize: 12,
    fontFamily: 'Raleway-SemiBold'
  },
  textMail: {}
});
