import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Platform,
  Linking,
  Image
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Spacer from '../component/common/Spacer';
import Icons from '../component/common/Icons';
import BackButtonHandler from '../utilities/BackButtonHandler';
import { connect } from 'react-redux';
import {
  getCompanyList,
  updateCompanyState,
  getCompanyDetails,
  deleteCompany
} from '../Actions/companyFormAction';
import Container from '../component/common/Container';

class CompanyContactDetails extends Component {
  constructor() {
    super();
    this.state = {
      x: 1
    };
  }

  componentDidMount() {
    const { ToastProvider } = this.props.screenProps;
    BackButtonHandler.mount(
      true,
      this.props.navigation,
      ToastProvider.showToast
    );
  }

  componentWillUnmount() {
    BackButtonHandler.unmount();
  }

  checkForImageOrFirstLetterName(first_Name, last_name, picture_url) {
    if (picture_url == null && (first_Name == '' || first_Name == null)) {
    } else if (picture_url == null) {
      const first = this.getFirstLetterOfContact(first_Name);
      const last = this.getFirstLetterOfContact(last_name);
      return (
        <View>
          <Text
            style={[
              styles.fontPrimary_monsMedium,
              { color: '#fff', fontSize: 20 }
            ]}
          >
            {first.substr(0, 1) + '' + last.substr(0, 1)}
          </Text>
        </View>
      );
    } else {
      return (
        <View>
          <Image
            style={{ width: 50, height: 50, borderRadius: 25 }}
            resizeMode={'contain'}
            source={{
              uri: picture_url
            }}
            onError={() => {
              // let newArr = [...contactFormState.contactListArray];
              // newArr[i].picture_url = null;
              // updateContactState('contactListArray', newArr);
              // setTimeout(() => this.setState({ refresh: true }), 1000);
            }}
          />
        </View>
      );
    }
  }

  splittedName = '';
  getFirstLetterOfContact(Name) {
    this.splittedName = Name.split(/\W/)
      .map(item => {
        for (var i = 0; i < item.length; i++) {
          return item[i];
        }
      })
      .join('');
    firstLetterOfName = this.splittedName;
    return firstLetterOfName;
  }

  checkNull(ArrayElement, arrayType) {
    if (ArrayElement == null || ArrayElement == '') {
      if (arrayType == 'emails') alert('there is no email');
      if (arrayType == 'phone') alert('there is no phone');
    } else {
      if (arrayType == 'emails') {
        Linking.openURL(
          'mailto:' + ArrayElement[0].email + '?subject=abcdefg&body=body'
        );
      } else if (arrayType == 'phone') {
        this.props.screenProps.ToastProvider.showToast(
          'Calling on...' + ArrayElement[0].phone,
          'sucess'
        );
        let phoneNumber = ArrayElement[0].phone;
        if (Platform.OS !== 'android') {
          phoneNumber = `telprompt:${phoneNumber}`;
        } else {
          phoneNumber = `tel:${phoneNumber}`;
        }
        Linking.canOpenURL(phoneNumber)
          .then(supported => {
            if (!supported) {
              Alert.alert('Phone number is not available');
            } else {
              return Linking.openURL(phoneNumber);
            }
          })
          .catch(err => console.log(err));
      }
    }
  }

  employeesList(item, index) {
    return (
      // <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
      <View style={{ flex: 1 }}>
        <Spacer size={3} />
        <View style={{ flexDirection: 'row', paddingLeft: 25 }}>
          <View style={{ flex: 7 }}>
            <Text style={{ color: '#00b2e2', fontSize: 16 }}>
              {item.first_name + item.last_name}
            </Text>
            <Text style={{ color: 'grey', fontSize: 13 }}>
              {item.phones == [] || item.phones == ''
                ? 'xx-xxx-xxx'
                : item.phones[0].phone}
            </Text>
          </View>
          <View
            style={{
              flex: 2,
              alignItems: 'center',
              justifyContent: 'center'
            }}
          >
            <TouchableOpacity
              onPress={() => {
                this.checkNull(item.phones, 'phone');
              }}
            >
              {Icons.contact_us()}
            </TouchableOpacity>
          </View>
        </View>
        <Spacer size={3} />
        <View
          style={{
            height: 1,
            width: '100%',
            backgroundColor: 'grey'
          }}
        />
      </View>

      // </ScrollView>
    );
  }

  render() {
    const { companyFormState } = this.props;
    return (
      <Container>
        <View style={{ flex: 1 }}>
          <View
            style={{ flex: 3.5, backgroundColor: '#00b2e2', paddingBottom: 20 }}
          >
            <View style={styles.headerStyle}>
              <View
                onStartShouldSetResponder={() => {
                  this.props.navigation.navigate('ContactForm');
                }}
                style={{ flex: 1, paddingLeft: 10 }}
              >
                {Icons.profile_back()}
              </View>
              <View style={{ flex: 7 }} />
              <View style={{ flex: 1, paddingRight: 10 }}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('AddNewCompany', {
                      isComeFromCompany: false,
                      companyInfo: companyFormState.companyDetailsListArray
                    });
                  }}
                >
                  {Icons.profile_edit()}
                </TouchableOpacity>
              </View>
              <View style={{ flex: 1, paddingRight: 7 }}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.deleteCompany(
                      companyFormState.companyDetailsListArray.id
                    );
                  }}
                >
                  {Icons.profile_delete()}
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ flex: 15, paddingBottom: 5 }}>
              <View
                style={{
                  alignItems: 'center',
                  paddingBottom: 7,
                  paddingTop: 5
                }}
              >
                <Text
                  style={{
                    color: '#ffff',
                    fontSize: 15,
                    fontFamily: 'Raleway-SemiBold'
                  }}
                >
                  {companyFormState.companyDetailsListArray.name}
                </Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <View
                  style={{
                    flex: 4,
                    alignItems: 'flex-end',
                    justifyContent: 'center'
                  }}
                >
                  <TouchableOpacity
                    style={styles.circleCallImage}
                    onPress={() => {
                      this.checkNull(
                        companyFormState.companyDetailsListArray.phones,
                        'phone'
                      );
                    }}
                  >
                    <View style={styles.circleCallImage}>
                      {Icons.contact_customer()}
                    </View>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    flex: 5,
                    alignItems: 'center'
                  }}
                >
                  <View style={styles.circle}>
                    {this.checkForImageOrFirstLetterName(
                      companyFormState.companyDetailsListArray.name,
                      '',
                      companyFormState.companyDetailsListArray.picture_url
                    )}
                  </View>
                </View>
                <View
                  style={{
                    flex: 4,
                    alignItems: 'flex-start',
                    justifyContent: 'center',
                    paddingTop: 10
                  }}
                >
                  <TouchableOpacity
                    style={styles.circleCallImage}
                    onPress={() => {
                      {
                        this.checkNull(
                          companyFormState.companyDetailsListArray.emails,
                          'emails'
                        );
                      }
                    }}
                  >
                    <View style={styles.circleCallImage}>
                      {Icons.msg_customer()}
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              <View
                style={{
                  flex: 10,
                  flexDirection: 'row',
                  paddingTop: 20,
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    {
                      this.props.navigation.navigate('AddEvents', {
                        isComeFrom: 'company'
                      });
                    }
                  }}
                >
                  {Icons.profile_event()}
                  <View style={{ paddingTop: 5 }}>
                    <Text style={styles.textNormal}>Event</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <View>
            <View
              style={{
                height: 50,
                justifyContent: 'center',
                alignItems: 'flex-start',
                borderBottomColor: '#eeee',
                paddingLeft: 25
              }}
            >
              <Text style={{ color: 'grey', fontSize: 15 }}>
                Your Contact list{'  '}
                {'( ' +
                  companyFormState.companyDetailsListArray.employees.length +
                  ' )'}
              </Text>
            </View>
            <View
              style={{ height: 1, width: '100%', backgroundColor: 'grey' }}
            />
          </View>
          <View style={{ flex: 4, backgroundColor: '#ffff' }}>
            <FlatList
              data={companyFormState.companyDetailsListArray.employees}
              renderItem={({ item, index }) => this.employeesList(item, index)}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return state;
};
const mapDispatchToProps = {
  getCompanyList,
  updateCompanyState,
  getCompanyDetails,
  deleteCompany
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CompanyContactDetails);

const styles = StyleSheet.create({
  headerStyle: {
    flex: 3,
    flexDirection: 'row',
    paddingTop: 10
  },
  contentStyle: {
    flex: 12
  },
  footerStyle: {
    flex: 10,
    flexDirection: 'row',
    paddingTop: 80,
    borderColor: '#dddd'
  },
  circle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    height: 80,
    width: 80,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#ffff',
    resizeMode: 'contain'
  },
  circleCallImage: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    height: 60,
    width: 60,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#fff',
    backgroundColor: '#ffff'
  },
  circleImage: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 70,
    width: 70,
    overflow: 'hidden',
    borderRadius: 50
  },
  text: {
    color: '#ffff',
    fontSize: 18,
    fontWeight: 'bold'
  },
  textBtn: {
    color: '#dddd',
    fontSize: 13,
    fontWeight: '400'
  },
  textBtnActive: {
    color: '#ffff',
    fontSize: 13,
    fontWeight: '400'
  },
  btnActive: {
    flex: 1,
    backgroundColor: '#00b2e2',
    fontSize: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  btn: {
    flex: 1,
    backgroundColor: '#ffff',
    fontSize: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textNormal: {
    color: '#ffff',
    fontSize: 14,
    fontWeight: '500'
  },
  textMail: {
    color: '#ffff',
    fontSize: 14,
    fontWeight: 'bold'
  }
});
