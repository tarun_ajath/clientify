import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
  Linking,
  Platform
} from 'react-native';
import axios from 'axios';
import Spacer from '../component/common/Spacer';
import Icons from '../component/common/Icons';
import BackButtonHandler from '../utilities/BackButtonHandler';
import DealDetailsAssociatedData from '../component/DealDetailsAssociatedData';
import { NavigationEvents } from 'react-navigation';
import ContactProfileTimline from '../component/ContactProfileTimline';
import DealDetailsAdditionalInfo from '../component/DealDetailsAdditionalInfo';
import DataStore from '../utilities/DataStore';
import { connect } from 'react-redux';
import {
  getDealDetailsData,
  updateDealDetialsData
} from '../Actions/DealDetailsActions';
import apiConfig from '../config/api';
import Container from '../component/common/Container';

class DealsDetailsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      x: 1,
      pipelineStageName: 'loading...'
    };
  }
  componentDidMount() {
    const { ToastProvider } = this.props.screenProps;
    BackButtonHandler.mount(
      true,
      this.props.navigation,
      ToastProvider.showToast
    );
  }

  componentWillUnmount() {
    BackButtonHandler.unmount();
  }

  renderScreens() {
    if (this.state.x == 1) {
      return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <ContactProfileTimline
            newTimlineListArray={this.props.dealDetailsState.dealDetailsArray}
          />
        </ScrollView>
      );
    } else if (this.state.x == 2) {
      return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <DealDetailsAssociatedData
            newListArray={this.props.dealDetailsState.dealDetailsArray}
          />
        </ScrollView>
      );
    } else if (this.state.x == 3) {
      return (
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <DealDetailsAdditionalInfo
            newListArray={this.props.dealDetailsState.dealDetailsArray}
          />
        </ScrollView>
      );
    }
  }

  checkNull(ArrayElement, arrayType) {
    if (ArrayElement == null || ArrayElement == '') {
      if (arrayType == 'emails') alert('there is no email');
      if (arrayType == 'phones') alert('there is no phone');
    } else {
      if (arrayType == 'emails') {
        Linking.openURL(
          'mailto:' + ArrayElement[0].email + '?subject=abcdefg&body=body'
        );
      } else if (arrayType == 'phones') {
      }
    }
  }
  componentDidUpdate() {
    const { dealDetailsState } = this.props;
    this.Hey(dealDetailsState.dealDetailsArray.pipeline_stage);
  }

  async Hey(stage) {
    let pipelineStageUrl = stage
      .split('?')[0]
      .split('http://staging2.clientify.net/v1/')[1];
    try {
      const resp = await axios.get(apiConfig.url + pipelineStageUrl, {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Token ' + DataStore.token
        }
      });
      this.setState({
        pipelineStageName: resp.data.name
      });
    } catch (error) {
      console.log('error only ', error);
    }
  }

  render() {
    const { dealDetailsState } = this.props;
    return (
      <Container>
        <View style={{ flex: 1 }}>
          <View
            style={{ flex: 3.5, backgroundColor: '#00b2e2', paddingBottom: 25 }}
          >
            <View style={styles.headerStyle}>
              <NavigationEvents
                onWillFocus={() => {
                  this.props.updateDealDetialsData('dealDetailsArray', []);
                  setTimeout(() => {
                    this.props.getDealDetailsData(
                      dealDetailsState.dealIdOfData
                    );
                  }, 100);
                }}
              />
              <TouchableOpacity
                style={{ flex: 1 }}
                onPress={() => {
                  const isComeFromDeal = this.props.navigation.state.params
                    .isComeFromDeal;
                  console.log('on dealdetails page  ', isComeFromDeal);
                  if (isComeFromDeal) {
                    this.props.navigation.navigate('DealsForm');
                  } else {
                    this.props.navigation.navigate('Profile');
                  }
                }}
              >
                <View style={{ flex: 1, paddingLeft: 10 }}>
                  {Icons.profile_back()}
                </View>
              </TouchableOpacity>
              <View style={{ flex: 4 }} />
              <TouchableOpacity
                style={{ flex: 1 }}
                onPress={() => {
                  this.props.navigation.navigate('AddDealScreen', {
                    isComeFromDeal: false,
                    dealInfo: dealDetailsState.dealDetailsArray,
                    type: 'DealDetail'
                  });
                }}
              >
                <View style={{ flex: 1, paddingRight: 10 }}>
                  {Icons.profile_edit()}
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{ flex: 1 }}
                onPress={() => {
                  alert('delete button');
                }}
              >
                <View style={{ flex: 1, paddingRight: 7 }}>
                  {Icons.profile_delete()}
                </View>
              </TouchableOpacity>
            </View>
            <Spacer />
            <Spacer />
            <View style={styles.contentStyle}>
              <View style={{ flex: 10, alignItems: 'center' }}>
                <View
                  style={{ justifyContent: 'center', alignItems: 'center' }}
                >
                  <Text style={styles.text}>
                    {dealDetailsState.dealDetailsArray == ''
                      ? 'loading...'
                      : dealDetailsState.dealDetailsArray.name}
                  </Text>
                  <Text style={styles.textMail}>
                    {dealDetailsState.dealDetailsArray == ''
                      ? 'loading...'
                      : dealDetailsState.dealDetailsArray.amount}
                  </Text>
                  <View
                    style={{
                      flexDirection: 'row',
                      borderColor: 'grey',
                      borderWidth: 1,
                      borderRadius: 30,
                      marginTop: 5,
                      padding: 3,
                      backgroundColor: '#008cb3'
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 13,
                        fontFamily: 'Raleway-Medium',
                        paddingLeft: 15,
                        paddingRight: 20,
                        color: '#fff'
                      }}
                    >
                      {this.state.pipelineStageName}
                    </Text>

                    {Icons.deal_tag()}
                  </View>
                </View>
              </View>
            </View>
            <Spacer />
            <View
              style={{
                flex: 10,
                flexDirection: 'row',
                paddingTop: 30,
                borderColor: '#dddd'
              }}
            >
              <View style={{ flex: 2 }} />
              <View
                onStartShouldSetResponder={() => {
                  this.props.navigation.navigate('AddNoteScreen', {
                    isComeFrom: 2,
                    Id: dealDetailsState.dealDetailsArray.id
                  });
                }}
                style={{ flex: 3, alignItems: 'center' }}
              >
                {Icons.profile_note()}
                <View style={{ paddingTop: 5 }}>
                  <Text style={styles.textNormal}>Note</Text>
                </View>
              </View>
              <View style={{ flex: 3, alignItems: 'center' }}>
                <TouchableOpacity onPress={() => {}}>
                  {Icons.profile_task()}
                </TouchableOpacity>
                <View style={{ paddingTop: 5 }}>
                  <Text style={styles.textNormal}>Task</Text>
                </View>
              </View>
              <View
                // onStartShouldSetResponder={() => {
                //   this.props.navigation.navigate("AddEvents", {
                //     isComeFrom: 2
                //   });
                // }}
                style={{ flex: 3, alignItems: 'center' }}
              >
                <TouchableOpacity
                  onPress={() => {
                    {
                      this.props.navigation.navigate('AddEvents', {
                        isComeFrom: 'deal'
                      });
                    }
                  }}
                >
                  {Icons.profile_event()}
                </TouchableOpacity>
                <View style={{ paddingTop: 5 }}>
                  <Text style={styles.textNormal}>Event</Text>
                </View>
              </View>
              <View style={{ flex: 2 }} />
            </View>
          </View>

          <View>
            <View
              style={{
                height: 50,
                flexDirection: 'row',
                borderBottomColor: '#eeee'
              }}
            >
              <View
                onStartShouldSetResponder={() => {
                  this.setState({
                    x: 1
                  });
                }}
                style={this.state.x == 1 ? styles.btnActive : styles.btn}
              >
                <Text
                  style={
                    this.state.x == 1 ? styles.textBtnActive : styles.textBtn
                  }
                >
                  Timeline
                </Text>
              </View>
              <View
                style={{ height: '100%', width: 1, backgroundColor: 'grey' }}
              />
              <View
                onStartShouldSetResponder={() => {
                  this.setState({
                    x: 2
                  });
                }}
                style={
                  (style = this.state.x == 2 ? styles.btnActive : styles.btn)
                }
              >
                <Text
                  style={
                    this.state.x == 2 ? styles.textBtnActive : styles.textBtn
                  }
                >
                  Associated{'\n'} {'   '} Data
                </Text>
              </View>
              <View
                style={{ height: '100%', width: 1, backgroundColor: 'grey' }}
              />
              <View
                onStartShouldSetResponder={() => {
                  this.setState({
                    x: 3
                  });
                }}
                style={
                  (style = this.state.x == 3 ? styles.btnActive : styles.btn)
                }
              >
                <Text
                  style={
                    this.state.x == 3 ? styles.textBtnActive : styles.textBtn
                  }
                >
                  Additional{'\n'} {'    '} info
                </Text>
              </View>
            </View>
            <View
              style={{ height: 1, width: '100%', backgroundColor: 'grey' }}
            />
          </View>
          <View style={{ flex: 4, backgroundColor: '#ffff' }}>
            {this.renderScreens()}
          </View>
        </View>
      </Container>
    );
  }
}
const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = {
  getDealDetailsData,
  updateDealDetialsData
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DealsDetailsScreen);

const styles = StyleSheet.create({
  headerStyle: {
    flex: 3,
    flexDirection: 'row',
    paddingTop: 10
  },
  contentStyle: {
    flex: 12
  },
  footerStyle: {
    flex: 10,
    flexDirection: 'row',
    paddingTop: 30,
    borderColor: '#dddd'
  },
  circle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    height: 80,
    width: 80,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#ffff',
    resizeMode: 'contain'
  },
  circleImage: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 70,
    width: 70,
    overflow: 'hidden',
    borderRadius: 50
  },
  text: {
    color: '#ffff',
    fontSize: 19,
    fontFamily: 'Raleway-SemiBold'
  },
  textBtn: {
    color: 'grey',
    fontSize: 13,
    fontFamily: 'Montserrat-Medium',
    alignItems: 'center'
  },
  textBtnActive: {
    color: '#fff',
    fontSize: 13,
    fontFamily: 'Montserrat-Medium',
    alignItems: 'center'
  },
  btnActive: {
    flex: 1,
    backgroundColor: '#00b2e2',
    fontSize: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  btn: {
    flex: 1,
    backgroundColor: '#ffff',
    fontSize: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textNormal: {
    color: '#ffff',
    fontSize: 14,
    fontFamily: 'Raleway-SemiBold'
  },
  textMail: {
    color: '#ffff',
    fontSize: 15,
    fontFamily: 'Raleway-Regular'
  }
});
