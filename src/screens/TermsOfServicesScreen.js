import React, { Component } from "react";
import { View, WebView } from "react-native";
import ComponentContainer from "../component/common/ComponentContainer";
import BackButtonHandler from "../utilities/BackButtonHandler";
import Header from "../component/common/Header";
import Icons from "../component/common/Icons";

class TermsOfServicesScreen extends Component {
  componentDidMount() {
    const { ToastProvider } = this.props.screenProps;
    BackButtonHandler.mount(
      true,
      this.props.navigation,
      ToastProvider.showToast
    );
  }
  componentWillUnmount() {
    BackButtonHandler.unmount();
  }
  goToSetting = () => {
    this.props.navigation.navigate("SettingForm");
  };

  goToHome = () => {
    this.props.navigation.navigate("SettingForm");
  };

  render() {
    return (
      <ComponentContainer>
        <Header
          titleName="Terms Of Services"
          leftTitleImg={Icons.profile_back}
          RightTitleImg={Icons.setting_active()}
          onPress={this.goToSetting}
          onPressBack={this.goToHome}
        />
        <WebView
          source={{
            uri: " https://clientify.com/condiciones-generales-uso/?lang=en" //https://clientify.com/condiciones-generales-uso/  for Spansh
          }}
        />
      </ComponentContainer>
    );
  }
}

export default TermsOfServicesScreen;
