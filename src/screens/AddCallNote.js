import React, { Component } from 'react';
import {
  View,
  TextInput,
  Text,
  Dimensions,
  ImageBackground
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import Container from '../component/common/Container';
import Header from '../component/common/Header';
import ButtonBox from '../component/common/ButtonBox';
import Spacer from '../component/common/Spacer';
import Icons from '../component/common/Icons';
import Icon from 'react-native-vector-icons/FontAwesome5';

const { height } = Dimensions.get('window');

class AddCallNote extends Component {
  constructor() {
    super();
    this.state = {
      noAnsCheck: false,
      busyCheck: false,
      wrongNoCheck: false,
      leftVoiceCallCheck: false,
      connectedCheck: false
    };
  }
  render() {
    return (
      <Container>
        <Header
          titleName="Add Call Note"
          leftTitleImg={Icons.profile_back}
          RightTitleImg={Icons.notification_light()}
        />

        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <View style={{ flex: 1, padding: 20, height: '85%' }}>
            <View style={{ paddingLeft: 5, paddingRight: 5 }}>
              <Text style={{ color: 'grey' }}>Register a call</Text>

              <View
                style={{
                  marginTop: 5,
                  flexDirection: 'row'
                }}
              >
                <View
                  style={{
                    flex: 1,
                    marginRight: 5,
                    borderRadius: 30,
                    borderWidth: 1,
                    borderColor: 'grey'
                  }}
                >
                  <ButtonBox
                    btnText="Add a note"
                    btnColor="white"
                    btnTextColor="#000"
                    btnImage={Icons.add}
                    // onPress={props.go}
                  />
                </View>
                <View
                  style={{
                    flex: 1,
                    marginLeft: 5,
                    borderRadius: 30,
                    borderWidth: 1,
                    borderColor: 'grey'
                  }}
                >
                  <ButtonBox
                    btnText="Add a task "
                    btnColor="white"
                    btnTextColor="#000"
                    btnImage={Icons.add}
                    // onPress={props.go}
                  />
                </View>
              </View>
            </View>
            <Spacer size={8} />

            <Text style={{ color: 'grey', paddingLeft: 5 }}>
              Result of the call
            </Text>

            <View
              onStartShouldSetResponder={() => {
                this.setState({
                  noAnsCheck: true,
                  busyCheck: false,
                  wrongNoCheck: false,
                  leftVoiceCallCheck: false,
                  connectedCheck: false
                });
              }}
              style={{
                flexDirection: 'row',
                padding: 5,
                marginTop: 10,
                height: 40
              }}
            >
              <View
                style={{
                  flex: 3
                }}
              >
                <Text style={{ color: '#000', fontSize: 16 }}>No answer</Text>
              </View>
              <View
                style={{
                  flex: 1
                }}
              />
              <View
                style={{
                  flex: 3,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center'
                }}
              >
                <Icon
                  name={this.state.noAnsCheck ? 'check' : null}
                  size={20}
                  color="#00b2e2"
                />
              </View>
            </View>
            <View
              style={{ height: 1, width: '100%', backgroundColor: '#000' }}
            />

            <View
              onStartShouldSetResponder={() => {
                this.setState({
                  noAnsCheck: false,
                  busyCheck: true,
                  wrongNoCheck: false,
                  leftVoiceCallCheck: false,
                  connectedCheck: false
                });
              }}
              style={{
                flexDirection: 'row',
                padding: 5,
                marginTop: 10,
                height: 40
              }}
            >
              <View
                style={{
                  flex: 3
                }}
              >
                <Text style={{ color: '#000', fontSize: 16 }}>Busy</Text>
              </View>
              <View
                style={{
                  flex: 1
                }}
              />
              <View
                style={{
                  flex: 3,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center'
                }}
              >
                <Icon
                  name={this.state.busyCheck ? 'check' : null}
                  size={20}
                  color="#00b2e2"
                />
              </View>
            </View>
            <View
              style={{ height: 1, width: '100%', backgroundColor: '#000' }}
            />

            <View
              onStartShouldSetResponder={() => {
                this.setState({
                  noAnsCheck: false,
                  busyCheck: false,
                  wrongNoCheck: true,
                  leftVoiceCallCheck: false,
                  connectedCheck: false
                });
              }}
              style={{
                flexDirection: 'row',
                padding: 5,
                marginTop: 10,
                height: 40
              }}
            >
              <View
                style={{
                  flex: 3
                }}
              >
                <Text style={{ color: '#000', fontSize: 16 }}>
                  Wrong number
                </Text>
              </View>
              <View
                style={{
                  flex: 2
                }}
              />
              <View
                style={{
                  flex: 3,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center'
                }}
              >
                <Icon
                  name={this.state.wrongNoCheck ? 'check' : null}
                  size={20}
                  color="#00b2e2"
                />
              </View>
            </View>
            <View
              style={{ height: 1, width: '100%', backgroundColor: '#000' }}
            />

            <View
              onStartShouldSetResponder={() => {
                this.setState({
                  noAnsCheck: false,
                  busyCheck: false,
                  wrongNoCheck: false,
                  leftVoiceCallCheck: true,
                  connectedCheck: false
                });
              }}
              style={{
                flexDirection: 'row',
                padding: 5,
                marginTop: 10,
                height: 40
              }}
            >
              <View
                style={{
                  flex: 3
                }}
              >
                <Text style={{ color: '#000', fontSize: 16 }}>
                  Left a voice call
                </Text>
              </View>
              <View
                style={{
                  flex: 2
                }}
              />
              <View
                style={{
                  flex: 3,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center'
                }}
              >
                <Icon
                  name={this.state.leftVoiceCallCheck ? 'check' : null}
                  size={20}
                  color="#00b2e2"
                />
              </View>
            </View>
            <View
              style={{ height: 1, width: '100%', backgroundColor: '#000' }}
            />

            <View
              onStartShouldSetResponder={() => {
                this.setState({
                  noAnsCheck: false,
                  busyCheck: false,
                  wrongNoCheck: false,
                  leftVoiceCallCheck: false,
                  connectedCheck: true
                });
              }}
              style={{
                flexDirection: 'row',
                padding: 5,
                marginTop: 10,
                height: 40
              }}
            >
              <View
                style={{
                  flex: 3
                }}
              >
                <Text style={{ color: '#000', fontSize: 16 }}>connected</Text>
              </View>
              <View
                style={{
                  flex: 2
                }}
              />
              <View
                style={{
                  flex: 3,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center'
                }}
              >
                <Icon
                  name={this.state.connectedCheck ? 'check' : null}
                  size={20}
                  color="#00b2e2"
                />
              </View>
            </View>
            <View
              style={{ height: 1, width: '100%', backgroundColor: '#000' }}
            />
          </View>

          <View
            style={{
              height: '15%',
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <View
              style={{
                width: '40%'
              }}
            >
              <ButtonBox
                // btnImage={Icons.google}
                btnText="Skip"
                btnColor="#f5921e"
                // onPress={props.go}
                btnTextColor="#fff"
              />
            </View>
          </View>
        </ScrollView>
      </Container>
    );
  }
}
export default AddCallNote;

// import React, { Component } from 'react';
// import {
//   View,
//   TextInput,
//   Text,
//   Dimensions,
//   ImageBackground
// } from 'react-native';
// import InputBox from '../component/common/InputBox';
// import { ScrollView } from 'react-native-gesture-handler';
// import Container from '../component/common/Container';
// import Header from '../component/common/Header';
// import ButtonBox from '../component/common/ButtonBox';
// import Spacer from '../component/common/Spacer';
// import Icons from '../component/common/Icons';

// const { height } = Dimensions.get('window');

// class AddCallNote extends Component {
//   render() {
//     return (
//       <Container>
//         <Header
//           titleName="Add Call Note"
//           leftTitleImg={Icons.profile_back}
//           RightTitleImg={Icons.notification_light()}
//         />

//         <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
//           <View style={{ flex: 1, padding: 20 }}>
//             <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
//               <View style={{ flex: 3 }}>
//                 <Text> Call</Text>
//               </View>
//               <View style={{ flex: 0.5 }}>
//                 <Text> : </Text>
//               </View>
//               <View style={{ flex: 4 }}>
//                 <Text> Pankaj Rathour</Text>
//               </View>
//             </View>

//             <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
//               <View style={{ flex: 3 }}>
//                 <Text> Date</Text>
//               </View>
//               <View style={{ flex: 0.5 }}>
//                 <Text> : </Text>
//               </View>
//               <View style={{ flex: 4 }}>
//                 <Text> 10-Jan-2018</Text>
//               </View>
//             </View>

//             <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
//               <View style={{ flex: 3 }}>
//                 <Text> Time</Text>
//               </View>
//               <View style={{ flex: 0.5 }}>
//                 <Text> : </Text>
//               </View>
//               <View style={{ flex: 4 }}>
//                 <Text> 02:00 am</Text>
//               </View>
//             </View>

//             <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
//               <View style={{ flex: 3, paddingTop: 15 }}>
//                 <Text> Outcome</Text>
//               </View>
//               <View style={{ flex: 4 }}>
//                 <InputBox placeholder="outcome" />
//               </View>
//             </View>

//             <Text> Comment </Text>
//             <TextInput
//               multiline={true}
//               style={{
//                 borderWidth: 1,
//                 height: height / 2.5,
//                 textAlignVertical: 'top',
//                 padding: 15
//               }}
//             />

//             <Spacer size={12} />
//             <View
//               style={{
//                 marginRight: 15,
//                 marginLeft: 15,
//                 flexDirection: 'row'
//               }}
//             >
//               <View
//                 style={{
//                   flex: 1,
//                   marginRight: 5,
//                   borderRadius: 30,
//                   borderWidth: 1,
//                   borderColor: 'grey'
//                 }}
//               >
//                 <ButtonBox
//                   btnText="Cancel"
//                   btnColor="white"
//                   btnTextColor="#000"
//                   // onPress={props.go}
//                 />
//               </View>
//               <View style={{ flex: 1, marginLeft: 5 }}>
//                 <ButtonBox
//                   btnText="Save "
//                   btnColor="#f5921e"
//                   btnTextColor="#fff"
//                   // onPress={props.go}
//                 />
//               </View>
//             </View>
//           </View>
//         </ScrollView>
//       </Container>
//     );
//   }
// }
// export default AddCallNote;
