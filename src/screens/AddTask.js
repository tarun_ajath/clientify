import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  Dimensions,
  TouchableOpacity,
  Modal
} from 'react-native';
import InputBox from '../component/common/InputBox';
import { ScrollView } from 'react-native-gesture-handler';
import Container from '../component/common/Container';
import Header from '../component/common/Header';
import ButtonBox from '../component/common/ButtonBox';
import Spacer from '../component/common/Spacer';
import Icons from '../component/common/Icons';
import Picker_dropdown from '../component/common/Picker_dropdown';
import { connect } from 'react-redux';
import { addTaskSubmit, updateAddTaskState } from '../Actions/addTaskAction';
import DateTimePicker from 'react-native-modal-datetime-picker';
import CompanyForm from '../component/CompanyForm';
import DealList from '../component/common/DealList';
import BackButtonHandler from '../utilities/BackButtonHandler';

class AddTask extends Component {
  constructor() {
    super();
    this.state = {
      typeList: [
        { name: 'Phone call', id: 1 },
        { name: 'Send email', id: 2 },
        { name: 'Contact', id: 3 },
        { name: 'Others', id: 4 }
      ],
      statusList: [
        { name: 'Not started', id: 1 },
        { name: 'In progress', id: 2 },
        { name: 'Deferred', id: 3 },
        { name: 'Waiting', id: 4 },
        { name: 'Completed', id: 5 },
        { name: 'Expired', id: 6 }
      ],
      expectedstartDate: 'YYYY-MM-DD',
      expectedstartDateVisible: false,
      modalVisibleCompany: false,
      modalVisibleDeal: false,
      addedDealsList: [],
      addedCompanyList: []
    };
  }

  componentDidMount() {
    const { ToastProvider } = this.props.screenProps;
    BackButtonHandler.mount(
      true,
      this.props.navigation,
      ToastProvider.showToast
    );

    const actionInfo = this.props.navigation.state.params.activityDetails;
    console.log('activity info  ', actionInfo);
    if (this.props.navigation.state.params.isUpdateActivity) {
      console.log('asa ');
      // this.firstArrayElement(actionInfo.name, 'name');
      // this.firstArrayElement(actionInfo.amount, 'due_date');
      // this.firstArrayElement(actionInfo.contact, 'type');
      // this.firstArrayElement(actionInfo.contact, 'status');
      // this.firstArrayElement(actionInfo.company, 'assigned_to');
      // this.firstArrayElement(actionInfo.pipeline, 'deals');
      // this.firstArrayElement(actionInfo.pipeline_stage, 'related_companies');
    }
  }

  componentWillUnmount() {
    BackButtonHandler.unmount();
  }

  submitAddTAsk = () => {
    const { ToastProvider } = this.props.screenProps;
    // const dealIt = this.props.addTaskState.deals.url.split('?')[0].toString();
    // const dealListItem = this.state.addedDealsList.push(dealIt);
    // const companyListItem = this.state.addedCompanyList.push(
    //   this.props.addTaskState.related_companies.url.split('?')[0]
    // );
    // if (
    //   this.props.addTaskState.name == '' ||
    //   this.props.addTaskState.due_date == 'YYYY/MM/DD' ||
    //   this.props.addTaskState.type == '' ||
    //   this.props.addTaskState.status == '' ||
    //   this.props.addTaskState.assigned_to == '' ||
    //   this.props.addTaskState.deals == '' ||
    //   this.props.addTaskState.related_companies == ''
    // ) {
    //   alert('please fill all the details');
    // } else {
    const reqBody = {
      name: this.props.addTaskState.name,
      due_date: this.props.addTaskState.due_date,
      type: this.props.addTaskState.type,
      status: this.props.addTaskState.status,
      assigned_to: this.props.addTaskState.assigned_to,
      deals: ['http://staging2.clientify.net/v1/deals/644/?format=api'],
      related_companies: [
        'http://staging2.clientify.net/v1/companies/38196/?format=api'
      ]
    };
    console.log('rq body', reqBody);
    this.props.addTaskSubmit(reqBody, ToastProvider.showToast, 'activity');
    // }
  };

  _showExpectedstartDate = () =>
    this.setState({ expectedstartDateVisible: true });
  _hideExpectedstartDateVisible = () =>
    this.setState({ expectedstartDateVisible: false });
  _handleExpectedstartDate = date => {
    const endDate = date.getDate().toString();
    // const endMonth = months[date.getMonth()];
    const endMonth = date.getMonth() + 1;
    const endYear = date.getFullYear().toString();
    this.setState({
      expectedstartDate: endYear + '-' + endMonth + '-' + endDate
    });
    this.props.updateAddTaskState('due_date', date.toISOString());
    console.log('isoformate date ', date.toISOString());
    this._hideExpectedstartDateVisible();
  };

  goToHome = () => {
    const { navigation } = this.props;
    const isComeFrom = navigation.getParam('isComeFrom');
    if (isComeFrom == 'profile') {
      this.props.navigation.navigate('Profile');
    } else if (isComeFrom == 'activity') {
      this.props.navigation.navigate('ActivitiesForm');
    }
  };

  closeModelFromCompany = item => {
    this.setState({ modalVisibleCompany: false });
    this.props.updateAddTaskState('related_companies', item);
  };

  dealsInfo = item => {
    this.setState({
      modalVisibleDeal: false
    });
    this.props.updateAddTaskState('deals', item);
    console.log('deal Info ', item);
  };

  render() {
    // console.log('prop on task', this.props);
    return (
      <Container>
        <View style={{ flex: 1 }}>
          <Header
            titleName="Add Task"
            leftTitleImg={Icons.profile_back}
            RightTitleImg={Icons.notification_light()}
            onPressBack={this.goToHome}
          />
          <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisibleCompany}
              onRequestClose={() => {
                this.setState({ modalVisibleCompany: false });
              }}
              BackButtonHandler={true}
            >
              <Header
                titleName="Company Modal"
                leftTitleImg={Icons.notification_light()}
                RightTitleImg={Icons.notification_light()}
              />
              <View style={{ flex: 9 }}>
                <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                  <View
                    style={{ flex: 9, paddingTop: 10, flexDirection: 'row' }}
                  >
                    <CompanyForm
                      isComeFromModel={false}
                      isComeFromEvent={false}
                      isComeFromTask={true}
                      closeModel={this.closeModelFromCompany}
                    />
                  </View>
                </ScrollView>
              </View>
            </Modal>

            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisibleDeal}
              onRequestClose={() => {
                this.setState({ modalVisibleDeal: false });
              }}
              BackButtonHandler={true}
            >
              <Header
                titleName="Deal Modal"
                leftTitleImg={Icons.notification_light()}
                RightTitleImg={Icons.notification_light()}
              />
              <View style={{ flex: 9 }}>
                <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                  <View
                    style={{ flex: 9, paddingTop: 10, flexDirection: 'row' }}
                  >
                    <DealList dealsInfo={this.dealsInfo} />
                  </View>
                </ScrollView>
              </View>
            </Modal>

            <View
              style={{
                marginTop: 15
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={{
                      color: '#000',
                      alignItems: 'center',
                      fontSize: 14
                    }}
                  >
                    Name
                  </Text>
                </View>
                <View
                  style={{
                    flex: 2,
                    height: 40,
                    padding: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: 15
                  }}
                >
                  <InputBox
                    placeholder="Enter Name"
                    value={this.props.addTaskState.name}
                    name="name"
                    updateLocalState={this.props.updateAddTaskState}
                  />
                </View>
              </View>
              <Spacer />
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={{
                      color: '#000',
                      alignItems: 'center',
                      fontSize: 14
                    }}
                  >
                    Due Date
                  </Text>
                </View>
                <View
                  style={{
                    flex: 2,
                    height: 48,
                    borderColor: 'grey',
                    borderRadius: 40,
                    borderWidth: 2,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: 15
                  }}
                >
                  <TouchableOpacity
                    style={{
                      flex: 1,
                      flexDirection: 'row'
                    }}
                    onPress={this._showExpectedstartDate}
                  >
                    <View
                      style={{
                        flex: 3,
                        paddingLeft: 10,
                        justifyContent: 'center'
                      }}
                    >
                      <Text
                        style={{ color: 'grey', fontSize: 13, paddingLeft: 15 }}
                      >
                        {this.state.expectedstartDate}
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center'
                      }}
                    >
                      {Icons.calender()}
                    </View>
                  </TouchableOpacity>
                  <DateTimePicker
                    mode="date"
                    minimumDate={new Date()}
                    isVisible={this.state.expectedstartDateVisible}
                    onConfirm={this._handleExpectedstartDate}
                    onCancel={this._hideExpectedstartDateVisible}
                    datePickerModeAndroid={'spinner'}
                  />
                </View>
              </View>
              <Spacer />
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={{
                      color: '#000',
                      alignItems: 'center',
                      fontSize: 14
                    }}
                  >
                    Type
                  </Text>
                </View>
                <View
                  style={{
                    flex: 2,
                    height: 48,
                    borderColor: 'grey',
                    borderRadius: 40,
                    borderWidth: 2,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: 15
                  }}
                >
                  <Picker_dropdown
                    pickerArrayData={this.state.typeList || []}
                    labelKey={'name'}
                    labelValue={'id'}
                    pickerName={'type'}
                    updateLocalState={this.props.updateAddTaskState}
                  />
                </View>
              </View>
              <Spacer />
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={{
                      color: '#000',
                      alignItems: 'center',
                      fontSize: 14
                    }}
                  >
                    Status
                  </Text>
                </View>
                <View
                  style={{
                    flex: 2,
                    height: 48,
                    borderColor: 'grey',
                    borderRadius: 40,
                    borderWidth: 2,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: 15
                  }}
                >
                  <Picker_dropdown
                    pickerArrayData={this.state.statusList || []}
                    labelKey={'name'}
                    labelValue={'id'}
                    pickerName={'status'}
                    updateLocalState={this.props.updateAddTaskState}
                  />
                </View>
              </View>
              <Spacer />
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={{
                      color: '#000',
                      alignItems: 'center',
                      fontSize: 14
                    }}
                  >
                    Assigned to
                  </Text>
                </View>
                <View
                  style={{
                    flex: 2,
                    height: 40,
                    padding: 0,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: 15
                  }}
                >
                  <InputBox
                    placeholder="Assigned to"
                    value={this.props.addTaskState.assigned_to}
                    name="assigned_to"
                    updateLocalState={this.props.updateAddTaskState}
                  />
                </View>
              </View>
              <Spacer />
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={{
                      color: '#000',
                      alignItems: 'center',
                      fontSize: 14
                    }}
                  >
                    Deals
                  </Text>
                </View>
                <View
                  onStartShouldSetResponder={() => {
                    this.setState({
                      modalVisibleDeal: true
                    });
                  }}
                  style={{
                    flex: 2,
                    height: 48,
                    borderColor: 'grey',
                    borderRadius: 40,
                    borderWidth: 2,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: 15
                  }}
                >
                  <Text
                    style={{
                      color: '#7f7f7f',
                      alignItems: 'center'
                    }}
                  >
                    {this.props.addTaskState.deals == ''
                      ? 'select here'
                      : this.props.addTaskState.deals.name}
                  </Text>
                </View>
              </View>

              <Spacer />
              <View
                style={{
                  flexDirection: 'row',
                  marginLeft: 15,
                  marginRight: 15,
                  marginBottom: 8
                }}
              >
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'flex-start'
                  }}
                >
                  <Text
                    style={{
                      color: '#000',
                      alignItems: 'center',
                      fontSize: 14
                    }}
                  >
                    Related Company
                  </Text>
                </View>
                <View
                  onStartShouldSetResponder={() => {
                    this.setState({
                      modalVisibleCompany: true
                    });
                  }}
                  style={{
                    flex: 2,
                    height: 48,
                    borderColor: 'grey',
                    borderRadius: 40,
                    borderWidth: 2,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: 15
                  }}
                >
                  <Text
                    style={{
                      color: '#7f7f7f',
                      alignItems: 'center'
                    }}
                  >
                    {this.props.addTaskState.related_companies == ''
                      ? 'select here'
                      : this.props.addTaskState.related_companies.name}
                  </Text>
                </View>
              </View>
              <Spacer size={20} />

              <Spacer />
              <View
                style={{
                  marginRight: 15,
                  marginLeft: 15,
                  flexDirection: 'row'
                }}
              >
                <View
                  style={{
                    flex: 1,
                    marginRight: 5,
                    borderRadius: 30,
                    borderWidth: 1,
                    borderColor: 'grey'
                  }}
                >
                  <ButtonBox
                    btnText="Cancel"
                    btnColor="white"
                    btnTextColor="#000"
                    onPress={this.goToHome}
                  />
                </View>
                <View style={{ flex: 1, marginLeft: 5 }}>
                  <ButtonBox
                    btnText="Save "
                    btnColor="#f5921e"
                    btnTextColor="#fff"
                    onPress={this.submitAddTAsk}
                  />
                </View>
              </View>
              <Spacer size={20} />
            </View>
          </ScrollView>
        </View>
      </Container>
    );
  }
}
const mapStateToProps = state => {
  return state;
};
const mapDispatchToProps = {
  addTaskSubmit,
  updateAddTaskState
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddTask);
