import React, { Component } from 'react';
import SplashScreen from 'react-native-splash-screen';
import { View, ImageBackground, ActivityIndicator, Text } from 'react-native';
import DataStore from '../utilities/DataStore';

class FirstScreen extends Component {
  constructor() {
    super();
    this.checkLogin = this.checkLogin.bind(this);
  }
  async componentDidMount() {
    await this.checkLogin();
    SplashScreen.hide();
  }

  async checkLogin() {
    let tokenString = await DataStore.get('token');
    const userToken = JSON.parse(tokenString);

    setTimeout(() => {
      if (userToken) {
        DataStore.token = userToken;
        this.props.navigation.navigate('Home');
      } else {
        this.props.navigation.navigate('Login');
      }
    }, 100);
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={require('../assets/splash.png')}
          style={{
            width: '100%',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <ActivityIndicator size={'large'} color={'#00b2e2'} />
        </ImageBackground>
      </View>
    );
  }
}

export default FirstScreen;
