import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  ActivityIndicator
} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import { BottomNavigation, FAB, Portal } from 'react-native-paper';
import DealsOpenExpired from '../component/DealsOpenExpired';
import DealsWon from '../component/DealsWon';
import DealsLost from '../component/DealsLost';
import Header from '../component/common/Header';
import Icons from '../component/common/Icons';
import Container from '../component/common/Container';
import { connect } from 'react-redux';
import { getDealList, updateDealState } from '../Actions/dealFormAction';
import {
  getDealDetailsData,
  updateDealDetialsData
} from '../Actions/DealDetailsActions';

class DealsForm extends Component {
  constructor() {
    super();
    this.state = {
      x: 1,
      showFab: false,
      open: false
    };
  }
  componentDidMount() {
    this.props.getDealList();
    // console.log('Deals form props are now ' + JSON.stringify(this.props));

    this.props.navigation.addListener('didFocus', () =>
      this.setState({
        showFab: true
      })
    );
    this.props.navigation.addListener('willBlur', () =>
      this.setState({
        showFab: false
      })
    );
  }

  // async Deals() {
  //   let res = await HttpClient.deals();
  //   this.setState({
  //     dealsList: JSON.parse(JSON.stringify(res.data))
  //   });
  //   let respOpenExpired = res.data.filter(item => {
  //     return item['status_desc'] == 'Expired';
  //   });
  //   let respWon = res.data.filter(item => {
  //     return item['status_desc'] == 'Won';
  //   });
  //   let respLost = res.data.filter(item => {
  //     return item['status_desc'] == 'Lost';
  //   });

  //   this.setState({
  //     dealsOpenExpiredList: respOpenExpired,
  //     dealWonList: respWon,
  //     dealLostList: respLost
  //   });
  // }

  fabFunctionDealAdd = () => {
    if (this.state.showFab) {
      return (
        <Portal>
          <FAB.Group
            color={'white'}
            style={{ marginBottom: 55 }}
            open={this.state.open}
            icon={this.state.open ? 'close' : 'add'}
            actions={[
              {
                icon: Icons.deal_custumer_yellow,
                label: 'Add Deal',
                onPress: () =>
                  this.props.navigation.navigate('AddDealScreen', {
                    isComeFromDeal: true,
                    type: 'Deal'
                  })
              }
            ]}
            onStateChange={({ open }) => this.setState({ open })}
            onPress={() => {
              if (this.state.open) {
                // do something if the speed dial is open
                // alert('Helllooooo');
              }
            }}
          />
        </Portal>
      );
    }
  };

  navigateTo = param => {
    this.props.navigation.navigate(param);
  };

  renderHome() {
    const { ToastProvider } = this.props.screenProps;
    const { dealFormState, getCompanyList, dealDetailsState } = this.props;
    if (
      dealFormState.isLoadingDealList &&
      dealDetailsState.isLoadingDealDetails
    ) {
      return (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
            alignSelf: 'center'
          }}
        >
          <ActivityIndicator size="large" color="#00b2e2" animating={true} />
        </View>
      );
    } else {
      let respExpired = dealFormState.dealListArray.filter(item => {
        return item['status_desc'] == 'Expired';
      });
      let respOpen = dealFormState.dealListArray.filter(item => {
        return item['status_desc'] == 'Open';
      });
      let respOpenExpired = [...respExpired, ...respOpen];
      let respWon = dealFormState.dealListArray.filter(item => {
        return item['status_desc'] == 'Won';
      });
      let respLost = dealFormState.dealListArray.filter(item => {
        return item['status_desc'] == 'Lost';
      });

      if (this.state.x == 1) {
        return (
          <DealsOpenExpired
            navigateTo={this.navigateTo}
            getDealDetailsData={this.props.getDealDetailsData}
            dealsOpenExpiredList={respOpenExpired}
            showToast={ToastProvider.showToast}
          />
        );
      } else if (this.state.x == 2) {
        return (
          <DealsWon
            dealWonList={respWon}
            showToast={ToastProvider.showToast}
            getDealDetailsData={this.props.getDealDetailsData}
          />
        );
      } else if (this.state.x == 3) {
        return (
          <DealsLost
            dealLostList={respLost}
            showToast={ToastProvider.showToast}
            getDealDetailsData={this.props.getDealDetailsData}
          />
        );
      }
    }
  }
  goToAddDeal = () => {
    this.props.navigation.navigate('AddDealScreen');
  };
  goToHome = () => {
    this.props.navigation.navigate('ContactStack');
  };

  render() {
    const { dealFormState, getCompanyList, dealDetailsState } = this.props;
    // console.log(
    //   'got deal details Data... ' +
    //     JSON.stringify(dealDetailsState.dealDetailsArray)
    // );
    // console.log('Deal List   ' + JSON.stringify(dealFormState.dealListArray));

    return (
      <Container>
        <View style={{ flex: 10 }}>
          <Header
            titleName="Deals"
            leftTitleImg={Icons.profile_back}
            RightTitleImg={Icons.add_deals()}
            onPress={this.goToAddDeal}
            onPressBack={this.goToHome}
          />
          <View style={{ flex: 0.8, height: 45, flexDirection: 'row' }}>
            <NavigationEvents
              onWillFocus={() => {
                this.props.updateDealState('dealListArray', []);
                setTimeout(() => {
                  this.props.getDealList();
                }, 100);
              }}
            />
            <View
              onStartShouldSetResponder={() => {
                this.setState({
                  x: 1
                });
              }}
              style={this.state.x == 1 ? styles.openbtnActive : styles.openbtn}
            >
              {this.fabFunctionDealAdd()}
              <Text
                style={
                  this.state.x == 1 ? styles.textBtnActive : styles.textBtn
                }
              >
                Open | Expired
              </Text>
            </View>
            <View
              onStartShouldSetResponder={() => {
                this.setState({
                  x: 2
                });
              }}
              style={this.state.x == 2 ? styles.btnActive : styles.btn}
            >
              <Text
                style={
                  this.state.x == 2 ? styles.textBtnActive : styles.textBtn
                }
              >
                Won
              </Text>
            </View>
            <View
              onStartShouldSetResponder={() => {
                this.setState({
                  x: 3
                });
              }}
              style={this.state.x == 3 ? styles.btnActive : styles.btn}
            >
              <Text
                style={
                  this.state.x == 3 ? styles.textBtnActive : styles.textBtn
                }
              >
                Lost
              </Text>
            </View>
          </View>
          <View style={styles.container}>
            <View style={{ flex: 8 }}>{this.renderHome()}</View>
          </View>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return state;
};
const mapDispatchToProps = {
  getDealList,
  updateDealState,
  getDealDetailsData,
  updateDealDetialsData
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DealsForm);

const styles = StyleSheet.create({
  container: {
    flex: 8,
    backgroundColor: '#F5FCFF'
  },
  textBtn: {
    color: 'grey',
    fontSize: 13,
    fontFamily: 'Montserrat-Medium'
  },
  textBtnActive: {
    color: '#fff',
    fontSize: 13,
    fontFamily: 'Montserrat-Medium'
  },
  openbtnActive: {
    flex: 2,
    backgroundColor: '#00b2e2',
    fontSize: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  openbtn: {
    flex: 2,
    backgroundColor: '#ffff',
    fontSize: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  btnActive: {
    flex: 1,
    backgroundColor: '#00b2e2',
    fontSize: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  btn: {
    flex: 1,
    backgroundColor: '#ffff',
    fontSize: 10,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
