import React, { Component } from 'react';
import { View, Image, Text, Dimensions, ImageBackground } from 'react-native';
import BackButtonHandler from '../utilities/BackButtonHandler';
import Container from '../component/common/Container';
import Header from '../component/common/Header';
import Icons from '../component/common/Icons';
import Spacer from '../component/common/Spacer';
import style from '../component/common/styles';
import styles from '../component/common/styles';

const { height } = Dimensions.get('window');

class NotificationScreen extends Component {
  componentDidMount() {
    const { ToastProvider } = this.props.screenProps;
    BackButtonHandler.mount(
      true,
      this.props.navigation,
      ToastProvider.showToast
    );
  }

  componentWillUnmount() {
    BackButtonHandler.unmount();
  }

  render() {
    return (
      <Container>
        <Header
          titleName="Notification"
          leftTitleImg={Icons.notification_light()}
          RightTitleImg={Icons.notification_light}
        />
        {/* {this.notifications()} */}
        <View style={{ flex: 1, paddingTop: 10 }}>
          <View
            style={{
              height: 45,
              flexDirection: 'row',
              // backgroundColor: 'green',
              paddingLeft: 25
            }}
          >
            <View style={{ flex: 1 }}>
              <View
                style={{
                  width: 3,
                  height: '100%',
                  backgroundColor: '#f5921e'
                }}
              />
            </View>
            <View
              style={{
                flex: 8,
                alignItems: 'flex-start'
              }}
            >
              <Text style={[styles.fontPrimary_semiBold, { fontSize: 16 }]}>
                New Assign Task
              </Text>
              <Text style={[styles.fontPrimary_Regular, { fontSize: 12 }]}>
                A new Task has been assigned to you.
              </Text>
            </View>
            <View
              style={{
                alignItems: 'flex-end',
                justifyContent: 'flex-start',
                alignItems: 'center',
                flex: 2,
                paddingRight: 5
              }}
            >
              <Text
                style={[
                  styles.fontPrimary_Regular,
                  { fontSize: 13, alignItems: 'flex-end', color: 'grey' }
                ]}
              >
                9:00AM
              </Text>
            </View>
            {/* <Text style={{ fontSize: 13, paddingLeft: 40 }}>9:00AM</Text> */}
          </View>
          <Spacer />
          <View
            style={{
              width: '100%',
              height: 0.3,
              backgroundColor: '#000'
              // paddingTop: 10
            }}
          />
          <Spacer size={8} />
          <View
            style={{
              height: 45,
              flexDirection: 'row',
              // backgroundColor: 'green',
              paddingLeft: 25
            }}
          >
            <View style={{ flex: 1 }}>
              <View
                style={{
                  width: 3,
                  height: '100%',
                  backgroundColor: '#f5921e'
                }}
              />
            </View>
            <View
              style={{
                flex: 8,
                alignItems: 'flex-start'
              }}
            >
              <Text style={{ fontWeight: 'bold', fontSize: 17 }}>
                Expired Task
              </Text>
              <Text style={{ fontSize: 13 }}>You have an Expired Task.</Text>
            </View>
            <View
              style={{
                alignItems: 'flex-end',
                justifyContent: 'flex-start',
                alignItems: 'center',
                flex: 2,
                paddingRight: 5
              }}
            >
              <Text
                style={{ fontSize: 13, alignItems: 'flex-end', color: 'grey' }}
              >
                yesterday
              </Text>
            </View>
            {/* <Text style={{ fontSize: 13, paddingLeft: 40 }}>9:00AM</Text> */}
          </View>
          <Spacer />
          <View
            style={{
              width: '100%',
              height: 0.3,
              backgroundColor: '#000'
              // paddingTop: 10
            }}
          />
          <Spacer size={8} />
        </View>
        <Spacer />
      </Container>
    );
  }
}
export default NotificationScreen;
