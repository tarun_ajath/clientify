import React, { Component } from 'react';
import {
  View,
  Image,
  StyleSheet,
  Dimensions,
  ImageBackground
} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import HttpClient from '../utilities/HttpClient';
import BackButtonHandler from '../utilities/BackButtonHandler';
import LoginForm from '../component/LoginForm';
import InputBox from '../component/common/InputBox';
import { ScrollView, State } from 'react-native-gesture-handler';
import Container from '../component/common/Container';
import { connect } from 'react-redux';
import { updateLoginState, loginReqSubmit } from '../Actions/loginActions';
import Validations from '../component/common/Validations';

const { height } = Dimensions.get('window');

class LoginScreen extends Component {
  constructor() {
    super();
    this.unsubscriber = null;
    this.state = {
      user: null
    };
  }

  componentDidMount() {
    SplashScreen.hide();
    const { ToastProvider } = this.props.screenProps;
    BackButtonHandler.mount(
      false,
      this.props.navigation,
      ToastProvider.showToast
    );
  }

  componentWillUnmount() {
    BackButtonHandler.unmount();
  }

  validateDataLogin = () => {
    const { loginState, updateLocalState } = this.props;
    const { ToastProvider } = this.props.screenProps;
    this.props.loginReqSubmit();
    // if (
    //   Validations.validateEmail(loginState.email) == true &&
    //   (loginState.password.length <= 7 || loginState.password.length > 15)
    // ) {
    // if (
    //   loginState.email == 'test@test.com' &&
    //   loginState.password == 'test123'
    // ) {
    // ToastProvider.showToast('You successfully logged in.', 'success');
    // updateLocalState('email', 'text');
    // updateLocalState('password', '123');
    // }
    // }
  };

  googleLogin = () => {
    const { ToastProvider } = this.props.screenProps;
    const { updateLocalState } = this.props;
    // updateLocalState('email', 'text');
    // updateLocalState('password', '123');
    // ToastProvider.showToast('You successfully logged in.', 'success');
    this.props.navigation.navigate('Home');
  };

  navigateTo = (page, params) => {
    this.props.navigation.navigate(page, params);
  };

  render() {
    const { loginState, updateLocalState } = this.props;
    const { ToastProvider } = this.props.screenProps;
    return (
      <Container>
        <ImageBackground
          style={{ flex: 1 }}
          resizeMode="cover"
          source={require('../assets/login_bg.png')}
        >
          <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <View
              onStartShouldSetResponder={() => {
                alert('Hello' + this.state.user.email);
              }}
              style={{
                flex: 7,
                justifyContent: 'flex-end',
                alignItems: 'center'
                // backgroundColor: 'red'
              }}
            >
              <Image
                style={{
                  flex: 1
                }}
                resizeMode="center"
                source={require('../assets/login_logo.png')}
              />
            </View>
            <View
              style={{
                flex: 5
              }}
            >
              <LoginForm
                validateData={this.validateDataLogin}
                googleLogin={this.googleLogin}
                navigateTo={this.navigateTo}
                loginState={loginState}
                ToastProvider={ToastProvider}
                updateLocalState={updateLocalState}
              />
            </View>
            {/* <View style={{ flex: 1 }} /> */}
          </ScrollView>
        </ImageBackground>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = dispatch => {
  return {
    loginReqSubmit: (name, value) => dispatch(loginReqSubmit(name, value)),
    updateLocalState: (name, value) => dispatch(updateLoginState(name, value))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginScreen);
