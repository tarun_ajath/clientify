import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  Dimensions,
  Linking,
  TouchableOpacity
} from 'react-native';
import InputBox from '../component/common/InputBox';
import { ScrollView } from 'react-native-gesture-handler';
import Container from '../component/common/Container';
import Header from '../component/common/Header';
import Spacer from '../component/common/Spacer';
import Icon from 'react-native-vector-icons/FontAwesome';
import ButtonBox from '../component/common/ButtonBox';
import Icons from '../component/common/Icons';
import styles from '../component/common/styles';
import { getUserInfo, updateSettingState } from '../Actions/SettingFormAction';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';

const { height } = Dimensions.get('window');

class SettingForm extends Component {
  componentDidMount() {
    this.props.getUserInfo();
  }

  languageChange = () => {
    alert('language will changed');
  };

  logout = async () => {
    // await DataStore.delete();
    // DataStore.reset();
    // this.props.navigation.navigate('Login');
  };

  goToHome = () => {
    this.props.navigation.navigate('ContactStack');
  };

  render() {
    console.log('setting props', this.props);
    const userData = this.props.SettingState.UserDetailsArray;
    console.log('sdssds', userData);
    return (
      <Container>
        <View style={{ flex: 1 }}>
          <Header
            titleName="Setting"
            leftTitleImg={Icons.profile_back}
            RightTitleImg={Icons.setting_active}
            // onPress={this.goToHome}
            onPressBack={this.goToHome}
          />
          <Spacer size={8} />
          <View
            onStartShouldSetResponder={() => {}}
            style={{ flexDirection: 'row', paddingLeft: 20 }}
          >
            {userData.picture == '' || userData.picture == null ? (
              Icons.my_account()
            ) : (
              <Image
                resizeMode={'contain'}
                style={{ width: 35, height: 35 }}
                source={{
                  uri: userData.picture
                }}
              />
            )}

            {/* <View>{Icons.my_account()}</View> */}
            <View style={{ paddingLeft: 20 }}>
              <Text style={[styles.fontPrimary_Bold, styles.textSize16, {}]}>
                {userData.first_name + ' ' + userData.last_name}
              </Text>
              <Text
                style={[styles.fontPrimary_semiBold, styles.textSize13, {}]}
              >
                {userData.phone == '' || userData.phone == null
                  ? 'xx-xxx-xxx'
                  : userData.phone}
              </Text>
            </View>
          </View>
          <Spacer />
          <View style={{ height: 1, width: '100%', backgroundColor: 'grey' }} />
          <Spacer />
          <View
            onStartShouldSetResponder={() => {
              this.props.navigation.navigate('NotificationScreen');
            }}
            style={{
              flexDirection: 'row',
              paddingLeft: 40
            }}
          >
            <View>{Icons.notification()}</View>
            <View
              style={{
                paddingLeft: 30,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <Text
                style={[
                  styles.fontPrimary_Regular,
                  styles.textSize15,
                  { color: 'grey' }
                ]}
              >
                Notification
              </Text>
            </View>
          </View>
          <Spacer />
          <View style={{ height: 1, width: '100%', backgroundColor: 'grey' }} />

          <Spacer />
          <View style={{ flexDirection: 'row', paddingLeft: 40 }}>
            <TouchableOpacity
              style={{ flexDirection: 'row' }}
              onPress={() => {
                Linking.openURL(
                  'mailto:team@clientify.com?subject=abcdefg&body=body'
                );
              }}
            >
              <View>{Icons.contact_us()}</View>
              <View
                style={{
                  paddingLeft: 30,
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Text
                  style={[
                    styles.fontPrimary_Regular,
                    styles.textSize15,
                    { fontSize: 15, color: 'grey' }
                  ]}
                >
                  Contact us
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <Spacer />
          <View style={{ height: 1, width: '100%', backgroundColor: 'grey' }} />

          <Spacer />
          <View style={{ flexDirection: 'row', paddingLeft: 40 }}>
            <TouchableOpacity
              style={{ flexDirection: 'row' }}
              onPress={() => {
                this.props.navigation.navigate('TermsOfServicesScreen');
              }}
            >
              <View>{Icons.term_of_services()}</View>
              <View
                style={{
                  paddingLeft: 30,
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Text
                  style={[
                    styles.fontPrimary_Regular,
                    styles.textSize15,
                    { color: 'grey' }
                  ]}
                >
                  Terms of Services
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <Spacer />
          <View style={{ height: 1, width: '100%', backgroundColor: 'grey' }} />

          <Spacer />
          <View style={{ flexDirection: 'row', paddingLeft: 40 }}>
            <TouchableOpacity
              style={{ flexDirection: 'row' }}
              onPress={() => {
                this.props.navigation.navigate('AboutUsScreen');
              }}
            >
              <View>{Icons.about_us()}</View>
              <View
                style={{
                  paddingLeft: 30,
                  justifyContent: 'center',
                  alignItems: 'center'
                }}
              >
                <Text
                  style={[
                    styles.fontPrimary_Regular,
                    styles.textSize15,
                    { color: 'grey' }
                  ]}
                >
                  About us
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <Spacer />
          <View style={{ height: 1, width: '100%', backgroundColor: 'grey' }} />
          <Spacer size={10} />

          <View
            style={{
              marginRight: 15,
              marginLeft: 15,
              flexDirection: 'row'
            }}
          >
            <View
              style={{
                flex: 1,
                marginRight: 5,
                borderRadius: 30,
                borderWidth: 1,
                borderColor: 'grey'
              }}
            >
              <ButtonBox
                btnText="Spanish"
                btnColor="white"
                btnTextColor="#000"
                btnImage={Icons.spanish}
                onPress={this.languageChange}
              />
            </View>
            <View style={{ flex: 1, marginLeft: 5 }}>
              <ButtonBox
                btnText="English"
                btnColor="#00b2e2"
                btnTextColor="#fff"
                btnImage={Icons.english}
                onPress={this.languageChange}
              />
            </View>
          </View>
          <Spacer size={20} />
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <View
              style={{
                width: '50%',
                backgroundColor: 'red',
                borderRadius: 30,
                borderWidth: 1,
                borderColor: 'grey'
              }}
            >
              <ButtonBox
                btnText="Logout"
                btnColor="#f5921e"
                btnTextColor="#000"
                btnImage={Icons.logout}
                onPress={this.logout}
              />
            </View>
          </View>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return state;
};
const mapDispatchToProps = {
  getUserInfo,
  updateSettingState
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingForm);
