import { BackHandler } from 'react-native';

class BackButtonHandler {
  counter = 0;
  handleBackButtonStart;
  handleBackButtonStop;

  mount(back, navigation, showToast, page) {
    this.handleBackButtonStart = navigation.addListener('didFocus', () =>
      BackHandler.addEventListener('hardwareBackPress', () =>
        this.onBackButtonPressAndroid(back, navigation, showToast, page)
      )
    );

    this.handleBackButtonStop = navigation.addListener('willBlur', () =>
      BackHandler.removeEventListener('hardwareBackPress', () =>
        this.onBackButtonPressAndroid(back, navigation, showToast, page)
      )
    );
  }

  onBackButtonPressAndroid = (back, navigation, showToast, page) => {
    if (page) {
      navigation.navigate(page);
      return;
    }
    if (back) {
      navigation.goBack();
      return true;
    }
    if (this.counter > 0) {
      BackHandler.exitApp();
    } else {
      setTimeout(() => {
        this.counter = 0;
      }, 3000);
      this.counter++;
      showToast('Press again to exit.', 'normal');
      return true;
    }
  };

  unmount() {
    this.handleBackButtonStart && this.handleBackButtonStart.remove();
    this.handleBackButtonStop && this.handleBackButtonStop.remove();
  }
}

export default new BackButtonHandler();
