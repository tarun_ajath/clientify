import { AsyncStorage } from 'react-native';

class DataStore {
  token;

  async store(key, value) {
    return await AsyncStorage.setItem(key, value);
  }

  async get(key) {
    return await AsyncStorage.getItem(key);
  }

  async delete() {
    return await AsyncStorage.clear();
  }

  reset() {
    this.token = '';
  }
}

export default new DataStore();
