import axios from 'axios';
import apiConfig from '../config/api';
import DataStore from './DataStore';

class HttpClient {
  async userContactList(pageNo) {
    res = await axios.get(
      apiConfig.url +
        'contacts/?api_key=' +
        DataStore.token +
        '&format=json&page=' +
        pageNo
    );
    return res;
  }

  async userCompanyList(pageNo) {
    res = await axios.get(
      apiConfig.url +
        'companies/?api_key=' +
        DataStore.token +
        '&format=json&page=' +
        pageNo
    );
    return res;
  }

  async deals() {
    res = await axios.get(
      apiConfig.url + 'deals/?api_key=' + DataStore.token + '&format=json'
    );
    return res;
  }

  async action() {
    const res = await axios.get(
      apiConfig.url + 'tasks/?api_key=' + DataStore.token + '&format=json'
    );
    return res;
  }

  async contactProfileData(contactId) {
    const res = await axios.get(
      apiConfig.url +
        'contacts/' +
        contactId +
        '/?api_key=' +
        DataStore.token +
        '&format=json'
    );
    return res;
  }

  async deleteContactProfile(contactId) {
    const res = await axios.delete(
      apiConfig.url +
        'contacts/' +
        contactId +
        '/?api_key=' +
        DataStore.token +
        '&format=json'
    );
    return res;
  }

  async companyDetails(companyId) {
    const res = await axios.get(
      apiConfig.url +
        'companies/' +
        companyId +
        '/?api_key=' +
        DataStore.token +
        '&format=json'
    );
    return res;
  }

  async dealDetailsData(dealId) {
    res = await axios.get(
      apiConfig.url +
        'deals/' +
        dealId +
        '/?api_key=' +
        DataStore.token +
        '&format=json'
    );
    return res;
  }

  async addDeal(addDealReqBody) {
    return await axios.post(
      apiConfig.url + 'deals/?api_key=' + DataStore.token + '&format=json',
      addDealReqBody
    );
  }

  async addDealUpdate(addDealEditReqBody, dealId) {
    return await axios.put(
      apiConfig.url +
        'deals/' +
        dealId +
        '/?api_key=' +
        DataStore.token +
        '&format=json',
      addDealEditReqBody
    );
  }

  async addContactUpdate(updateContactReqBody, contactId) {
    return await axios.put(
      apiConfig.url +
        'contacts/' +
        contactId +
        '/?api_key=' +
        DataStore.token +
        '&format=json',
      updateContactReqBody
    );
  }

  async addCompanyUpdate(updateCompanyReqBody, companyId) {
    return await axios.put(
      apiConfig.url +
        'companies/' +
        companyId +
        '/?api_key=' +
        DataStore.token +
        '&format=json',
      updateCompanyReqBody
    );
  }

  async addContact(addContactReqBody) {
    return await axios.post(
      apiConfig.url + 'contacts/?api_key=' + DataStore.token + '&format=json',
      addContactReqBody
    );
  }

  async addCompany(addCompanyReqBody) {
    return await axios.post(
      apiConfig.url + 'companies/',
      // ?api_key=' +
      // apiConfig.api_key +
      // '&format=json'
      addCompanyReqBody,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Token ' + DataStore.token
        }
      }
    );
  }

  async deleteCompany(companyId) {
    return await axios.delete(apiConfig.url + 'companies/' + companyId + '/', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Token ' + DataStore.token
      }
    });
  }

  async addTask(addTaskReqBody) {
    return await axios.post(apiConfig.url + 'tasks/', addTaskReqBody, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Token ' + DataStore.token
      }
    });
  }

  async addNote(relatedId, reqBody, isComeFromContact) {
    if (isComeFromContact) {
      return await axios.post(
        apiConfig.url + 'contacts/' + relatedId + '/note/',
        reqBody,
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Token ' + DataStore.token
          }
        }
      );
    } else {
      return await axios.post(
        apiConfig.url + 'deals/' + relatedId + '/note/',
        reqBody,
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: 'Token ' + DataStore.token
          }
        }
      );
    }
  }

  async addEvents(addEventReqBody) {
    return await axios.post(
      apiConfig.url + 'events/?api_key=' + DataStore.token + '&format=json',
      addEventReqBody,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Token ' + DataStore.token
        }
      }
    );
  }

  async loadCompanySectors() {
    return await axios.get(
      apiConfig.url + 'companies/sectors',
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Token ' + DataStore.token
        }
      }
      // /?api_key=' +
      // apiConfig.api_key +
      // '&format=json'
    );
  }

  async userPipelineList() {
    res = await axios.get(apiConfig.url + 'deals/pipelines', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Token ' + DataStore.token
      }
    });
    return res;
  }

  async userPipelineById(pipelineId) {
    res = await axios.get(apiConfig.url + 'deals/pipelines/' + pipelineId, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Token ' + DataStore.token
      }
    });
    return res;
  }

  async userPipelineStageList() {
    res = await axios.get(apiConfig.url + 'deals/pipelines/stages', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Token ' + DataStore.token
      }
    });
    return res;
  }

  async userPipelineStageById(pipelineStageId) {
    res = await axios.get(
      apiConfig.url + 'deals/pipelines/stages/' + pipelineStageId,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: 'Token ' + DataStore.token
        }
      }
    );
    return res;
  }

  async loginSubmit(loginReqBody) {
    res = await axios.post(
      apiConfig.url + 'api-auth/obtain_token/',
      loginReqBody,
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    );
    return res;
  }

  async contactSearch(searchQuery) {
    return await axios.get(apiConfig.url + 'contacts/?query=' + searchQuery, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Token ' + DataStore.token
      }
    });
  }

  async companySearch(searchQuery) {
    return await axios.get(apiConfig.url + 'companies/?query=' + searchQuery, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Token ' + DataStore.token
      }
    });
  }

  async UserInfo() {
    return await axios.get(apiConfig.url + 'settings/my-account/', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Token ' + DataStore.token
      }
    });
  }

  // async Demo(reqBody) {
  //   console.log('hii http');
  //   return await axios.post(
  //     'http://spicaworks.com.md-94.webhostbox.net/e4u/index.php/api/register',
  //     reqBody
  //   );
  // }

  setToken(apiToken) {
    this.token = apiToken;
  }

  removeToken(apiToken) {
    this.token = '';
  }

  login = () => {
    console.log(this.token);
  };
}

export default new HttpClient();
