import { DefaultTheme } from 'react-native-paper';

class ThemeHandler {
  constructor() {
    this.theme = {
      ...DefaultTheme,
      roundness: 2,
      colors: {
        ...DefaultTheme.colors,
        primary: '#3498db',
        accent: '#f1c40f',
        dark: '#222',
        success: 'green',
        warning: 'yellow'
      }
    };
  }

  getColor = color => {
    switch (color) {
      case 'normal':
        return this.theme.colors.dark;
      case 'success':
        return this.theme.colors.success;
      case 'error':
        return this.theme.colors.error;
      case 'warning':
        return this.theme.colors.warning;
      default:
        return this.theme.colors.dark;
    }
  };
}

export default new ThemeHandler();
