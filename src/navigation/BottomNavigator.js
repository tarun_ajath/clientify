import React, { Component } from 'react';
import { createBottomTabNavigator } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import ContactStack from '../navigation/ContactStack';
import DealsStack from '../navigation/DealsStack';
import ActivitiesStack from '../navigation/ActivitiesStack';
import SettingStack from '../navigation/SettingStack';
import Icon from 'react-native-vector-icons/Ionicons';
import Icons from '../component/common/Icons';

const BottomNavigator = createMaterialBottomTabNavigator(
  {
    ContactStack: {
      screen: ContactStack,
      navigationOptions: {
        tabBarLabel: 'Contacts',
        tabBarColor: '#fff',
        // barStyle: { backgroundColor: '#00bfff' },
        tabBarIcon: ({ tintColor, focused }) =>
          focused ? Icons.contacts_active() : Icons.contacts_inactive()
      }
    },
    DealsStack: {
      screen: DealsStack,
      navigationOptions: {
        tabBarLabel: 'Deals',
        tabBarColor: 'red',
        tabBarIcon: ({ tintColor, focused }) =>
          // <Icon name={focused ? 'ios-call' : 'ios-home'} size={24} />
          focused ? Icons.deal_active() : Icons.deal_inactive()
      }
    },
    ActivitiesStack: {
      screen: ActivitiesStack,
      navigationOptions: {
        tabBarLabel: 'Activities',
        tabBarColor: '#fff',
        tabBarIcon: ({ tintColor, focused }) =>
          focused ? Icons.activity_active() : Icons.activity_inactive()
      }
    },
    SettingStack: {
      screen: SettingStack,
      navigationOptions: {
        tabBarLabel: 'Settings',
        tabBarColor: '#fff',

        tabBarIcon: ({ tintColor, focused }) =>
          focused ? Icons.setting_active() : Icons.setting_inactive()
      }
    }
  },
  {
    initialRouteName: 'ContactStack',
    // order: ["Contacts", "Deals", "Activities", "Settings"],
    // barStyle: { backgroundColor: 'red' },
    activeColor: '#fff',
    inactiveColor: 'grey',
    // activeBarStyle: { backgroundColor: 'red' },
    barStyle: { backgroundColor: '#00b2e2' },
    style: { backgroundColor: '#00b2e2' },
    shifting: false,
    tabBarColor: '#fff'
  }
);

export default BottomNavigator;
