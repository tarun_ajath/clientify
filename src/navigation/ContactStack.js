import { createStackNavigator } from 'react-navigation';
import ContactForm from '../screens/ContactForm';
import AddNewContacts from '../screens/AddNewContacts';
import AddNewCompany from '../screens/AddNewCompany';
import ProfileScreen from '../screens/ProfileScreen';
import CompanyContactDetails from '../screens/CompanyContactDetails';
import AddNoteScreen from '../screens/AddNoteScreen';
import AddTask from '../screens/AddTask';
import UserContactList from '../component/UserContactList';
import ContactDealForm from '../component/ContactDealForm';
import DealsDetailsScreen from '../screens/DealsDetailsScreen';
import AddEvents from '../screens/AddEvents';

const ContactStack = createStackNavigator(
  {
    ContactForm: {
      screen: ContactForm
    },
    UserContactList: {
      screen: UserContactList
    },
    AddNewContacts: {
      screen: AddNewContacts
      // navigationOptions: {
      //   tabBarVisible: false
      // }
    },
    AddNewCompany: {
      screen: AddNewCompany
    },
    Profile: {
      screen: ProfileScreen
    },
    CompanyContactDetails: {
      screen: CompanyContactDetails
    },
    AddNoteScreen: {
      screen: AddNoteScreen
    },
    AddTask: {
      screen: AddTask
    },
    ContactDealForm: {
      screen: ContactDealForm
    },
    DealsDetailsScreen: {
      screen: DealsDetailsScreen
    },
    AddEvents: {
      screen: AddEvents
    }
  },
  {
    initialRouteName: 'ContactForm',
    headerMode: 'none'
  }
);

ContactStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible
  };
};

export default ContactStack;
