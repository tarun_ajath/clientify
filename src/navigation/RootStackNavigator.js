import { createStackNavigator, createAppContainer } from 'react-navigation';
import LoginScreen from '../screens/LoginScreen';
import BottomNavigator from '../navigation/BottomNavigator';
import FirstScreen from '../screens/FirstScreen';

const StackNavigator = createStackNavigator(
  {
    Login: {
      screen: LoginScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    Home: {
      screen: BottomNavigator,
      navigationOptions: {
        gesturesEnabled: false
      }
    },
    FirstScreen: {
      screen: FirstScreen,
      navigationOptions: {
        gesturesEnabled: false
      }
    }
  },
  {
    initialRouteName: 'FirstScreen',
    headerMode: 'none'
  }
);

const RootStackNavigator = createAppContainer(StackNavigator);

export default RootStackNavigator;
