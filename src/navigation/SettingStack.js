import { createStackNavigator } from 'react-navigation';
import SettingForm from '../screens/SettingForm';
import TermsOfServicesScreen from '../screens/TermsOfServicesScreen';
import AboutUsScreen from '../screens/AboutUsScreen';
import NotificationScreen from '../screens/NotificationScreen';

const SettingStack = createStackNavigator(
  {
    SettingForm: {
      screen: SettingForm
    },
    TermsOfServicesScreen: {
      screen: TermsOfServicesScreen
    },
    AboutUsScreen: {
      screen: AboutUsScreen
    },
    NotificationScreen: {
      screen: NotificationScreen
    }
  },
  {
    initialRouteName: 'SettingForm',
    headerMode: 'none'
  }
);

SettingStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible
  };
};

export default SettingStack;
