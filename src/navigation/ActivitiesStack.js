import { createStackNavigator } from 'react-navigation';
import ActivitiesForm from '../screens/ActivitiesForm';
import AddEventActivity from '../component/AddEventActivity';
import AddTask from '../screens/AddTask';

const ActivitiesStack = createStackNavigator(
  {
    ActivitiesForm: {
      screen: ActivitiesForm
    },
    AddEventActivity: {
      screen: AddEventActivity
    },
    AddTask: {
      screen: AddTask
    }
  },
  {
    initialRouteName: 'ActivitiesForm',
    headerMode: 'none'
  }
);

ActivitiesStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible
  };
};

export default ActivitiesStack;
