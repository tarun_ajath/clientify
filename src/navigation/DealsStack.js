import { createStackNavigator } from 'react-navigation';
import DealsForm from '../screens/DealsForm';
import DealsDetailsScreen from '../screens/DealsDetailsScreen';
import AddDealScreen from '../screens/AddDealScreen';
import AddCallNote from '../screens/AddCallNote';
import AddEvents from '../screens/AddEvents';
import AddNoteScreen from '../screens/AddNoteScreen';

const DealsStack = createStackNavigator(
  {
    DealsForm: {
      screen: DealsForm
    },
    DealsDetailsScreen: {
      screen: DealsDetailsScreen
    },
    AddDealScreen: {
      screen: AddDealScreen
    },
    AddCallNote: {
      screen: AddCallNote
    },
    AddEvents: {
      screen: AddEvents
    },
    AddNoteScreen: {
      screen: AddNoteScreen
    }
  },
  {
    initialRouteName: 'DealsForm',
    headerMode: 'none'
  }
);

DealsStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible
  };
};

export default DealsStack;
