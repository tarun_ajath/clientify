import React, { Component } from 'react';
import {
  View,
  Text,
  Dimensions,
  FlatList,
  PermissionsAndroid,
  ActivityIndicator,
  CheckBox
} from 'react-native';
import BackButtonHandler from '../utilities/BackButtonHandler';
import styless from '../component/common/styles';
import Container from './common/Container';
import InputBox from './common/InputBox';
import Header from './common/Header';
import Icons from './common/Icons';
import ButtonBox from './common/ButtonBox';
import { connect } from 'react-redux';
import Contacts from 'react-native-contacts';
import {
  getContactList,
  updateContactState
} from '../Actions/contactFormAction';

class UserContactList extends Component {
  constructor() {
    super();
    this.state = {
      checked: false
    };
  }
  componentDidMount() {
    const { ToastProvider } = this.props.screenProps;
    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
      title: 'Contacts',
      message: 'This app would like to view your contacts.'
    }).then(() => {
      Contacts.getAll((err, contacts) => {
        if (err === 'denied') {
          alert('in  err  ' + JSON.stringify(err));
        } else {
          this.props.updateContactState('ImportContactsList', contacts);
        }
      });
    });
    BackButtonHandler.mount(
      true,
      this.props.navigation,
      ToastProvider.showToast
    );
  }
  componentWillUnmount() {
    BackButtonHandler.unmount();
  }
  isNumberNull(numberArray) {
    if (numberArray == null) {
      return;
    } else {
      return (
        <Text style={{ fontSize: 14, color: '#000' }}>
          {numberArray.number}
        </Text>
      );
    }
  }

  contactList(item, i) {
    return (
      <View style={{ padding: 10 }}>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 6 }}>
            <Text style={{ fontSize: 18, color: '#000', paddingBottom: 5 }}>
              {item.givenName}
            </Text>

            <Text style={{ fontSize: 14, color: '#000' }}>
              {this.isNumberNull(item.phoneNumbers[0])}
            </Text>
          </View>

          <View style={{ flex: 2 }}>
            <CheckBox
              value={this.state.checked}
              onPress={checked => {
                alert('hello' + checked);
                this.setState({ checked: !checked });
              }}
            />
          </View>
        </View>
        <View style={{ height: 1, width: '100%', backgroundColor: 'grey' }} />
      </View>
    );
  }

  ImportContactList = () => {
    const { contactFormState } = this.props;
    if (contactFormState.isLoading) {
      return (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
            alignSelf: 'center'
          }}
        >
          <ActivityIndicator size="large" color="#00b2e2" animating={true} />
        </View>
      );
    } else {
      if (
        contactFormState.ImportContactSearchText == '' ||
        !contactFormState.ImportContactSearchText.trim().length
      ) {
        return (
          <FlatList
            data={contactFormState.ImportContactsList}
            renderItem={({ item, index }) => this.contactList(item, index)}
            keyExtractor={(item, index) => index.toString()}
          />
        );
      } else {
        let updatedContactList = contactFormState.ImportContactsList.filter(
          item => {
            return item['givenName']
              .toLowerCase()
              .includes(
                contactFormState.ImportContactSearchText.trim().toLowerCase()
              );
          }
        );
        return (
          <FlatList
            data={updatedContactList}
            renderItem={({ item, index }) => this.contactList(item, index)}
            keyExtractor={(item, index) => index.toString()}
          />
        );
      }
    }
  };

  importContactFun = () => {
    this.props.navigation.navigate('ContactForm');
  };

  goToHome = () => {
    this.props.navigation.navigate('ContactForm');
  };

  render() {
    console.log('importcontact props', this.props);
    const { contactFormState } = this.props;
    return (
      <Container>
        <Header
          titleName="Phone Contacts"
          leftTitleImg={Icons.profile_back}
          RightTitleImg={Icons.add_deals()}
          onPressBack={this.goToHome}
        />
        <View style={{ flex: 1 }}>
          <View
            style={{
              height: 55,
              marginLeft: 25,
              marginRight: 25,
              paddingTop: 10,
              paddingBottom: 10
            }}
          >
            <InputBox
              placeholder="Search by contact No"
              rightImageName={Icons.search}
              value={contactFormState.ImportContactSearchText}
              name="ImportContactSearchText"
              updateLocalState={this.props.updateContactState}
              onTextChangeMethod={this.ImportContactList}
            />
          </View>
          <View
            style={{ flex: 10, marginTop: 5, marginLeft: 10, marginRight: 10 }}
          >
            {this.ImportContactList()}
          </View>
          <View
            style={{
              height: 70,
              marginLeft: 10,
              marginRight: 10,
              paddingTop: 10,
              paddingBottom: 5
            }}
          >
            <ButtonBox
              btnText="Import Contacts"
              btnColor="#f5921e"
              onPress={this.importContactFun}
              btnTextColor="#fff"
            />
          </View>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = {
  getContactList,
  updateContactState
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserContactList);
