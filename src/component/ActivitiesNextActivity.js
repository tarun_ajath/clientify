import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  CheckBox,
  TouchableOpacity
} from 'react-native';
import ListContents from './common/ActivitiesListItem';
import Icons from './common/Icons';
import styless from './common/styles';

class ActivitiesNextActivity extends Component {
  constructor() {
    super();
    this.state = {
      checked: true
    };
  }

  ActivityExpiredTask = (item, index) => {
    return (
      <View style={{ marginTop: 15, marginBottom: 10 }}>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 4, paddingLeft: 20 }}>
            <Text style={[styless.fontPrimary_Regular, styless.textSize13, {}]}>
              Task
            </Text>
          </View>
          <View style={{ flex: 1, alignItems: 'center' }}>
            <Text>:</Text>
          </View>
          <View style={{ flex: 4, paddingRight: 20 }}>
            <Text
              style={[
                styless.fontPrimary_semiBold,
                styless.textSize14,
                {
                  color: '#000'
                }
              ]}
              numberOfLines={1}
            >
              {item.name}
            </Text>
          </View>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 4, paddingLeft: 20 }}>
            <Text style={[styless.fontPrimary_Regular, styless.textSize13, {}]}>
              Type
            </Text>
          </View>
          <View style={{ flex: 1, alignItems: 'center' }}>
            <Text>:</Text>
          </View>
          <View style={{ flex: 4, paddingRight: 20 }}>
            <Text
              style={[
                styless.fontPrimary_semiBold,
                styless.textSize14,
                {
                  color: '#000'
                }
              ]}
              numberOfLines={1}
            >
              {item.type_desc}
            </Text>
          </View>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 4, paddingLeft: 20 }}>
            <Text style={[styless.fontPrimary_Regular, styless.textSize13, {}]}>
              Due Date
            </Text>
          </View>
          <View style={{ flex: 1, alignItems: 'center' }}>
            <Text>:</Text>
          </View>
          <View style={{ flex: 4, paddingRight: 20 }}>
            <Text
              style={[
                styless.fontPrimary_semiBold,
                styless.textSize14,
                {
                  color: '#000'
                }
              ]}
              numberOfLines={1}
            >
              {new Date(item.due_date).getFullYear() +
                '-' +
                (new Date(item.due_date).getMonth() + 1) +
                '-' +
                new Date(item.due_date).getDate()}
            </Text>
          </View>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 4, paddingLeft: 20 }}>
            <Text style={[styless.fontPrimary_Regular, styless.textSize13, {}]}>
              Assigned to
            </Text>
          </View>
          <View style={{ flex: 1, alignItems: 'center' }}>
            <Text>:</Text>
          </View>
          <View style={{ flex: 4, paddingRight: 20 }}>
            <Text
              style={[
                styless.fontPrimary_semiBold,
                styless.textSize14,
                {
                  color: '#000'
                }
              ]}
              numberOfLines={1}
            >
              {item.assigned_to}
            </Text>
          </View>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 4, paddingLeft: 20 }}>
            <Text style={[styless.fontPrimary_Regular, styless.textSize13, {}]}>
              Status
            </Text>
          </View>
          <View style={{ flex: 1, alignItems: 'center' }}>
            <Text>:</Text>
          </View>
          <View style={{ flex: 4, paddingRight: 20 }}>
            <Text
              style={[
                styless.fontPrimary_semiBold,
                styless.textSize14,
                {
                  color: '#000'
                }
              ]}
              numberOfLines={1}
            >
              {item.status_desc}
            </Text>
          </View>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            paddingTop: 10,
            paddingLeft: 15,
            paddingRight: 20,
            paddingBottom: 5
          }}
        >
          <View
            style={{
              justifyContent: 'flex-start',
              alignItems: 'center'
            }}
          >
            <CheckBox
              value={this.state.checked}
              onValueChange={() =>
                this.setState({ checked: !this.state.checked })
              }
            />
          </View>
          <View
            style={{
              flex: 5,
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center'
            }}
          >
            <Text
              style={[
                styless.fontPrimary_Regular,
                styless.textSize13,
                {
                  color: 'grey'
                }
              ]}
              numberOfLines={1}
            >
              Mark as completed
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'flex-end',
              alignItems: 'center'
            }}
          >
            <TouchableOpacity>
              <View style={styles.circle}>
                <View>{Icons.message_orange_background()}</View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ height: 1, width: '100%', backgroundColor: '#000' }} />
      </View>
    );
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.props.activityExpiredTaskList == [] ||
        this.props.activityExpiredTaskList == '' ? (
          <View
            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
          >
            <Text style={{ color: 'red', fontSize: 16 }}>No Data</Text>
          </View>
        ) : (
          <FlatList
            data={this.props.activityExpiredTaskList}
            renderItem={({ item, index }) =>
              this.ActivityExpiredTask(item, index)
            }
            keyExtractor={(item, index) => index.toString()}
          />
        )}
      </View>
    );
  }
}

export default ActivitiesNextActivity;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {},
  circle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5921d',
    height: 40,
    width: 40,
    borderRadius: 50,
    resizeMode: 'contain'
  },
  circleEvent: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00bfff',
    height: 40,
    width: 40,
    borderRadius: 50,
    resizeMode: 'contain'
  }
});
