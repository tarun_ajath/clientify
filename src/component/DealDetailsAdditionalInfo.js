import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, FlatList } from 'react-native';
import Spacer from './common/Spacer';
import Icons from './common/Icons';
import styless from './common/styles';

class DealDetailsAdditionalInfo extends Component {
  firstArrayElement(ArrayElement, arrayType) {
    if (ArrayElement == null || ArrayElement == '') {
      return (
        <Text style={[styless.fontPrimary_Regular, styless.textSize15, {}]}>
          no data
        </Text>
      );
    } else {
      if (arrayType == 'created') {
        return (
          <Text style={[styless.fontPrimary_Regular, styless.textSize15, {}]}>
            {new Date(ArrayElement).getFullYear() +
              '-' +
              (new Date(ArrayElement).getMonth() + 1) +
              '-' +
              new Date(ArrayElement).getDate()}
          </Text>
        );
      } else if (arrayType == 'expected_closed_date') {
        return (
          <Text style={[styless.fontPrimary_Regular, styless.textSize15, {}]}>
            {ArrayElement}
          </Text>
        );
      } else if (arrayType == 'probability') {
        return (
          <Text style={[styless.fontPrimary_Regular, styless.textSize15, {}]}>
            {ArrayElement}
          </Text>
        );
      } else if (arrayType == 'status') {
        return (
          <Text style={[styless.fontPrimary_Regular, styless.textSize15, {}]}>
            {ArrayElement}
          </Text>
        );
      } else if (arrayType == 'contact') {
        return (
          <Text style={[styless.fontPrimary_Regular, styless.textSize15, {}]}>
            {ArrayElement}
          </Text>
        );
      } else if (arrayType == 'company') {
        return (
          <Text style={[styless.fontPrimary_Regular, styless.textSize15, {}]}>
            {ArrayElement}
          </Text>
        );
      }
    }
  }
  render() {
    let listData = this.props.newListArray;
    return (
      <View style={{ padding: 20 }}>
        <View style={{ flex: 1, paddingTop: 8 }}>
          <View style={{ flex: 4 }}>
            <Text style={[styless.fontPrimary_Bold, styless.textSize13, {}]}>
              Created on
            </Text>
          </View>
          <View style={{ flex: 4, paddingLeft: 5, paddingTop: 3 }}>
            <Text style={[styless.fontPrimary_Regular, styless.textSize15, {}]}>
              {this.firstArrayElement(listData.created, 'created')}
            </Text>
          </View>
        </View>

        <View style={{ flex: 1, paddingTop: 10 }}>
          <View style={{ flex: 4 }}>
            <Text style={[styless.fontPrimary_Bold, styless.textSize13, {}]}>
              Expected Close Date
            </Text>
          </View>
          <View style={{ flex: 4, paddingLeft: 5, paddingTop: 3 }}>
            <Text style={[styless.fontPrimary_Regular, styless.textSize15, {}]}>
              {this.firstArrayElement(
                listData.expected_closed_date,
                'expected_closed_date'
              )}
            </Text>
          </View>
        </View>

        <View style={{ flex: 1, paddingTop: 10 }}>
          <View style={{ flex: 4 }}>
            <Text style={[styless.fontPrimary_Bold, styless.textSize13, {}]}>
              Probability
            </Text>
          </View>
          <View style={{ flex: 4, paddingLeft: 5, paddingTop: 3 }}>
            {this.firstArrayElement(listData.probability, 'probability')}
          </View>
        </View>

        <View style={{ flex: 1, paddingTop: 10 }}>
          <View style={{ flex: 4 }}>
            <Text style={[styless.fontPrimary_Bold, styless.textSize13, {}]}>
              Status
            </Text>
          </View>
          <View style={{ flex: 4, paddingLeft: 5, paddingTop: 3 }}>
            <Text style={[styless.fontPrimary_Regular, styless.textSize15, {}]}>
              {this.firstArrayElement(listData.status, 'status')}
            </Text>
          </View>
        </View>

        <View style={{ flex: 1, paddingTop: 10 }}>
          <View style={{ flex: 4 }}>
            <Text style={[styless.fontPrimary_Bold, styless.textSize13, {}]}>
              Contacts
            </Text>
          </View>
          <View style={{ flex: 4, paddingLeft: 5, paddingTop: 3 }}>
            <Text style={[styless.fontPrimary_Regular, styless.textSize15, {}]}>
              {this.firstArrayElement(listData.contact, 'contact')}
            </Text>
          </View>
        </View>

        <View style={{ flex: 1, paddingTop: 10 }}>
          <View style={{ flex: 4 }}>
            <Text style={[styless.fontPrimary_Bold, styless.textSize13, {}]}>
              Companies
            </Text>
          </View>
          <View style={{ flex: 4, paddingLeft: 5, paddingTop: 3 }}>
            <Text style={[styless.fontPrimary_Regular, styless.textSize15, {}]}>
              {this.firstArrayElement(listData.company, 'company')}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}
export default DealDetailsAdditionalInfo;
