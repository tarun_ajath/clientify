import React, { Component } from 'react';
import { Text, View, ScrollView, FlatList } from 'react-native';
import Spacer from './common/Spacer';
import styless from './common/styles';

import Container from './common/Container';

class DealDetailsAssociatedData extends Component {
  EventsList = (item, index) => {
    return (
      <View style={{ margin: 8 }}>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={{ flex: 2.5 }}>
            <Text style={{ fontWeight: 'bold', fontSize: 15 }}>
              {item.name}
            </Text>
          </View>
        </View>

        <View
          style={{
            height: 1,
            width: '100%',
            backgroundColor: 'grey',
            marginTop: 7
          }}
        />
      </View>
    );
  };

  TaskList = (item, index) => {
    return (
      <View style={{ margin: 8 }}>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={{ flex: 2.5 }}>
            <Text style={{ fontWeight: 'bold', fontSize: 15 }}>
              {item.name}
            </Text>
          </View>
          <View style={{ flex: 1 }}>
            <Text style={{ fontSize: 12 }}>
              {new Date(item.due_date).getFullYear() +
                '-' +
                (new Date(item.due_date).getMonth() + 1) +
                '-' +
                new Date(item.due_date).getDate()}
            </Text>
          </View>
        </View>

        <View style={{ flex: 1 }}>
          <Text style={{ fontSize: 12 }}>{item.type_desc}</Text>
        </View>

        <View
          style={{
            height: 1,
            width: '100%',
            backgroundColor: 'grey',
            marginTop: 7
          }}
        />
      </View>
    );
  };
  filterDataForTimelineProfile = ListArray => {
    if (ListArray == null) {
    } else {
      let newListArray = [];
      Object.keys(ListArray).forEach(key => {
        newListArray.push({ key: key, value: ListArray[key] });
      });
      // console.log(
      //   'new fhhfgj of timeline are now ' + JSON.stringify(newListArray)
      // );
      return newListArray;
    }
  };
  customFieldList = listVal => {
    if (listVal != null) {
      const newListArray = this.filterDataForTimelineProfile(listVal);
      console.log('fdfdfdf', newListArray);

      return (
        <View>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ flex: 4 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize12, {}]}
              >
                {JSON.parse(JSON.stringify(newListArray)).key}{' '}
              </Text>
            </View>
            <View
              style={{ flex: 1, alignItems: 'flex-start', paddingBottom: 3 }}
            >
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 3 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize12, {}]}
              >
                {JSON.parse(JSON.stringify(newListArray)).value}
              </Text>
            </View>
          </View>
        </View>
      );
    }
  };

  render() {
    let listData = this.props.newListArray;
    return (
      <Container>
        <View style={{ padding: 15 }}>
          <View
            style={{
              height: 40,
              paddingLeft: 15,
              backgroundColor: '#00b2e2',
              justifyContent: 'center',
              alignItems: 'flex-start'
            }}
          >
            <Text style={[styless.fontPrimary_Bold, styless.textSize17, {}]}>
              Events
            </Text>
          </View>
          <View style={{ flex: 1 }}>
            {listData.events == [] ||
            listData.events == null ||
            listData.events == '' ? (
              <Text
                style={[
                  styless.fontPrimary_Regular,
                  styless.textSize12,
                  { paddingLeft: 15 }
                ]}
              >
                No Events
              </Text>
            ) : (
              <FlatList
                style={{ flex: 1 }}
                data={listData.events}
                renderItem={({ item, index }) => this.EventsList(item, index)}
                keyExtractor={(item, index) => index.toString()}
              />
            )}
          </View>

          <View
            style={{
              height: 40,
              paddingLeft: 15,
              backgroundColor: '#00b2e2',
              justifyContent: 'center',
              alignItems: 'flex-start'
            }}
          >
            <Text
              style={[
                styless.fontPrimary_Bold,
                styless.textSize17,
                { marginTop: 5, marginBottom: 5 }
              ]}
            >
              Tasks
            </Text>
          </View>
          <View style={{ flex: 1 }}>
            {listData.tasks == [] ||
            listData.tasks == null ||
            listData.tasks == '' ? (
              <Text
                style={[
                  styless.fontPrimary_Regular,
                  styless.textSize12,
                  { paddingLeft: 15 }
                ]}
              >
                No related tasks
              </Text>
            ) : (
              <FlatList
                style={{ flex: 1 }}
                data={listData.tasks}
                renderItem={({ item, index }) => this.TaskList(item, index)}
                keyExtractor={(item, index) => index.toString()}
              />
            )}
          </View>

          <View
            style={{
              height: 40,
              paddingLeft: 15,
              backgroundColor: '#00b2e2',
              justifyContent: 'center',
              alignItems: 'flex-start'
            }}
          >
            <Text
              style={[
                styless.fontPrimary_Bold,
                styless.textSize17,
                { marginTop: 5, marginBottom: 5 }
              ]}
            >
              tags
            </Text>
          </View>
          <View style={{ paddingLeft: 15 }}>
            <Text style={[styless.fontPrimary_Regular, styless.textSize12, {}]}>
              {listData.tags == [] ||
              listData.tags == null ||
              listData.tags == ''
                ? 'No Tags'
                : listData.tags.toString()}
            </Text>
          </View>

          <View
            style={{
              height: 40,
              paddingLeft: 15,
              backgroundColor: '#00b2e2',
              justifyContent: 'center',
              alignItems: 'flex-start'
            }}
          >
            <Text
              style={[
                styless.fontPrimary_Bold,
                styless.textSize17,
                { marginTop: 5, marginBottom: 5 }
              ]}
            >
              custom_fields
            </Text>
          </View>
          <View style={{ paddingLeft: 15 }}>
            {listData.custom_fields == [] ||
            listData.custom_fields == null ||
            listData.custom_fields == '' ? (
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize12, {}]}
              >
                No custom fields
              </Text>
            ) : (
              <FlatList
                data={listData.custom_fields}
                renderItem={({ item, index }) =>
                  this.customFieldList(item, index)
                }
                keyExtractor={(item, index) => index.toString()}
              />
            )}
          </View>
        </View>
      </Container>
    );
  }
}

export default DealDetailsAssociatedData;
