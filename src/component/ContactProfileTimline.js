import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, FlatList } from 'react-native';
import Timeline from 'react-native-timeline-listview';
import Spacer from './common/Spacer';
import Icons from './common/Icons';
import styless from './common/styles';

const circleColor = 'call';
let GlobalTimeLineList = [];

class ContactProfileTimline extends Component {
  constructor(props) {
    super(props);
    this.data = this.props.newTimlineListArray.wall_entries;
  }
  // filterDataForTimeline(ListArray) {
  //   let newListArray = [];
  //   Object.keys(ListArray).forEach(key => {
  //     console.log('new keys are =>> ' + key);
  //     newListArray.push({ key: key, value: ListArray[key] });
  //   });
  //   console.log(
  //     'new array of timeline are now ' + JSON.stringify(newListArray)
  //   );
  //   return newListArray;
  // }

  // componentDidMount() {
  //   console.log(
  //     'array of data is ' + JSON.stringify(this.props.newTimlineListArray)
  //   );
  //   console.log(
  //     'length array of data is ' +
  //       Object.keys(this.props.newTimlineListArray).length
  //   );
  //   for (
  //     let i = 0;
  //     i < Object.keys(this.props.newTimlineListArray).length;
  //     i++
  //   ) {
  //     let ListArray = JSON.parse(this.props.newTimlineListArray[i].extra);
  //     let timlinefilterJson = this.filterDataForTimeline(ListArray);
  //     this.props.newTimlineListArray[i].extraArray=
  //     console.log('filtered arrrr ' + JSON.stringify(timlinefilterJson));

  //     // <FlatList
  //     //   data={timlinefilterJson}
  //     //   renderItem={({ item, index }) => this.listValues(item, index)}
  //     //   keyExtractor={(item, index) => index.toString()}
  //     // />;
  //   }
  // }
  dateFormate = () => {
    return dsfdsf;
  };

  renderDetails(rowData, sectionID, rowID) {
    let dateTime = (
      <View style={{ flexGrow: 1, flexDirection: 'row' }}>
        <View
          style={{ flexGrow: 1, color: '#A9A9A9A9', alignItems: 'flex-start' }}
        >
          <Text style={[styless.fontPrimary_Regular, styless.textSize12, {}]}>
            {new Date(rowData.created).getFullYear() +
              '-' +
              (new Date(rowData.created).getMonth() + 1) +
              '-' +
              new Date(rowData.created).getDate()}
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            color: '#A9A9A9A9',
            alignItems: 'flex-end'
          }}
        >
          <Text style={[styless.fontPrimary_Regular, styless.textSize12, {}]}>
            {new Date(rowData.created).getHours() +
              ':' +
              new Date(rowData.created).getMinutes()}
          </Text>
        </View>
      </View>
    );
    let title = (
      <Text
        style={[
          styless.fontPrimary_semiBold,
          styless.textSize14,
          { paddingTop: 5, paddingBottom: 4 }
        ]}
      >
        {rowData.user}
      </Text>
    );
    let subtitle = (
      <Text
        style={[
          styless.fontPrimary_Regular,
          styless.textSize13,
          { paddingBottom: 8 }
        ]}
      >
        {rowData.type}
      </Text>
    );

    let listValues = (item, i) => {
      if (item != null) {
        return (
          <View>
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <View style={{ flex: 4 }}>
                <Text
                  style={[styless.fontPrimary_Regular, styless.textSize12, {}]}
                >
                  {JSON.parse(JSON.stringify(item)).key}{' '}
                </Text>
              </View>
              <View
                style={{ flex: 1, alignItems: 'flex-start', paddingBottom: 3 }}
              >
                <Text>:</Text>
              </View>
              <View style={{ flex: 4, paddingRight: 3 }}>
                <Text
                  style={[styless.fontPrimary_Regular, styless.textSize12, {}]}
                >
                  {JSON.parse(JSON.stringify(item)).value}
                </Text>
              </View>
            </View>
          </View>
        );
      }
    };

    let desc = () => {
      return (
        <FlatList
          data={rowData.extraArray}
          renderItem={({ item, index }) => listValues(item, index)}
          keyExtractor={(item, index) => index.toString()}
        />
      );
    };
    return (
      <View style={{ flex: 1, paddingLeft: 40, paddingRight: 10 }}>
        <View
          style={{
            flex: 1,
            backgroundColor: '#ffffff',
            padding: 10
          }}
        >
          {dateTime}
          {title}
          {subtitle}
          {desc()}
        </View>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <Spacer />
        <Timeline
          style={styles.list}
          data={this.data}
          timeContainerStyle={{
            minWidth: 0,
            paddingRight: 40
          }}
          showTime={false}
          circleSize={40}
          circleColor={circleColor === 'call' ? '#F44336' : '#7E69FA'}
          lineColor="#000000"
          lineWidth={0.5}
          innerCircle={'icon'}
          iconStyle={styles.icon}
          renderFullLine="true"
          renderDetail={this.renderDetails}
        />
      </View>
    );
  }
}
export default ContactProfileTimline;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#eeeeee',
    paddingLeft: 10
    // PaddingTop: 20
  },
  list: {
    flex: 1
  },
  icon: {
    height: 20,
    width: 20,
    justifyContent: 'flex-end'
  },
  image: {
    height: 20,
    width: 20
  },
  title: {
    fontSize: 15,
    color: '#000000',
    fontFamily: 'Raleway-Bold',
    paddingTop: 5,
    lineHeight: 30
  },
  subtitle: {
    fontSize: 13,
    lineHeight: 30,
    color: '#000',
    fontFamily: 'Raleway-SemiBold'
  },
  descriptionContainer: {
    flex: 1
  },
  textDescription: {
    color: 'gray',
    fontFamily: 'Raleway-Regular'
  }
});
