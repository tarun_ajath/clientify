import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import Timeline from 'react-native-timeline-listview';
import Spacer from './Spacer';
import Icons from './Icons';
import styless from './styles';

const circleColor = 'call';
class TimeLineListComponent extends Component {
  constructor(props) {
    super(props);
    this.data = [
      {
        time: '09:00AM',
        date: '09-Jan-2018',
        subtitle: 'Benjamin',
        title: 'Note Added',
        icon: require('../../assets/cash_view.png'),
        imageUrl: require('../../assets/cash_view.png'),
        //"https://cloud.githubusercontent.com/assets/21040043/24240419/1f553dee-0fe4-11e7-8638-6025682232b1.jpg",
        amount: '$4564.00',
        expiration_date: '15-Jan-2018',
        status: 'Open/Expired',
        probability: '20%',
        pipeline: 'Default',
        source: 'None',
        owner_name: null
      },
      {
        time: '09:00AM',
        date: '09-Jan-2018',
        subtitle: 'Benjamin',
        title: 'Note Added',
        icon: require('../../assets/cash_view.png'),
        imageUrl: require('../../assets/cash_view.png'),
        //"https://cloud.githubusercontent.com/assets/21040043/24240419/1f553dee-0fe4-11e7-8638-6025682232b1.jpg",
        amount: '$4564.00',
        expiration_date: '15-Jan-2018',
        status: 'Open/Expired',
        probability: '20%',
        pipeline: 'Default',
        source: 'None',
        owner_name: 'kkjjk'
      },
      {
        time: '09:00AM',
        date: '09-Jan-2018',
        subtitle: 'Benjamin',
        title: 'Note Added',
        icon: require('../../assets/cash_view.png'),
        imageUrl: require('../../assets/cash_view.png'),
        //"https://cloud.githubusercontent.com/assets/21040043/24240419/1f553dee-0fe4-11e7-8638-6025682232b1.jpg",
        amount: '$4564.00',
        expiration_date: '15-Jan-2018',
        status: 'Open/Expired',
        probability: '20%',
        pipeline: 'Default',
        source: 'None',
        owner_name: null
      }
    ];

    this.renderDetails = this.renderDetails.bind(this);
  }

  renderDetails(rowData, sectionID, rowID) {
    let dateTime = (
      <View style={{ flexGrow: 1, flexDirection: 'row' }}>
        <View style={{ flexGrow: 1, fontSize: 12, color: '#A9A9A9A9' }}>
          <Text style={[styless.fontPrimary_Regular, { fontSize: 12 }]}>
            {rowData.date}
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            fontSize: 12,
            color: '#A9A9A9A9',
            paddingLeft: 55
          }}
        >
          <Text style={[styless.fontPrimary_Regular, { fontSize: 12 }]}>
            {rowData.time}
          </Text>
        </View>
      </View>
    );
    let title = <Text style={[styles.title]}>{rowData.title}</Text>;
    let subtitle = <Text style={[styles.subtitle]}>{rowData.subtitle}</Text>;
    // var desc = null;
    const x = 2;
    if (x == 2) {
      desc = (
        <View>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 5 }}>
              <Text>Amount</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 5 }}>
              <Text style={styles.text} numberOfLines={1}>
                {rowData.amount}
              </Text>
            </View>
          </View>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 5 }}>
              <Text>Expiraton Date</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 5 }}>
              <Text style={styles.text}>{rowData.expiration_date}</Text>
            </View>
          </View>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 5 }}>
              <Text>Status</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 5 }}>
              <Text style={styles.text} numberOfLines={1}>
                {rowData.status}
              </Text>
            </View>
          </View>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 5 }}>
              <Text>Probability</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 5 }}>
              <Text style={styles.text} numberOfLines={1}>
                {rowData.probability}
              </Text>
            </View>
          </View>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 5 }}>
              <Text>Pipeline</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 5 }}>
              <Text style={styles.text} numberOfLines={1}>
                {rowData.pipeline}
              </Text>
            </View>
          </View>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 5 }}>
              <Text>Source</Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 5 }}>
              <Text style={styles.text} numberOfLines={1}>
                {rowData.source}
              </Text>
            </View>
          </View>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 5 }}>
              <Text>Owner Name</Text>
            </View>
            {/* <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 5 }}>
              <Text style={styles.text} numberOfLines={1}>
                {rowData.owner_name}
              </Text>
            </View> */}
            <View
              onStartShouldSetResponder={() => {
                alert('Hello Chat');
              }}
              style={{
                flex: 1,
                alignItems: 'flex-end',
                paddingRight: 10
              }}
            >
              {Icons.chat_mark()}
            </View>
          </View>
          <View
            style={{
              flex: 1,
              alignItems: 'center'
            }}
          >
            <Spacer />
            <Text
              style={{ fontSize: 25, fontWeight: 'bold', color: '#A9A9A9A9' }}
            >
              ...
            </Text>
          </View>
        </View>
      );

      return (
        <View style={{ flex: 1, paddingLeft: 40, paddingRight: 10 }}>
          <View
            style={{
              flex: 1,
              backgroundColor: '#ffffff',
              padding: 10
            }}
          >
            {dateTime}
            {title}
            {subtitle}
            {desc}
          </View>
        </View>
      );
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Spacer />
        <Timeline
          style={styles.list}
          data={this.data}
          timeContainerStyle={{
            minWidth: 0,
            paddingRight: 40
          }}
          showTime={false}
          circleSize={40}
          circleColor={circleColor === 'call' ? '#F44336' : '#7E69FA'}
          // circleColor="#7E69FA" //#F44336(red color)
          lineColor="#000000"
          lineWidth={0.5}
          innerCircle={'icon'}
          iconStyle={styles.icon}
          renderFullLine="true"
          renderDetail={this.renderDetails}
        />
      </View>
    );
  }
}
export default TimeLineListComponent;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#eeeeee',
    paddingLeft: 10
    // PaddingTop: 20
  },
  list: {
    flex: 1
  },
  icon: {
    height: 20,
    width: 20,
    justifyContent: 'flex-end'
  },
  image: {
    height: 20,
    width: 20
  },
  title: {
    fontSize: 18,
    color: '#000000',
    fontFamily: 'Raleway-Bold',
    paddingTop: 5,
    lineHeight: 30
  },
  subtitle: {
    fontSize: 15,
    lineHeight: 30,
    color: '#000',
    fontFamily: 'Raleway-SemiBold'
  },
  descriptionContainer: {
    flex: 1
  },
  textDescription: {
    color: 'gray',
    fontFamily: 'Raleway-Regular'
  }
});
