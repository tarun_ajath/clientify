import React, { Component } from 'react';
import { View, TextInput } from 'react-native';

const Line = props => {
  const { lineColor } = props;
  return (
    <View
      style={{
        height: 1,
        width: '100%',
        backgroundColor: lineColor
      }}
    />
  );
};

Line.propTypes = {};

Line.defaultProps = {
  lineColor: 'grey'
};

export default Line;
