import React, { Component } from 'react';
import { View, TextInput, Text, Picker, Alert } from 'react-native';
import PropTypes, { any } from 'prop-types';
import Icons from './Icons';

class PickerDropDown extends Component {
  constructor() {
    super();
    this.state = {
      language: 'Select a language',
      options: [{ title: 'Java', id: 0 }, { title: 'JavaScript', id: 1 }]
    };
  }

  _getSelectedValue = itemValue => {
    this.setState({ selected: itemValue });
  };

  render() {
    const {
      pickerArrayData,
      updateLocalState,
      pickerName,
      labelKey,
      labelValue
    } = this.props;
    return (
      <View
        style={{
          flexDirection: 'row'
        }}
      >
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            paddingLeft: 10
          }}
        />
        <View
          style={{
            flex: 7,
            height: 35,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <Picker
            mode="dropdown"
            selectedValue={this.state.selected}
            style={{ height: 30, width: 170 }}
            onValueChange={itemValue => {
              console.log('sel  ', itemValue);
              updateLocalState(pickerName, itemValue);
              this._getSelectedValue(itemValue);
            }}
          >
            {pickerArrayData.map((item, index) => {
              return (
                <Picker.Item
                  label={item[labelKey]}
                  key={index}
                  value={item[labelValue]}
                />
              );
            })}
          </Picker>

          {/* <Picker
            mode="dropdown"
            selectedValue={this.state.language}
            style={{ height: 30, width: 165 }}
            onValueChange={(itemValue, itemIndex) =>
              this.setState({ language: itemValue })
            }
          >
            <Picker.Item label="C" value="C" />
            <Picker.Item label="C++" value="c++" />
            <Picker.Item label="Data Structure" value="ds" />
            <Picker.Item label="JavaScript" value="js" />
            <Picker.Item label="React" value="react" />
            <Picker.Item label="Node" value="node" />
            <Picker.Item label="Python" value="py" />
            <Picker.Item label="Angular" value="ng" />
            <Picker.Item label="HTML" value="html" />
            <Picker.Item label="Native" value="native" />
            <Picker.Item label="Ionic" value="ion" />
            <Picker.Item label="PHP" value="php" />
            <Picker.Item label="Laravel" value="laravel" />
            <Picker.Item label="Vue.js" value="vue" />
            <Picker.Item label="Angular JS" value="ng-js" />
            <Picker.Item label="TypeScript" value="ts" />
          </Picker> */}
        </View>
      </View>
    );
  }
}

export default PickerDropDown;
