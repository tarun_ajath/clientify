import React, { Component } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import Icons from './Icons';
import styless from './styles';

let firstName = '';

class CompanyListItem extends Component {
  render() {
    const { name } = this.props;

    return (
      <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 10 }}>
        <View style={{ flex: 2, padding: 5 }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#00bfff',
              height: 50,
              width: 50,
              borderRadius: 25
            }}
          >
            {Icons.company()}
          </View>
        </View>
        <View
          style={{
            flex: 10,
            alignContent: 'center',
            justifyContent: 'center',
            padding: 5
          }}
        >
          <View style={styles.name}>
            <Text style={[styless.fontPrimary_monsMedium, {}]}>
              {this.props.name}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

export default CompanyListItem;
const styles = StyleSheet.create({
  name: {
    justifyContent: 'center',
    alignItems: 'flex-start'
    // height: 50
  },
  text: {
    // fontWeight: 'bold'
  },
  textContainer: {
    color: '#000',
    fontWeight: 'bold',
    fontSize: 20
  }
});
