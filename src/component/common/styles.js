import { moderateScale, verticalScale, scale } from 'react-native-size-matters';

const styles = {
  fontPrimary_monsBold: {
    fontFamily: 'Montserrat-Bold'
  },
  fontPrimary_monsSemiBold: {
    fontFamily: 'Montserrat-SemiBold'
  },
  fontPrimary_monsRegular: {
    fontFamily: 'Montserrat-Regular'
  },
  fontPrimary_monsMedium: {
    fontFamily: 'Montserrat-Medium'
  },
  fontPrimary_Bold: {
    fontFamily: 'Raleway-Bold'
  },
  fontPrimary_extraBold: {
    fontFamily: 'Raleway-ExtraBold'
  },
  fontPrimary_Light: {
    fontFamily: 'Raleway-Light'
  },
  fontPrimary_Medium: {
    fontFamily: 'Raleway-Medium'
  },
  fontPrimary_Regular: {
    fontFamily: 'Raleway-Regular'
  },
  fontPrimary_semiBold: {
    fontFamily: 'Raleway-SemiBold'
  },
  smallText_L: {
    fontSize: verticalScale(11)
  },
  btnText: {
    fontSize: verticalScale(12)
  },
  footerText: {
    fontSize: verticalScale(12)
  },
  textSize8: {
    fontSize: verticalScale(8)
  },
  textSize9: {
    fontSize: verticalScale(9)
  },
  textSize10: {
    fontSize: verticalScale(10)
  },
  textSize11: {
    fontSize: verticalScale(11)
  },
  textSize12: {
    fontSize: verticalScale(12)
  },
  textSize13: {
    fontSize: verticalScale(13)
  },
  textSize14: {
    fontSize: verticalScale(14)
  },
  textSize15: {
    fontSize: verticalScale(15)
  },
  textSize16: {
    fontSize: verticalScale(16)
  },
  textSize17: {
    fontSize: verticalScale(17)
  },
  textSize18: {
    fontSize: verticalScale(18)
  },
  textSize19: {
    fontSize: verticalScale(19)
  },
  textSize20: {
    fontSize: verticalScale(20)
  },
  textSize22: {
    fontSize: verticalScale(22)
  }
};

export default styles;
