import React, { Component } from 'react';
import {
  ScrollView,
  View,
  Text,
  StyleSheet,
  CheckBox,
  Image,
  TouchableOpacity
} from 'react-native';
import Icons from './Icons';
import styless from './styles';

class ActivitiesListItem extends Component {
  constructor() {
    super();
    this.state = {
      checked: true
    };
  }

  render() {
    const {
      task,
      type,
      due_date,
      assigned_to,
      status,
      event,
      start,
      end,
      contact,
      place,
      deals,
      viewType
    } = this.props;
    if (this.props.viewType == 'Task') {
      return (
        <View style={{ marginTop: 15, marginBottom: 10 }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Task
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {this.props.task}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Type
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {this.props.type}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Due Date
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {this.props.due_date}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Assigned to
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {this.props.assigned_to}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Status
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {this.props.status}
              </Text>
            </View>
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              paddingTop: 10,
              paddingLeft: 15,
              paddingRight: 20,
              paddingBottom: 5
            }}
          >
            <View
              style={{
                justifyContent: 'flex-start',
                alignItems: 'center'
              }}
            >
              <CheckBox
                value={this.state.checked}
                onValueChange={() =>
                  this.setState({ checked: !this.state.checked })
                }
              />
            </View>
            <View
              style={{
                flex: 5,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center'
              }}
            >
              <Text
                style={[
                  styless.fontPrimary_Regular,
                  styless.textSize13,
                  {
                    color: 'grey'
                  }
                ]}
                numberOfLines={1}
              >
                Mark as completed
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: 'flex-end',
                alignItems: 'center'
              }}
            >
              <TouchableOpacity>
                <View style={styles.circle}>
                  <View>{Icons.message_orange_background()}</View>
                </View>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ height: 1, width: '100%', backgroundColor: '#000' }} />
        </View>
      );
    } else if (this.props.viewType == 'Events') {
      return (
        <View style={{ marginTop: 15, marginBottom: 10 }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Event
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {this.props.event}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Start
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {this.props.start}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                End
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {this.props.end}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Place
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {this.props.place}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Contact/Companies
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {this.props.contact}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Deals
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {this.props.deals}
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              paddingTop: 10,
              paddingLeft: 5,
              paddingRight: 20,
              paddingBottom: 5
            }}
          >
            {/* <View
              style={{
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'center'
              }}
            >
              <CheckBox
                value={this.state.checked}
                onValueChange={() =>
                  this.setState({ checked: !this.state.checked })
                }
              />
            </View> */}
            {/* <View
              style={{
                flex: 5,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
                paddingLeft: 0
              }}
            >
              <Text>Mark as completed</Text>
            </View> */}
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-end',
                alignItems: 'center'
              }}
            >
              <TouchableOpacity>
                <View style={styles.circleEvent}>
                  <View>{Icons.event_blue()}</View>
                </View>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ height: 1, width: '100%', backgroundColor: '#000' }} />
        </View>
      );
    }
  }
}
export default ActivitiesListItem;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {},
  circle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5921d',
    height: 40,
    width: 40,
    borderRadius: 50,
    resizeMode: 'contain'
  },
  circleEvent: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#00bfff',
    height: 40,
    width: 40,
    borderRadius: 50,
    resizeMode: 'contain'
  }
});
