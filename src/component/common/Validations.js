import React from 'react';
import { Image, Dimensions } from 'react-native';
const { width } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/FontAwesome';

// const { ToastProvider } = this.props.screenProps;

function validateEmail(email) {
  let regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
  if (regexEmail.test(email)) {
    return true;
  } else {
    alert('wrong email');
    // ToastProvider.showToast('Email is not Valid', 'error');
    return false;
  }
}

export default {
  validateEmail
};
