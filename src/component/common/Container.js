import React, { Component, Fragment } from 'react';
import { View, SafeAreaView, Platform, StatusBar } from 'react-native';

class Container extends Component {
  iosContainer = () => {
    const { style, children } = this.props;
    return (
      <Fragment>
        <SafeAreaView
          style={{
            backgroundColor: '#00b2e2',
            flex: 0
          }}
        />
        <SafeAreaView
          style={{
            ...style,
            flex: 1
          }}
        >
          <View
            style={{
              ...style,
              flex: 1
            }}
          >
            {children}
          </View>
        </SafeAreaView>
      </Fragment>
    );
  };

  androidContainer = () => {
    const { style, children } = this.props;
    return <View style={{ ...style, flex: 1 }}>{children}</View>;
  };

  render() {
    return Platform.OS == 'ios' ? this.iosContainer() : this.androidContainer();
  }
}

export default Container;
