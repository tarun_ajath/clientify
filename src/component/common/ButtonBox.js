import React, { Component } from 'react';
import {
  View,
  TextInput,
  TouchableNativeFeedback,
  Text,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native';
import { Button } from 'react-native-paper';
import PropTypes from 'prop-types';
import Icons from './Icons';
import styles from './styles';

class ButtonBox extends Component {
  render() {
    const {
      btnColor,
      btnText,
      onPress,
      btnTextColor,
      btnImage,
      btnImageRight,
      btnLoaderBoolean
    } = this.props;
    return (
      <View
        onStartShouldSetResponder={() => {
          {
            onPress();
          }
        }}
        style={{
          flexDirection: 'row',
          borderRadius: 30,
          backgroundColor: btnColor,
          padding: 5
        }}
      >
        {/* <TouchableOpacity onPress={onPress}> */}
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View
            style={{
              flex: 2,
              // backgroundColor: 'green',
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            {btnImage()}
          </View>
          <View
            style={{
              height: 35,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            {/* <Button
            icon={btnImage}
            uppercase={false}
            color={btnTextColor}
            mode="text"
            onPress={onPress}
          >
            {btnText}
          </Button> */}
            {btnLoaderBoolean == true ? (
              <ActivityIndicator size="small" color={'#00b2e2'} />
            ) : (
              <Text
                style={[
                  styles.fontPrimary_semiBold,
                  styles.textSize14,
                  { color: btnTextColor }
                ]}
              >
                {btnText}
              </Text>
            )}
          </View>
          <View
            style={{
              flex: 2,
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            {btnImageRight()}
          </View>
        </View>
        {/* </TouchableOpacity> */}
      </View>
    );
  }
}

ButtonBox.propTypes = {};

ButtonBox.defaultProps = {
  btnColor: 'red',
  btnText: 'Click',
  btnTextColor: '#fff',
  btnImage: () => {
    // alert('Hello');
  },
  btnImageRight: () => {
    // alert('Hello');
  }
};

export default ButtonBox;
