import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import Timeline from 'react-native-timeline-listview';
import Spacer from './Spacer';
import Icons from './Icons';
import styless from './styles';

class TimelineComponent extends Component {
  constructor(props) {
    super(props);
    this.data = [
      {
        time: '09:00AM',
        date: '09-Jan-2018',
        subtitle: 'Benjamin',
        title: 'Note Added',
        icon: require('../../assets/note_view.png'),
        imageUrl: require('../../assets/note_view.png'),
        //"https://cloud.githubusercontent.com/assets/21040043/24240419/1f553dee-0fe4-11e7-8638-6025682232b1.jpg",
        description:
          'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eum voluptas adipisci magni fugiat alias sequi officia suscipit dolorum dolor, ullam quo atque, cupiditate accusamus minus consequuntur necessitatibus quibusdam officiis minima.'
      },
      {
        time: '09:00AM',
        date: '09-Jan-2018',
        subtitle: 'Benjamin',
        title: 'Note Added',
        icon: require('../../assets/note_view.png'),
        imageUrl: require('../../assets/note_view.png'),
        //"https://cloud.githubusercontent.com/assets/21040043/24240419/1f553dee-0fe4-11e7-8638-6025682232b1.jpg",
        description:
          'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eum voluptas adipisci magni fugiat alias sequi officia suscipit dolorum dolor, ullam quo atque, cupiditate accusamus minus consequuntur necessitatibus quibusdam officiis minima.'
      },
      {
        time: '09:00AM',
        date: '09-Jan-2018',
        subtitle: 'Benjamin',
        title: 'Note Added',
        icon: require('../../assets/note_view.png'),
        imageUrl: require('../../assets/note_view.png'),
        //"https://cloud.githubusercontent.com/assets/21040043/24240419/1f553dee-0fe4-11e7-8638-6025682232b1.jpg",
        description:
          'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Eum voluptas adipisci magni fugiat alias sequi officia suscipit dolorum dolor, ullam quo atque, cupiditate accusamus minus consequuntur necessitatibus quibusdam officiis minima.'
      }
    ];

    this.renderDetails = this.renderDetails.bind(this);
  }

  renderDetails(rowData, sectionID, rowID) {
    let dateTime = (
      <View style={{ flexGrow: 1, flexDirection: 'row' }}>
        <View style={{ flexGrow: 1, fontSize: 12, color: '#A9A9A9A9' }}>
          <Text style={[styless.fontPrimary_Regular, { fontSize: 12 }]}>
            {rowData.date}
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            fontSize: 12,
            color: '#A9A9A9A9',
            paddingLeft: 55
          }}
        >
          <Text style={[styless.fontPrimary_Regular, { fontSize: 12 }]}>
            {rowData.time}
          </Text>
        </View>
      </View>
    );
    let title = <Text style={[styles.title]}>{rowData.title}</Text>;
    let subtitle = <Text style={[styles.subtitle]}>{rowData.subtitle}</Text>;
    var desc = null;

    if (rowData.description && rowData.imageUrl) {
      desc = (
        <View>
          <View style={styles.descriptionContainer}>
            <View style={{ flex: 1 }}>
              <Text style={[styles.textDescription]}>
                {rowData.description}
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                alignItems: 'flex-end',
                paddingRight: 10
              }}
            >
              {Icons.chat_mark()}
            </View>
          </View>
          <View
            style={{
              flex: 1,
              alignItems: 'center'
              // justifyContent: 'center',
              // flexDirection: 'row'
            }}
          >
            <Text
              style={{ fontSize: 25, fontWeight: 'bold', color: '#A9A9A9A9' }}
            >
              ...
            </Text>
          </View>
        </View>
      );

      return (
        <View style={{ flex: 1, paddingLeft: 40, paddingRight: 10 }}>
          <View
            style={{
              flex: 1,
              backgroundColor: '#ffffff',
              padding: 10
            }}
          >
            {dateTime}
            {title}
            {subtitle}
            {desc}
          </View>
        </View>
      );
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Spacer />
        <Timeline
          style={styles.list}
          data={this.data}
          timeContainerStyle={{
            minWidth: 0,
            paddingRight: 40
          }}
          showTime={false}
          circleSize={40}
          circleColor="#00b2e2"
          // options={{ marginTop: 5 }}
          // timeContainerStyle={{ marginTop: 100, paddingTop: 50, color: 'red' }}
          lineColor="#000000"
          lineWidth={0.5}
          innerCircle={'icon'}
          iconStyle={styles.icon}
          renderFullLine="true"
          renderDetail={this.renderDetails}
        />
      </View>
    );
  }
}
export default TimelineComponent;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#eeeeee',
    paddingLeft: 10
    // PaddingTop: 20
  },
  iconMargin: {
    marginTop: 50
  },
  list: {
    flex: 1
  },
  icon: {
    height: 20,
    width: 20,
    justifyContent: 'flex-end'
  },
  image: {
    height: 20,
    width: 20
  },
  title: {
    fontSize: 18,
    color: '#000000',
    fontFamily: 'Raleway-Bold',
    paddingTop: 5,
    lineHeight: 30
  },
  subtitle: {
    fontSize: 15,
    lineHeight: 30,
    color: '#000',
    fontFamily: 'Raleway-SemiBold'
  },
  descriptionContainer: {
    flex: 1
  },
  textDescription: {
    color: 'gray',
    fontFamily: 'Raleway-Regular'
  }
});
