import React, { Component } from "react";
import { View, TouchableOpacity, Text, Dimensions } from "react-native";
import Container from "./Container";
import { FlatList } from "react-native-gesture-handler";
import HttpClient from "../../utilities/HttpClient";

const { height } = Dimensions.get("window");

class DealList extends Component {
  constructor() {
    super();
    this.state = {
      dealListFromApi: []
    };
  }
  async componentDidMount() {
    try {
      let res = await HttpClient.deals();
      this.setState({
        dealListFromApi: res.data.results
      });
      console.log("list of deals  ", res);
    } catch (error) {
      console.log("error of deal is ", error);
    }
  }

  DealList = (item, index) => {
    return (
      <View style={{ padding: 10 }}>
        <View style={{ paddingHorizontal: 10 }}>
          <TouchableOpacity
            onPress={() => {
              this.props.dealsInfo(item);
            }}
          >
            <Text style={{ fontSize: 15 }}>{item.name}</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            height: 1,
            width: "100%",
            backgroundColor: "grey",
            marginTop: 5
          }}
        />
      </View>
    );
  };
  render() {
    return (
      <Container>
        <View style={{ flex: 1 }}>
          <FlatList
            data={this.state.dealListFromApi}
            renderItem={({ item, index }) => this.DealList(item, index)}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </Container>
    );
  }
}

export default DealList;
