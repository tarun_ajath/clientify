import React, { Fragment } from "react";
import { Platform, SafeAreaView, View } from "react-native";

const ComponentContainer = props => {
  return Platform.OS == "ios" ? (
    <Fragment>
      <SafeAreaView
        style={{
          flex: 0,
          backgroundColor: props.iosStatusbarColor || Theme.colors.primary
        }}
      />
      <SafeAreaView style={{ flex: 1 }}>{props.children}</SafeAreaView>
    </Fragment>
  ) : (
    <View style={{ flex: 1 }}>{props.children}</View>
  );
};

export default ComponentContainer;
