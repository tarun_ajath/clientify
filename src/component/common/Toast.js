import React, { Component } from 'react';
import { Snackbar } from 'react-native-paper';
import ThemeHandler from '../../utilities/ThemeHandler';

class Toast extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      toastShow,
      hideToast,
      toastColor,
      toastMessage
    } = this.props.context.ToastProvider;
    return (
      <Snackbar
        visible={toastShow}
        onDismiss={hideToast}
        style={{
          backgroundColor: this.fetchColor(toastColor)
        }}
        duration={1500}
      >
        {toastMessage}
      </Snackbar>
    );
  }

  fetchColor(color) {
    return ThemeHandler.getColor(color);
  }
}
export default Toast;
