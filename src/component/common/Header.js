import * as React from 'react';
import { Appbar } from 'react-native-paper';
import PropTypes from 'prop-types';
import Icons from './Icons';
import styles from './styles';
class Header extends React.Component {
  _goBack = () => this.props.navigation.navigate('Login');

  //   _onSearch = () => console.log('Searching');

  //   _onMore = () => console.log('Shown more');

  render() {
    const {
      titleName,
      leftTitleImg,
      RightTitleImg,
      onPress,
      onPressBack
    } = this.props;
    return (
      <Appbar
        style={{
          backgroundColor: '#00b2e2'
        }}
      >
        {/* <Appbar.Header> */}
        {/* <Appbar.BackAction onPress={this._goBack} icon={null} /> */}
        <Appbar.Action icon={leftTitleImg} onPress={onPressBack} />
        <Appbar.Content
          title={titleName}
          // style={{ backgroundColor: 'red' }}
          subtitle=""
          titleStyle={[
            styles.textSize17,
            {
              color: '#fff',

              textAlign: 'center',
              fontFamily: 'Montserrat-Medium'
            }
          ]}
        />
        <Appbar.Action icon={RightTitleImg} onPress={onPress} />
        {/* <Appbar.Action icon="more-vert" onPress={this._onMore} /> */}
        {/* </Appbar.Header> */}
      </Appbar>
    );
  }
}

Header.propTypes = {};

Header.defaultProps = {
  titleName: 'Homescreen',
  leftTitleImg: () => {
    // alert('Hello');
  },
  RightTitleImg: () => {
    // alert('Hello');
  }
};

export default Header;
