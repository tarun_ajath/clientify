import React, { Component } from 'react';
import { View, TextInput, Text } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './styles';

class InputBox extends Component {
  constructor() {
    super();
    this.state = {
      active: false
    };
  }

  onFocus = () => {
    this.setState({
      active: true
    });
  };

  onBlur = () => {
    this.setState({
      active: false
    });
  };

  render() {
    const {
      placeholder,
      borderColor,
      activeBorderColor,
      imageName,
      rightImageName,
      value,
      updateLocalState,
      name,
      onTextChangeMethod
    } = this.props;
    const { active } = this.state;
    return (
      <View
        style={{
          height: 48,
          flexDirection: 'row',
          borderColor: active ? activeBorderColor : borderColor,
          borderWidth: 2,
          borderRadius: 30,
          backgroundColor: '#fff'
        }}
      >
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            marginLeft: 10
            // backgroundColor: 'red'
          }}
        >
          <Icon
            name={imageName}
            size={20}
            color={active ? '#00b2e2' : 'black'}
          />
        </View>
        <View
          style={{
            flex: 7,
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <TextInput
            placeholder={placeholder}
            onBlur={this.onBlur}
            onFocus={this.onFocus}
            style={[
              styles.fontPrimary_Regular,
              styles.textSize13,
              {
                textAlign: 'center',
                color: active ? activeBorderColor : borderColor,
                width: '100%'
              }
            ]}
            value={value}
            onChangeText={text => {
              updateLocalState(name, text);
              onTextChangeMethod(text);
            }}
          />
        </View>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            marginRight: 10
          }}
        >
          {rightImageName()}
        </View>
      </View>
    );
  }
}

InputBox.propTypes = {};

InputBox.defaultProps = {
  placeholder: 'email',
  textColor: 'black',
  borderColor: 'grey',
  activeBorderColor: '#00b2e2',
  imageName: null,
  rightImageName: () => {},
  onTextChangeMethod: () => {}
};

export default InputBox;
