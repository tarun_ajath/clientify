import React, { Component } from 'react';
import {
  ScrollView,
  View,
  Text,
  StyleSheet,
  CheckBox,
  Image,
  TouchableOpacity
} from 'react-native';
import Icons from './Icons';
import styless from './styles';

class DealsListItem extends Component {
  constructor() {
    super();
    this.state = {
      // checked: true
    };
  }

  render() {
    const {
      task,
      type,
      due_date,
      assigned_to,
      status,
      event,
      start,
      end,
      contact,
      place,
      deals,
      viewType
    } = this.props;
    if (this.props.viewType == 'Task') {
      return (
        <View style={{ marginTop: 15, marginBottom: 10 }}>
          <View
            onStartShouldSetResponder={() => {
              // this.props.navigation.navigate('DealsDetailsScreen');
            }}
            style={{ flexDirection: 'row' }}
          >
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Name
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {this.props.task}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Contact no.
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {this.props.type}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Process
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {this.props.due_date}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Close date
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {this.props.assigned_to}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Amount
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {this.props.status}
              </Text>
            </View>
          </View>

          <View
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#00b2e2'
            }}
          >
            <View style={{ flex: 4, paddingLeft: 10, flexDirection: 'row' }}>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'flex-start',
                  paddingRight: 10
                }}
              >
                {Icons.clock_status_open()}
              </View>
              <Text
                style={[
                  styless.fontPrimary_Regular,
                  styless.textSize13,
                  { color: '#fff' }
                ]}
                numberOfLines={1}
              >
                Status
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text style={styles.textWhite}>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#fff'
                  }
                ]}
                numberOfLines={1}
              >
                Active
              </Text>
            </View>
          </View>

          <View style={{ height: 1, width: '100%', backgroundColor: '#000' }} />
        </View>
      );
    } else if (this.props.viewType == 'Events') {
      return (
        <View style={{ marginTop: 15, marginBottom: 10 }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Name
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {this.props.event}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Contact no.
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {this.props.start}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Process
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {this.props.end}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Close Date
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {this.props.place}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Amount
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {this.props.contact}
              </Text>
            </View>
          </View>

          <View
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#f5921e'
            }}
          >
            {/* <View style={{ flex: 4, paddingLeft: 20 }}> */}
            <View style={{ flex: 4, paddingLeft: 10, flexDirection: 'row' }}>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'flex-start',
                  paddingRight: 10
                }}
              >
                {Icons.clock_status_inactive()}
              </View>
              <Text
                style={[
                  styless.fontPrimary_Regular,
                  styless.textSize13,
                  { color: '#fff' }
                ]}
                numberOfLines={1}
              >
                Status
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text style={styles.textWhite}>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#fff'
                  }
                ]}
                numberOfLines={1}
              >
                Expired
              </Text>
            </View>
          </View>
          <View style={{ height: 1, width: '100%', backgroundColor: '#000' }} />
        </View>
      );
    }
  }
}
export default DealsListItem;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    color: '#000',
    fontSize: 15,
    fontFamily: 'Raleway-SemiBold'
  },
  textWhite: {
    color: '#fff',
    fontSize: 15,
    fontFamily: 'Raleway-SemiBold'
  }
});
