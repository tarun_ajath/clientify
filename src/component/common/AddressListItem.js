import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import styles from './styles';

let firstName = '';

class AddressListItem extends Component {
  splittedName = '';
  getInitials = () => {
    const { name } = this.props;
    // this.splittedName = name.split(' ').reduce((previous, current) => {
    //     return {v:previous[0]+ current[0]}
    // })
    // alert(this.splittedName.v);
    // firstName=this.splittedName.v;
    this.splittedName = name
      .split(/\W/)
      .map(item => {
        for (var i = 0; i < item.length; i++) {
          return item[i];
        }
      })
      .join('');
    firstName = this.splittedName;
  };

  render() {
    const { name, mail, splittedName } = this.props;
    {
      this.getInitials();
    }
    return (
      <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 10 }}>
        <View style={{ flex: 2, alignContent: 'center', padding: 5 }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#00b2e2',
              height: 50,
              width: 50,
              borderRadius: 25
            }}
          >
            <Text
              style={[
                styles.fontPrimary_monsMedium,
                { color: '#fff', fontSize: 20 }
              ]}
            >
              {firstName}
            </Text>
          </View>
        </View>
        <View style={{ flex: 10, alignContent: 'center', padding: 5 }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'flex-start',
              height: 50
            }}
          >
            <Text
              style={[
                styles.fontPrimary_monsMedium,
                { color: '#000', fontSize: 18 }
              ]}
            >
              {this.props.name}
            </Text>
            <Text
              style={[
                styles.fontPrimary_Bold,
                { color: '#7f7f7f', fontSize: 14 }
              ]}
            >
              {this.props.mail}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

export default AddressListItem;
