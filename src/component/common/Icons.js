import React from 'react';
import { Image, Dimensions } from 'react-native';
const { width } = Dimensions.get('window');
import Icon from 'react-native-vector-icons/FontAwesome';

function contacts_active() {
  return (
    <Image
      source={require('../../assets/contacts_active.png')}
      resizeMode={'contain'}
      style={{ width: 18, height: 18 }}
    />
  );
}

function contacts_active_dealDetails() {
  return (
    <Image
      source={require('../../assets/contacts_active.png')}
      resizeMode={'contain'}
      style={{ width: 30, height: 30 }}
    />
  );
}

function contacts_inactive() {
  return (
    <Image
      source={require('../../assets/contacts_inactive.png')}
      resizeMode={'contain'}
      style={{ width: 18, height: 18 }}
    />
  );
}

function deal_active() {
  return (
    <Image
      source={require('../../assets/deal_active.png')}
      resizeMode={'contain'}
      style={{ width: 35, height: 35 }}
    />
  );
}
function deal_inactive() {
  return (
    <Image
      source={require('../../assets/deal_inactive.png')}
      resizeMode={'contain'}
      style={{ width: 35, height: 35 }}
    />
  );
}

function activity_active() {
  return (
    <Image
      source={require('../../assets/activity_active.png')}
      resizeMode={'contain'}
      style={{ width: 18, height: 18 }}
    />
  );
}

function activity_inactive() {
  return (
    <Image
      source={require('../../assets/activity_inactive.png')}
      resizeMode={'contain'}
      style={{ width: 18, height: 18 }}
    />
  );
}

function setting_active() {
  return (
    <Image
      source={require('../../assets/setting_active.png')}
      resizeMode={'contain'}
      style={{ width: 18, height: 18 }}
    />
  );
}

function setting_inactive() {
  return (
    <Image
      source={require('../../assets/setting_inactive.png')}
      resizeMode={'contain'}
      style={{ width: 18, height: 18 }}
    />
  );
}

function company() {
  return (
    <Image
      source={require('../../assets/company.png')}
      resizeMode={'contain'}
    />
  );
}

function profile_back() {
  return (
    <Image
      source={require('../../assets/back.png')}
      resizeMode={'contain'}
      style={{ width: 22, height: 22 }}
    />
  );
}

function profile_edit() {
  return (
    <Image
      source={require('../../assets/edit.png')}
      resizeMode={'contain'}
      style={{ width: 22, height: 22 }}
    />
  );
}

function profile_delete() {
  return (
    <Image
      source={require('../../assets/delete.png')}
      resizeMode={'contain'}
      style={{ width: 22, height: 22 }}
    />
  );
}

function profile_note() {
  return (
    <Image
      source={require('../../assets/note.png')}
      resizeMode={'contain'}
      style={{ width: 32, height: 32 }}
    />
  );
}

function profile_task() {
  return (
    <Image
      source={require('../../assets/task.png')}
      resizeMode={'contain'}
      style={{ width: 32, height: 32 }}
    />
  );
}

function profile_event() {
  return (
    <Image
      source={require('../../assets/event.png')}
      resizeMode={'contain'}
      style={{ width: 32, height: 32 }}
    />
  );
}

function profile_email() {
  return (
    <Image
      source={require('../../assets/email.png')}
      resizeMode={'contain'}
      style={{ width: 32, height: 32 }}
    />
  );
}

function notification() {
  return (
    <Image
      source={require('../../assets/notification_light.png')}
      resizeMode={'contain'}
    />
  );
}

function message_orange_background() {
  return (
    <Image
      source={require('../../assets/message_orange_background.png')}
      resizeMode={'contain'}
    />
  );
}

function event_blue() {
  return (
    <Image
      source={require('../../assets/event_blue.png')}
      resizeMode={'contain'}
      style={{ width: 35, height: 35 }}
    />
  );
}

function my_account() {
  return (
    <Image
      source={require('../../assets/my_account.png')}
      resizeMode={'contain'}
      style={{ width: 35, height: 35 }}
    />
  );
}

function notification() {
  return (
    <Image
      source={require('../../assets/notification.png')}
      resizeMode={'contain'}
      style={{ width: 20, height: 20 }}
    />
  );
}

function contact_us() {
  return (
    <Image
      source={require('../../assets/contact_us.png')}
      resizeMode={'contain'}
      style={{ width: 20, height: 20 }}
    />
  );
}

function term_of_services() {
  return (
    <Image
      source={require('../../assets/term_of_services.png')}
      resizeMode={'contain'}
      style={{ width: 20, height: 20 }}
    />
  );
}

function about_us() {
  return (
    <Image
      source={require('../../assets/about_us.png')}
      resizeMode={'contain'}
      style={{ width: 20, height: 20 }}
    />
  );
}

function english() {
  return (
    <Image
      source={require('../../assets/english.png')}
      resizeMode={'contain'}
      style={{ width: 17, height: 17 }}
    />
  );
}

function spanish() {
  return (
    <Image
      source={require('../../assets/spanish.png')}
      resizeMode={'contain'}
      style={{ width: 17, height: 17 }}
    />
  );
}

function clock() {
  return (
    <Image
      source={require('../../assets/clock.png')}
      resizeMode={'contain'}
      style={{ width: 15, height: 15 }}
    />
  );
}

function google() {
  return (
    <Image source={require('../../assets/google.png')} resizeMode={'contain'} />
  );
}

function logout() {
  return (
    <Image
      source={require('../../assets/logout.png')}
      resizeMode={'contain'}
      style={{ width: 17, height: 17 }}
    />
  );
}

function location() {
  return (
    <Image
      source={require('../../assets/location.png')}
      resizeMode={'contain'}
    />
  );
}

function chat_mark() {
  return (
    <Image
      source={require('../../assets/chat_mark.png')}
      resizeMode={'contain'}
    />
  );
}

function notification_light() {
  return (
    <Image
      source={require('../../assets/notification_light.png')}
      resizeMode={'contain'}
      style={{ width: 18, height: 18 }}
    />
  );
}

function add_deals() {
  return (
    <Image
      source={require('../../assets/add_deals.png')}
      resizeMode={'contain'}
      style={{ width: 18, height: 18 }}
    />
  );
}
function location() {
  return (
    <Image
      source={require('../../assets/location.png')}
      resizeMode={'contain'}
      style={{ width: 18, height: 18 }}
    />
  );
}

function calender() {
  return (
    <Image
      source={require('../../assets/calender.png')}
      resizeMode={'contain'}
      style={{ width: 15, height: 15 }}
    />
  );
}

function cash_view() {
  return (
    <Image
      source={require('../../assets/cash_view.png')}
      resizeMode={'contain'}
      style={{ width: 15, height: 15 }}
    />
  );
}

function dropdown() {
  return (
    <Image
      source={require('../../assets/dropdown.png')}
      resizeMode={'contain'}
      style={{ width: 15, height: 15 }}
    />
  );
}

function clock_status_inactive() {
  return (
    <Image
      source={require('../../assets/clock_status_inactive.png')}
      resizeMode={'contain'}
      style={{ width: 15, height: 15 }}
    />
  );
}

function clock_status_open() {
  return (
    <Image
      source={require('../../assets/clock_status_open.png')}
      resizeMode={'contain'}
      style={{ width: 15, height: 15 }}
    />
  );
}

function add() {
  return (
    <Image
      source={require('../../assets/add.png')}
      resizeMode={'contain'}
      style={{ width: 12, height: 12 }}
    />
  );
}

function add_contacts() {
  return (
    <Image
      source={require('../../assets/add_contacts.png')}
      resizeMode={'contain'}
      style={{ width: 15, height: 15 }}
    />
  );
}

function add_company() {
  return (
    <Image
      source={require('../../assets/add_company.png')}
      resizeMode={'contain'}
      style={{ width: 22, height: 22 }}
    />
  );
}

function contact_customer() {
  return (
    <Image
      source={require('../../assets/contact_customer.png')}
      resizeMode={'contain'}
      // style={{ width: 22, height: 22 }}
    />
  );
}

function msg_customer() {
  return (
    <Image
      source={require('../../assets/msg_customer.png')}
      resizeMode={'contain'}
      // style={{ width: 22, height: 22 }}
    />
  );
}

function deal_custumer() {
  return (
    <Image
      source={require('../../assets/deal_custumer.png')}
      resizeMode={'contain'}
      // style={{ width: 22, height: 22 }}
    />
  );
}

function deal_tag() {
  return (
    <Image
      source={require('../../assets/deal_tag.png')}
      resizeMode={'contain'}
      style={{ width: 14, height: 14 }}
    />
  );
}

function search() {
  return <Icon name="search" size={20} />;
}

function calender() {
  return (
    <Image
      source={require('../../assets/calender.png')}
      resizeMode={'contain'}
      style={{ width: 20, height: 20 }}
    />
  );
}

function deal() {
  return (
    <Image
      source={require('../../assets/deal.png')}
      resizeMode={'contain'}
      style={{ width: 14, height: 14 }}
    />
  );
}

function user() {
  return (
    <Image
      source={require('../../assets/user.png')}
      resizeMode={'contain'}
      style={{ width: 14, height: 14 }}
    />
  );
}

function office() {
  return (
    <Image
      source={require('../../assets/office.png')}
      resizeMode={'contain'}
      style={{ width: 14, height: 14 }}
    />
  );
}

function add_company_yellow() {
  return (
    <Image
      source={require('../../assets/add_company_yellow.png')}
      resizeMode={'contain'}
      style={{ width: 20, height: 20 }}
    />
  );
}

function contact_customer_yellow() {
  return (
    <Image
      source={require('../../assets/contact_customer_yellow.png')}
      resizeMode={'contain'}
      style={{ width: 20, height: 20 }}
    />
  );
}

function deal_custumer_yellow() {
  return (
    <Image
      source={require('../../assets/deal_custumer_yellow.png')}
      resizeMode={'contain'}
      style={{ width: 27, height: 27 }}
    />
  );
}

function deal_tag_yellow() {
  return (
    <Image
      source={require('../../assets/deal_tag_yellow.png')}
      resizeMode={'contain'}
      style={{ width: 20, height: 20 }}
    />
  );
}

function task_yellow() {
  return (
    <Image
      source={require('../../assets/task_yellow.png')}
      resizeMode={'contain'}
      style={{ width: 22, height: 22 }}
    />
  );
}

export default {
  contacts_active,
  contacts_inactive,
  deal_active,
  deal_inactive,
  activity_active,
  activity_inactive,
  setting_active,
  setting_inactive,
  company,
  add_company,
  location,
  calender,
  profile_back,
  profile_edit,
  profile_delete,
  profile_note,
  profile_task,
  profile_event,
  profile_email,
  notification,
  message_orange_background,
  event_blue,
  my_account,
  notification,
  term_of_services,
  contact_us,
  about_us,
  english,
  spanish,
  clock,
  google,
  logout,
  location,
  chat_mark,
  notification_light,
  add_deals,
  contacts_active_dealDetails,
  cash_view,
  dropdown,
  clock_status_inactive,
  clock_status_open,
  add,
  add_contacts,
  search,
  msg_customer,
  contact_customer,
  deal_custumer,
  deal_tag,
  calender,
  deal,
  user,
  office,
  add_company_yellow,
  contact_customer_yellow,
  deal_custumer_yellow,
  deal_tag_yellow,
  task_yellow
};
