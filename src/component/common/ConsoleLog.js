class ConsoleLog {
  enableLog;

  constructor() {
    this.enableLog = true;
  }

  log(...arg) {
    if (this.enableLog) console.log(...arg);
  }
}

export default new ConsoleLog();
