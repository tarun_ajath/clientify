import React from 'react';
import { Text } from 'react-native';

export default ({ size }) => {
  return <Text style={[{ fontSize: size || 5 }]}>{'\n'}</Text>;
};
