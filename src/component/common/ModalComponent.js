import React, { Component } from 'react';
import { View, Modal, ActivityIndicator } from 'react-native';

class ModalComponent extends Component {
  render() {
    return (
      <View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.props.visible}
        >
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'rgba(0, 0,0, 0.2)'
            }}
          >
            <ActivityIndicator size="large" color="#00b2e2" />
          </View>
        </Modal>
      </View>
    );
  }
}
export default ModalComponent;
