import React, { Component, Fragment } from 'react';
import {
  View,
  Image,
  Text,
  Dimensions,
  FlatList,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity
} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import InputBox from '../component/common/InputBox';
import { ScrollView } from 'react-native-gesture-handler';
import Container from '../component/common/Container';
import CompanyListItem from './common/CompanyListItem';
import styless from '../component/common/styles';
import HttpClient from '../utilities/HttpClient';
import Icons from '../component/common/Icons';
import { connect } from 'react-redux';
import {
  getCompanyList,
  updateCompanyState,
  getCompanyDetails
} from '../Actions/companyFormAction';

const { height } = Dimensions.get('window');
let companyListPageNo = 1;

class CompanyForm extends Component {
  constructor() {
    super();
    this.state = {
      refresh: false,
      searchCompanyResult: []
    };
  }

  componentDidMount() {
    this.props.getCompanyList(
      companyListPageNo,
      this.props.companyFormState.companyListArray
    );
  }

  checkForImageOrFirstLetterName(first_Name, picture_url, i) {
    if (picture_url == null && (first_Name == '' || first_Name == null)) {
    } else if (picture_url == null) {
      const first = this.getFirstLetterOfCompany(first_Name);
      return (
        <View>
          <Text
            style={[
              styles.fontPrimary_monsMedium,
              { color: '#fff', fontSize: 17 }
            ]}
          >
            {first.substr(0, 1)}
          </Text>
        </View>
      );
    } else {
      const { companyFormState, getCompanyList } = this.props;
      return (
        <View>
          <Image
            style={{ width: 50, height: 50, borderRadius: 25 }}
            resizeMode={'contain'}
            source={{
              uri: picture_url
            }}
            onError={() => {
              let newArr = [...companyFormState.companyListArray];
              newArr[i].picture_url = null;
              updateCompanyState('companyListArray', newArr);
              setTimeout(() => this.setState({ refresh: true }), 1000);
            }}
          />
        </View>
      );
    }
  }

  splittedName = '';
  getFirstLetterOfCompany(hey) {
    this.splittedName = hey
      .split(/\W/)
      .map(item => {
        for (var i = 0; i < item.length; i++) {
          return item[i];
        }
      })
      .join('');
    firstName = this.splittedName;
    return firstName;
  }
  searchCompany = async text => {
    try {
      let res = await HttpClient.companySearch(text);
      this.setState({
        searchCompanyResult: res.data.results
      });
    } catch (error) {
      console.log(error.response);
    }
  };

  nextPageData = async () => {
    companyListPageNo = companyListPageNo + 1;
    // alert('comes to end' + companyListPageNo);
    this.props.getCompanyList(
      companyListPageNo,
      this.props.companyFormState.companyListArray
    );
  };

  companyList(item, i) {
    return (
      <View style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 10 }}>
        <View style={{ flex: 2, padding: 5 }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#00bfff',
              height: 50,
              width: 50,
              borderRadius: 25
            }}
          >
            {/* {Icons.add_company()} */}
            {this.checkForImageOrFirstLetterName(
              item.name,
              item.picture_url,
              i
            )}
          </View>
        </View>
        <View
          style={{
            flex: 10,
            alignContent: 'center',
            justifyContent: 'center',
            padding: 5
          }}
        >
          <TouchableOpacity
            style={{}}
            onPress={() => {
              if (this.props.isComeFromModel == true) {
                this.props.closeModel(item);
              } else {
                if (this.props.isComeFromEvent) {
                  this.props.closeModel(item);
                } else if (this.props.isComeFromTask) {
                  this.props.closeModel(item);
                } else if (this.props.isComeFromAddContact) {
                  this.props.closeModel(item);
                } else {
                  this.props.getCompanyDetails(item.id);
                }
              }
            }}
          >
            <View style={styles.name}>
              <Text style={[styless.fontPrimary_monsMedium, {}]}>
                {item.name}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  rendercompanyList() {
    const { companyFormState, getCompanyList } = this.props;
    // if (companyFormState.isLoadingCompanyList) {
    //   return (
    //     <View
    //       style={{
    //         justifyContent: 'center',
    //         alignItems: 'center',
    //         flex: 1,
    //         alignSelf: 'center'
    //       }}
    //     >
    //       <ActivityIndicator size="large" color="#00b2e2" animating={true} />
    //     </View>
    //   );
    // } else {
    if (
      companyFormState.companyInputText == '' ||
      !companyFormState.companyInputText.trim().length
    ) {
      return (
        <Fragment>
          <FlatList
            data={companyFormState.companyListArray}
            extraData={this.state.refresh}
            renderItem={({ item, index }) => this.companyList(item, index)}
            keyExtractor={(item, index) => index.toString()}
            onEndReached={({ distanceFromEnd }) => {
              this.nextPageData();
            }}
          />
          {this.props.companyFormState.isLoadingCompanyList == true ? (
            <View
              style={{
                height: 50,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <ActivityIndicator
                size="small"
                color="#00b2e2"
                animating={true}
              />
            </View>
          ) : (
            <Fragment />
          )}
        </Fragment>
      );
    } else {
      // let updatecompanyList = companyFormState.companyListArray.filter(item => {
      //   return item['name']
      //     .toLowerCase()
      //     .includes(companyFormState.companyInputText.trim().toLowerCase());
      // });
      return (
        <FlatList
          data={this.state.searchCompanyResult}
          renderItem={({ item, index }) => this.companyList(item, index)}
          keyExtractor={(item, index) => index.toString()}
        />
      );
    }
    // }
  }

  render() {
    const { companyFormState, updateCompanyState, getCompanyList } = this.props;
    return (
      <View style={{ flex: 9 }}>
        <NavigationEvents
          onWillFocus={() => {
            companyListPageNo = 1;
            updateCompanyState('companyListArray', []);
            setTimeout(() => {
              getCompanyList(
                companyListPageNo,
                this.props.companyFormState.companyListArray
              );
            }, 100);
          }}
        />
        <View
          style={{
            height: 55,
            marginLeft: 25,
            marginRight: 25,
            paddingTop: 10,
            paddingBottom: 5
          }}
        >
          <InputBox
            placeholder="Search by Company Name"
            rightImageName={Icons.search}
            value={companyFormState.companyInputText}
            name="companyInputText"
            updateLocalState={this.props.updateCompanyState}
            onTextChangeMethod={this.searchCompany}
          />
        </View>
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <View
            onStartShouldSetResponder={() => {
              // this.props.navigation.navigate('CompanyContactDetails');
            }}
            style={{ flex: 9, paddingTop: 10 }}
          >
            {this.rendercompanyList()}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return state;
};
const mapDispatchToProps = {
  getCompanyList,
  updateCompanyState,
  getCompanyDetails
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CompanyForm);

const styles = StyleSheet.create({
  name: {
    justifyContent: 'center',
    alignItems: 'flex-start'
    // height: 50
  },
  text: {
    // fontWeight: 'bold'
  },
  textContainer: {
    color: '#000',
    fontWeight: 'bold',
    fontSize: 20
  }
});
