import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity
} from 'react-native';
import Icons from './common/Icons';
import styless from './common/styles';
import Header from './common/Header';
import BackButtonHandler from '../utilities/BackButtonHandler';
import { connect } from 'react-redux';
import {
  getDealDetailsData,
  updateDealDetialsData
} from '../Actions/DealDetailsActions';

class ContactDealForm extends Component {
  componentDidMount() {
    const { ToastProvider } = this.props.screenProps;
    BackButtonHandler.mount(
      true,
      this.props.navigation,
      ToastProvider.showToast
    );
  }

  componentWillUnmount() {
    BackButtonHandler.unmount();
  }

  openExpiredListItem(item, i) {
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.getDealDetailsData(
            item.id,
            this.props.screenProps.ToastProvider.showToast,
            false
          );
        }}
      >
        <View style={{ marginTop: 15, marginBottom: 10 }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Name
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {item.name}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Contact no.
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {item.owner}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Process
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                1234567890
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Close date
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {item.expected_closed_date}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Amount
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {item.amount}
              </Text>
            </View>
          </View>

          <View
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: '#00b2e2'
            }}
          >
            <View style={{ flex: 4, paddingLeft: 10, flexDirection: 'row' }}>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'flex-start',
                  paddingRight: 10
                }}
              >
                {Icons.clock_status_open()}
              </View>
              <Text
                style={[
                  styless.fontPrimary_Regular,
                  styless.textSize13,
                  { color: '#fff' }
                ]}
                numberOfLines={1}
              >
                Status
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center', paddingLeft: 10 }}>
              <Text style={styles.textWhite}>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#fff'
                  }
                ]}
                numberOfLines={1}
              >
                {item.status_desc}
              </Text>
            </View>
          </View>

          <View style={{ height: 1, width: '100%', backgroundColor: '#000' }} />
        </View>
      </TouchableOpacity>
    );
  }
  goToHome = () => {
    this.props.navigation.navigate('Profile');
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header
          titleName="Related Deals"
          leftTitleImg={Icons.profile_back}
          RightTitleImg={Icons.notification_light()}
          onPressBack={this.goToHome}
        />
        <FlatList
          data={this.props.navigation.state.params.dealsOpenExpiredList}
          renderItem={({ item, index }) =>
            this.openExpiredListItem(item, index)
          }
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = {
  getDealDetailsData,
  updateDealDetialsData
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContactDealForm);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    color: '#000',
    fontSize: 15,
    fontFamily: 'Raleway-SemiBold'
  },
  textWhite: {
    color: '#fff',
    fontSize: 15,
    fontFamily: 'Raleway-SemiBold'
  }
});
