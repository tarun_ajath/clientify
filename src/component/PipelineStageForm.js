import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  Dimensions,
  FlatList,
  StyleSheet,
  ActivityIndicator
} from 'react-native';
import InputBox from '../component/common/InputBox';
import { ScrollView } from 'react-native-gesture-handler';
import Container from '../component/common/Container';
import styless from '../component/common/styles';
import HttpClient from '../utilities/HttpClient';
import Icons from '../component/common/Icons';
import { connect } from 'react-redux';
import {
  getPipelineStageList,
  updatePipelineStageState
} from '../Actions/pipelineStageAction';

const { height } = Dimensions.get('window');

class PipelineStageForm extends Component {
  constructor() {
    super();
    this.state = {};
  }

  componentDidMount() {
    this.props.getPipelineStageList();
  }

  checkForImageOrFirstLetterName(first_Name, i) {
    if (first_Name == '' || first_Name == null) {
    } else if (first_Name != null) {
      const first = this.getFirstLetterOfCompany(first_Name);
      return (
        <View>
          <Text
            style={[
              styles.fontPrimary_monsMedium,
              { color: '#fff', fontSize: 17 }
            ]}
          >
            {first.substr(0, 1)}
          </Text>
        </View>
      );
    } else {
      return;
    }
  }

  splittedName = '';
  getFirstLetterOfCompany(hey) {
    this.splittedName = hey
      .split(/\W/)
      .map(item => {
        for (var i = 0; i < item.length; i++) {
          return item[i];
        }
      })
      .join('');
    firstName = this.splittedName;
    return firstName;
  }

  pipelineList(item, i) {
    return (
      <View
        onStartShouldSetResponder={() => {
          if (this.props.isComeFromModel == true) {
            this.props.closePipelineStageModel(item);
          } else {
            alert('in Pipeline');
          }
        }}
        style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 10 }}
      >
        <View style={{ flex: 2, padding: 5 }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#00bfff',
              height: 50,
              width: 50,
              borderRadius: 25
            }}
          >
            {this.checkForImageOrFirstLetterName(item.name, i)}
          </View>
        </View>

        <View
          style={{
            flex: 10,
            alignContent: 'center',
            justifyContent: 'center',
            padding: 5
          }}
        >
          <View style={styles.name}>
            <Text style={[styless.fontPrimary_monsMedium, {}]}>
              {item.name}
            </Text>
          </View>
        </View>
      </View>
    );
  }
  renderPipelineList() {
    const { PipelineStageFormState } = this.props;
    if (PipelineStageFormState.isLoadingPipelineStageList) {
      return (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
            alignSelf: 'center'
          }}
        >
          <ActivityIndicator size="large" color="#00b2e2" animating={true} />
        </View>
      );
    } else {
      if (
        PipelineStageFormState.pipelineStageInputText == '' ||
        !PipelineStageFormState.pipelineStageInputText.trim().length
      ) {
        return (
          <FlatList
            data={PipelineStageFormState.pipelineStageListArray}
            renderItem={({ item, index }) => this.pipelineList(item, index)}
            keyExtractor={(item, index) => index.toString()}
          />
        );
      } else {
        let updatePipelineStageList = PipelineStageFormState.pipelineStageListArray.filter(
          item => {
            return item['name']
              .toLowerCase()
              .includes(
                PipelineStageFormState.pipelineStageInputText
                  .trim()
                  .toLowerCase()
              );
          }
        );
        return (
          <FlatList
            data={updatePipelineStageList}
            renderItem={({ item, index }) => this.pipelineList(item, index)}
            keyExtractor={(item, index) => index.toString()}
          />
        );
      }
    }
  }

  render() {
    const { PipelineStageFormState } = this.props;
    // console.log(
    //   'pipelineStageListArray   ' +
    //     JSON.stringify(PipelineStageFormState.pipelineStageListArray)
    // );

    // console.log(
    //   'pipelineStageList   ' +
    //     JSON.stringify(PipelineStageFormState.isLoadingPipelineStageList)
    // );
    return (
      <View style={{ flex: 9 }}>
        <View
          style={{
            height: 55,
            marginLeft: 25,
            marginRight: 25,
            paddingTop: 10,
            paddingBottom: 5
          }}
        >
          <InputBox
            placeholder="Search by PipelineStage Name"
            rightImageName={Icons.search}
            value={PipelineStageFormState.pipelineStageInputText}
            name="pipelineStageInputText"
            updateLocalState={this.props.updatePipelineStageState}
          />
        </View>
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <View
            onStartShouldSetResponder={() => {}}
            style={{ flex: 9, paddingTop: 10 }}
          >
            {this.renderPipelineList()}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return state;
};
const mapDispatchToProps = dispatch => {
  return {
    getPipelineStageList: (name, value) =>
      dispatch(getPipelineStageList(name, value)),
    updatePipelineStageState: (name, value) =>
      dispatch(updatePipelineStageState(name, value))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PipelineStageForm);

const styles = StyleSheet.create({
  name: {
    justifyContent: 'center',
    alignItems: 'flex-start'
    // height: 50
  },
  text: {
    // fontWeight: 'bold'
  },
  textContainer: {
    color: '#000',
    fontWeight: 'bold',
    fontSize: 20
  }
});
