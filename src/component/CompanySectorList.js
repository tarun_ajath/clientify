import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  Dimensions,
  FlatList,
  StyleSheet,
  ActivityIndicator
} from 'react-native';
import InputBox from './common/InputBox';
import { ScrollView } from 'react-native-gesture-handler';
import Container from './common/Container';
import styless from './common/styles';
import HttpClient from '../utilities/HttpClient';
import Icons from './common/Icons';
import { connect } from 'react-redux';
import {
  getCompanySectorList,
  updateCompanySectorState
} from '../Actions/companySectorAction';

const { height } = Dimensions.get('window');

class CompanySectorList extends Component {
  constructor() {
    super();
    this.state = {};
  }

  componentDidMount() {
    this.props.getCompanySectorList();
  }

  checkForImageOrFirstLetterName(first_Name, i) {
    if (first_Name == '' || first_Name == null) {
    } else if (first_Name != null) {
      const first = this.getFirstLetterOfCompany(first_Name);
      return (
        <View>
          <Text
            style={[
              styles.fontPrimary_monsMedium,
              { color: '#fff', fontSize: 17 }
            ]}
          >
            {first.substr(0, 1)}
          </Text>
        </View>
      );
    } else {
      return;
    }
  }

  splittedName = '';
  getFirstLetterOfCompany(hey) {
    this.splittedName = hey
      .split(/\W/)
      .map(item => {
        for (var i = 0; i < item.length; i++) {
          return item[i];
        }
      })
      .join('');
    firstName = this.splittedName;
    return firstName;
  }

  companySectorList(item, i) {
    return (
      <View
        onStartShouldSetResponder={() => {
          if (this.props.isComeFromModel == true) {
            this.props.closeModel(item);
          } else {
            // alert('in Pipeline');
          }
        }}
        style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 10 }}
      >
        <View style={{ flex: 2, padding: 5 }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#00bfff',
              height: 50,
              width: 50,
              borderRadius: 25
            }}
          >
            {this.checkForImageOrFirstLetterName(item.name, i)}
          </View>
        </View>

        <View
          style={{
            flex: 10,
            alignContent: 'center',
            justifyContent: 'center',
            padding: 5
          }}
        >
          <View style={styles.name}>
            <Text style={[styless.fontPrimary_monsMedium, {}]}>
              {item.name}
            </Text>
          </View>
        </View>
      </View>
    );
  }
  renderCompanySectorList() {
    const { companySectorState } = this.props;
    if (companySectorState.isLoadingCompanySectorList) {
      return (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
            alignSelf: 'center'
          }}
        >
          <ActivityIndicator size="large" color="#00b2e2" animating={true} />
        </View>
      );
    } else {
      if (
        companySectorState.companySectorInputText == '' ||
        !companySectorState.companySectorInputText.trim().length
      ) {
        return (
          <FlatList
            data={companySectorState.companySectorListArray}
            renderItem={({ item, index }) =>
              this.companySectorList(item, index)
            }
            keyExtractor={(item, index) => index.toString()}
          />
        );
      } else {
        let updateCompanySectorList = companySectorState.companySectorListArray.filter(
          item => {
            return item['name']
              .toLowerCase()
              .includes(
                companySectorState.companySectorInputText.trim().toLowerCase()
              );
          }
        );
        return (
          <FlatList
            data={updateCompanySectorList}
            renderItem={({ item, index }) =>
              this.companySectorList(item, index)
            }
            keyExtractor={(item, index) => index.toString()}
          />
        );
      }
    }
  }

  render() {
    const { companySectorState } = this.props;
    // console.log(
    //   'companySectorListArray   ' +
    //     JSON.stringify(companySectorState.companySectorListArray)
    // );

    // console.log(
    //   'companySectorList   ' +
    //     JSON.stringify(companySectorState.isLoadingCompanySectorList)
    // );
    return (
      <View style={{ flex: 9 }}>
        <View
          style={{
            height: 55,
            marginLeft: 25,
            marginRight: 25,
            paddingTop: 10,
            paddingBottom: 5
          }}
        >
          <InputBox
            placeholder="Search by Pipeline Name"
            rightImageName={Icons.search}
            value={companySectorState.companySectorInputText}
            name="companySectorInputText"
            updateLocalState={this.props.updateCompanySectorState}
          />
        </View>
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <View
            onStartShouldSetResponder={() => {}}
            style={{ flex: 9, paddingTop: 10 }}
          >
            {this.renderCompanySectorList()}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return state;
};
const mapDispatchToProps = dispatch => {
  return {
    getCompanySectorList: (name, value) =>
      dispatch(getCompanySectorList(name, value)),
    updateCompanySectorState: (name, value) =>
      dispatch(updateCompanySectorState(name, value))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CompanySectorList);

const styles = StyleSheet.create({
  name: {
    justifyContent: 'center',
    alignItems: 'flex-start'
    // height: 50
  },
  text: {
    // fontWeight: 'bold'
  },
  textContainer: {
    color: '#000',
    fontWeight: 'bold',
    fontSize: 20
  }
});
