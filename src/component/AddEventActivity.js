import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  CheckBox,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Picker,
  Modal,
  FlatList,
  Image,
  ActivityIndicator
} from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import InputBox from './common/InputBox';
import Icons from './common/Icons';
import Header from './common/Header';
import Container from './common/Container';
import ButtonBox from './common/ButtonBox';
import Spacer from './common/Spacer';
import { connect } from 'react-redux';
import {
  getContactList,
  updateContactState
} from '../Actions/contactFormAction';
import { addEventSubmit, updateAddEventState } from '../Actions/AddEventAction';
import CompanyForm from './CompanyForm';
import BackButtonHandler from '../utilities/BackButtonHandler';
import DealList from './common/DealList';

months = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sept',
  'Oct',
  'Nov',
  'Dec'
];

class AddEventActivity extends Component {
  constructor() {
    super();
    this.state = {
      startDate: '01-Jan-2018',
      endDate: '01-Jan-2018',
      startTime: '02:00 am',
      endTime: '02:00 am',
      startDateActual: '01-01-2001',
      startTimeActual: '01:09',
      endDateActual: '01-01-2001',
      endTimeActual: '01:09',
      checked: false,
      startDatePickerVisible: false,
      startTimePickerVisible: false,
      endDatePickerVisible: false,
      endTimePickerVisible: false,

      modalVisibleContact: false,
      modalVisibleDeals: false,
      modalVisibleCompany: false,

      dealListItem: [],
      companyListItem: []
    };
  }
  componentDidMount() {
    const { ToastProvider } = this.props.screenProps;
    BackButtonHandler.mount(
      true,
      this.props.navigation,
      ToastProvider.showToast
    );
  }

  componentWillUnmount() {
    BackButtonHandler.unmount();
  }

  _showStartDatePicker = () => this.setState({ startDatePickerVisible: true });
  _showStartTimePicker = () => this.setState({ startTimePickerVisible: true });

  _showEndDatePicker = () => this.setState({ endDatePickerVisible: true });
  _showEndTimePicker = () => this.setState({ endTimePickerVisible: true });

  _hideStartDatePicker = () => this.setState({ startDatePickerVisible: false });
  _hideStartTimePicker = () => this.setState({ startTimePickerVisible: false });

  _hideEndDatePicker = () => this.setState({ endDatePickerVisible: false });
  _hideEndTimePicker = () => this.setState({ endTimePickerVisible: false });

  _handleStartDatePicked = date => {
    const startDate = date.getDate().toString();
    const startMonth = months[date.getMonth()];
    const startYear = date.getFullYear().toString();

    this.setState({
      startDateActual: date,
      startDate: startDate + '-' + startMonth + '-' + startYear
    });
    this._hideStartDatePicker();
  };
  _handleStartTimePicked = time => {
    const startHour =
      time.getHours().toString() > 12
        ? '0' + (time.getHours().toString() % 12) || 12
        : '0' + (time.getHours().toString() % 12) || 12;
    const startMinute =
      time.getMinutes().toString() < 10
        ? '0' + time.getMinutes().toString()
        : time.getMinutes().toString();
    const formatTime = time.getHours().toString() < 12 ? 'am' : 'pm';

    this.setState({
      startTimeActual: time,
      startTime: startHour + ':' + startMinute + ' ' + formatTime
    });

    this._hideStartTimePicker();
  };

  _handleEndDatePicked = date => {
    const endDate = date.getDate().toString();
    const endMonth = months[date.getMonth()];
    const endYear = date.getFullYear().toString();
    this.setState({
      endDateActual: date,
      endDate: endDate + '-' + endMonth + '-' + endYear
    });
    this._hideEndDatePicker();
  };
  _handleEndTimePicked = time => {
    const endHours =
      time.getHours().toString() < 12
        ? '0' + (time.getHours().toString() % 12) || 12
        : '0' + (time.getHours().toString() % 12) || 12;
    const endMinutes =
      time.getMinutes().toString() < 10
        ? '0' + time.getMinutes().toString()
        : time.getMinutes().toString();
    const formatTime = time.getHours().toString() < 12 ? 'am' : 'pm';

    this.setState({
      endTimeActual: time,
      endTime: endHours + ':' + endMinutes + ' ' + formatTime
    });
    this._hideEndTimePicker();
  };

  goToHome = () => {
    this.props.navigation.navigate('ActivitiesForm');
  };

  isEmailNull(emailArray) {
    if (emailArray == null) {
      return;
    } else {
      return (
        <Text
          style={[
            styles.fontPrimary_Bold,
            styles.textSize14,
            { color: '#7f7f7f' }
          ]}
        >
          {emailArray.email}
        </Text>
      );
    }
  }

  splittedName = '';
  getFirstLetterOfContact(Name) {
    this.splittedName = Name.split(/\W/)
      .map(item => {
        for (var i = 0; i < item.length; i++) {
          return item[i];
        }
      })
      .join('');
    firstLetterOfName = this.splittedName;
    return firstLetterOfName;
  }

  checkForImageOrFirstLetterName(first_Name, last_name, picture_url, i) {
    if (picture_url == null && (first_Name == '' || first_Name == null)) {
    } else if (picture_url == null) {
      const first = this.getFirstLetterOfContact(first_Name);
      const last = this.getFirstLetterOfContact(last_name);
      return (
        <View>
          <Text
            style={[
              styles.fontPrimary_monsMedium,
              { color: '#fff', fontSize: 20 }
            ]}
          >
            {first.substr(0, 1) + '' + last.substr(0, 1)}
          </Text>
        </View>
      );
    } else {
      const { contactFormState, getContactList } = this.props;
      return (
        <View>
          <Image
            style={{ width: 50, height: 50, borderRadius: 25 }}
            resizeMode={'contain'}
            source={{
              uri: picture_url
            }}
            onError={() => {
              let newArr = [...contactFormState.contactListArray];
              newArr[i].picture_url = null;
              updateContactState('contactListArray', newArr);
            }}
          />
        </View>
      );
    }
  }

  contactLists(item, i) {
    const {
      contactFormState,
      getContactList,
      profileScreenState,
      updateProfileState
    } = this.props;
    return (
      <View
        onStartShouldSetResponder={() => {
          this.setState({ modalVisibleContact: false });
          this.props.updateAddEventState('guest_contacts', item);
        }}
        style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 10 }}
      >
        <View style={{ flex: 2, alignContent: 'center', padding: 5 }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#00b2e2',
              height: 50,
              width: 50,
              borderRadius: 25
            }}
          >
            {this.checkForImageOrFirstLetterName(
              item.first_name,
              item.last_name,
              item.picture_url,
              i
            )}
          </View>
        </View>
        <View style={{ flex: 10, alignContent: 'center', padding: 5 }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'flex-start',
              height: 50
            }}
          >
            <Text
              style={[
                styles.fontPrimary_monsMedium,
                styles.textSize16,
                { color: '#000' }
              ]}
              numberOfLines={1}
            >
              {item.first_name + ' ' + item.last_name}
            </Text>
            {this.isEmailNull(item.emails[0])}
          </View>
        </View>
      </View>
    );
  }

  LoadingContactListData() {
    const { contactFormState } = this.props;
    if (
      contactFormState.contactTextInput == '' ||
      !contactFormState.contactTextInput.trim().length
    ) {
      return (
        <FlatList
          data={contactFormState.contactListArray}
          renderItem={({ item, index }) => this.contactLists(item, index)}
          keyExtractor={(item, index) => index.toString()}
        />
      );
    } else {
      let updatedContactList = contactFormState.contactListArray.filter(
        item => {
          return item['first_name']
            .toLowerCase()
            .includes(contactFormState.contactTextInput.trim().toLowerCase());
        }
      );
      return (
        <FlatList
          data={updatedContactList}
          renderItem={({ item, index }) => this.contactLists(item, index)}
          keyExtractor={(item, index) => index.toString()}
        />
      );
    }
  }
  nullAllEventsField() {
    this.props.updateAddEventState('name', '');
    this.props.updateAddEventState('location', '');
    this.props.updateAddEventState('from_datetime', '');
    this.props.updateAddEventState('to_datetime', '');
    this.props.updateAddEventState('all_day', false);
    this.props.updateAddEventState('guest_contacts', []);
    this.props.updateAddEventState('deals', []);
    this.props.updateAddEventState('companies', []);
  }

  submitAddDeal = () => {
    const { ToastProvider } = this.props.screenProps;
    if (
      this.props.addEventState.name == ''
      // this.props.addEventState.location == '' ||
      // this.state.startDate == '01-Jan-2018' ||
      // this.state.endDate == '01-Jan-2018' ||
      // this.state.startTime == '02:00 am' ||
      // this.state.endTime == '02:00 am'
    ) {
      alert('please fill all the details');
    } else {
      var startUTC = Date.UTC(
        this.state.startDateActual.getFullYear(),
        this.state.startDateActual.getMonth(),
        this.state.startDateActual.getDate(),
        this.state.startTimeActual.getHours(),
        this.state.startTimeActual.getMinutes(),
        this.state.startTimeActual.getSeconds(),
        this.state.startTimeActual.getMilliseconds()
      );
      let startDateInFormate = new Date(startUTC).toISOString();
      var endUTC = Date.UTC(
        this.state.endDateActual.getFullYear(),
        this.state.endDateActual.getMonth(),
        this.state.endDateActual.getDate(),
        this.state.endTimeActual.getHours(),
        this.state.endTimeActual.getMinutes(),
        this.state.endTimeActual.getSeconds(),
        this.state.endTimeActual.getMilliseconds()
      );
      let endDateInFormate = new Date(endUTC).toISOString();

      reqBody = {
        name: this.props.addEventState.name,
        location: this.props.addEventState.location,
        from_datetime: startDateInFormate,
        to_datetime: endDateInFormate,
        all_day: this.props.addEventState.all_day,
        guest_contacts: ['' + this.props.addEventState.guest_contacts.url]
      };
      console.log('Add Event req  ', reqBody);
      this.props.addEventSubmit(reqBody, ToastProvider.showToast, 'activity');
      this.nullAllEventsField();
    }
  };

  dealsInfo = item => {
    this.setState({ modalVisibleDeals: false });
    this.setState({
      dealListItem: item
    });
    this.props.updateAddEventState('deals', item);
    console.log('item deal item ', item);
  };

  companyInfo = item => {
    this.setState({ modalVisibleCompany: false });
    this.setState({
      companyListItem: item
    });
    this.props.updateAddEventState('companies', item);
    console.log('item deal item ', item);
  };

  render() {
    console.log('jhj', this.props);
    const { contactFormState, navigation } = this.props;
    const isComeFrom = navigation.getParam('isComeFrom');
    return (
      <Container>
        <View>
          <Header
            titleName="Add Events"
            leftTitleImg={Icons.profile_back}
            RightTitleImg={Icons.notification_light()}
            onPressBack={this.goToHome}
          />
          <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisibleContact}
              onRequestClose={() => {
                this.setState({ modalVisibleContact: false });
              }}
              BackButtonHandler={true}
            >
              <Header
                titleName="Contact Modal"
                leftTitleImg={Icons.notification_light()}
                RightTitleImg={Icons.notification_light()}
              />
              <View style={{ flex: 8 }}>
                <View
                  style={{
                    height: 55,
                    marginLeft: 25,
                    marginRight: 25,
                    paddingTop: 10,
                    paddingBottom: 5
                  }}
                >
                  <InputBox
                    rightImageName={Icons.search}
                    placeholder="Search by Contact First Name"
                    value={contactFormState.contactTextInput}
                    name="contactTextInput"
                    updateLocalState={this.props.updateContactState}
                  />
                </View>
                <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                  <View
                    style={{ flex: 9, paddingTop: 10, flexDirection: 'row' }}
                  >
                    {this.LoadingContactListData()}
                  </View>
                </ScrollView>
              </View>
            </Modal>

            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisibleDeals}
              onRequestClose={() => {
                this.setState({ modalVisibleDeals: false });
              }}
              BackButtonHandler={true}
            >
              <Header
                titleName="Deal Modal"
                leftTitleImg={Icons.notification_light()}
                RightTitleImg={Icons.notification_light()}
              />
              <View style={{ flex: 8 }}>
                <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                  <View
                    style={{ flex: 9, paddingTop: 10, flexDirection: 'row' }}
                  >
                    {/* {this.LoadingContactListData()} */}
                    <DealList dealsInfo={this.dealsInfo} />
                  </View>
                </ScrollView>
              </View>
            </Modal>

            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisibleCompany}
              onRequestClose={() => {
                this.setState({ modalVisibleCompany: false });
              }}
              BackButtonHandler={true}
            >
              <Header
                titleName="Company Modal"
                leftTitleImg={Icons.notification_light()}
                RightTitleImg={Icons.notification_light()}
              />
              <View style={{ flex: 8 }}>
                <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                  <View
                    style={{ flex: 9, paddingTop: 10, flexDirection: 'row' }}
                  >
                    <CompanyForm
                      isComeFromModel={false}
                      isComeFromEvent={true}
                      closeModel={this.companyInfo}
                    />
                  </View>
                </ScrollView>
              </View>
            </Modal>

            <View style={{ flex: 1, paddingTop: 10 }}>
              <View style={{ flexDirection: 'row', paddingTop: 10 }}>
                <View style={{ flex: 1 }} />
                <View style={{ flex: 4, justifyContent: 'center' }}>
                  <Text>Name</Text>
                </View>
                <View style={{ flex: 1 }} />
                <View style={{ flex: 7, height: 40 }}>
                  <InputBox
                    placeholder="Name"
                    value={this.props.addEventState.name}
                    name="name"
                    updateLocalState={this.props.updateAddEventState}
                  />
                </View>
                <View style={{ flex: 1 }} />
              </View>
              <Spacer />
              <View style={{ flexDirection: 'row', paddingTop: 10 }}>
                <View style={{ flex: 1 }} />
                <View style={{ flex: 4, justifyContent: 'center' }}>
                  <Text>Location</Text>
                </View>
                <View style={{ flex: 1 }} />
                <View style={{ flex: 7, height: 40 }}>
                  <InputBox
                    placeholder="Enter Location"
                    rightImageName={Icons.location}
                    value={this.props.addEventState.location}
                    name="location"
                    updateLocalState={this.props.updateAddEventState}
                  />
                </View>
                <View style={{ flex: 1 }} />
              </View>
              <View style={{ flexDirection: 'row', paddingTop: 20 }}>
                <View style={{ flex: 1 }} />
                <View style={{ flex: 5 }}>
                  <Text>Start Date</Text>
                </View>
                <View style={{ flex: 1 }} />
                <View style={{ flex: 5 }}>
                  <Text>Start Time</Text>
                </View>
                <View style={{ flex: 1 }} />
              </View>
              <View style={{ flexDirection: 'row', paddingTop: 10 }}>
                <View style={{ flex: 1 }} />
                <View style={{ flex: 7, height: 40 }}>
                  <TouchableOpacity onPress={this._showStartDatePicker}>
                    <View
                      style={{
                        flexDirection: 'row',
                        borderColor: 'grey',
                        borderWidth: 2,
                        borderRadius: 30,
                        backgroundColor: '#eeeeee'
                      }}
                    >
                      <View
                        style={{
                          flex: 1,
                          justifyContent: 'center',
                          alignItems: 'center',
                          paddingLeft: 10
                        }}
                      />
                      <View style={{ flex: 7, height: 35 }}>
                        <Text
                          style={[
                            styles.fontPrimary_Regular,
                            styles.textSize13,
                            { paddingTop: 8, color: 'grey' }
                          ]}
                        >
                          {this.state.startDate}
                        </Text>
                      </View>
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                          paddingRight: 10
                        }}
                      >
                        {Icons.calender()}
                      </View>
                    </View>
                  </TouchableOpacity>
                  <DateTimePicker
                    mode="date"
                    minimumDate={new Date()}
                    isVisible={this.state.startDatePickerVisible}
                    onConfirm={this._handleStartDatePicked}
                    onCancel={this._hideStartDatePicker}
                    datePickerModeAndroid={'spinner'}
                  />
                </View>
                <View style={{ flex: 1 }} />
                <View style={{ flex: 7, height: 40 }}>
                  <TouchableOpacity onPress={this._showStartTimePicker}>
                    <View
                      style={{
                        flexDirection: 'row',
                        borderColor: 'grey',
                        borderWidth: 2,
                        borderRadius: 30,
                        backgroundColor: '#eeeeee'
                      }}
                    >
                      <View
                        style={{
                          flex: 1,
                          justifyContent: 'center',
                          alignItems: 'center',
                          paddingLeft: 10
                        }}
                      />
                      <View style={{ flex: 7, height: 35 }}>
                        <Text
                          style={[
                            styles.fontPrimary_Regular,
                            styles.textSize13,
                            { paddingTop: 8, color: 'grey' }
                          ]}
                        >
                          {this.state.startTime}
                        </Text>
                      </View>
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                          paddingRight: 10
                        }}
                      >
                        {Icons.clock()}
                      </View>
                    </View>
                  </TouchableOpacity>
                  <DateTimePicker
                    mode="time"
                    isVisible={this.state.startTimePickerVisible}
                    onConfirm={this._handleStartTimePicked}
                    onCancel={this._hideStartTimePicker}
                    datePickerModeAndroid={'spinner'}
                  />
                </View>
                <View style={{ flex: 1 }} />
              </View>
              <View style={{ flexDirection: 'row', paddingTop: 20 }}>
                <View style={{ flex: 1 }} />
                <View style={{ flex: 5 }}>
                  <Text>End Date</Text>
                </View>
                <View style={{ flex: 1 }} />
                <View style={{ flex: 5 }}>
                  <Text>End Time</Text>
                </View>
                <View style={{ flex: 1 }} />
              </View>
              <View style={{ flexDirection: 'row', paddingTop: 10 }}>
                <View style={{ flex: 1 }} />
                <View style={{ flex: 7, height: 40 }}>
                  <TouchableOpacity onPress={this._showEndDatePicker}>
                    <View
                      style={{
                        flexDirection: 'row',
                        borderColor: 'grey',
                        borderWidth: 2,
                        borderRadius: 30,
                        backgroundColor: '#eeeeee'
                      }}
                    >
                      <View
                        style={{
                          flex: 1,
                          justifyContent: 'center',
                          alignItems: 'center',
                          paddingLeft: 10
                        }}
                      />
                      <View style={{ flex: 7, height: 35 }}>
                        <Text
                          style={[
                            styles.fontPrimary_Regular,
                            styles.textSize13,
                            { paddingTop: 8, color: 'grey' }
                          ]}
                        >
                          {this.state.endDate}
                        </Text>
                      </View>
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                          paddingRight: 10
                        }}
                      >
                        {Icons.calender()}
                      </View>
                    </View>
                  </TouchableOpacity>
                  <DateTimePicker
                    mode="date"
                    minimumDate={new Date()}
                    isVisible={this.state.endDatePickerVisible}
                    onConfirm={this._handleEndDatePicked}
                    onCancel={this._hideEndDatePicker}
                    datePickerModeAndroid={'spinner'}
                  />
                </View>
                <View style={{ flex: 1 }} />
                <View style={{ flex: 7, height: 40 }}>
                  <TouchableOpacity onPress={this._showEndTimePicker}>
                    <View
                      style={{
                        flexDirection: 'row',
                        borderColor: 'grey',
                        borderWidth: 2,
                        borderRadius: 30,
                        backgroundColor: '#eeeeee'
                      }}
                    >
                      <View
                        style={{
                          flex: 1,
                          justifyContent: 'center',
                          alignItems: 'center',
                          paddingLeft: 10
                        }}
                      />
                      <View style={{ flex: 7, height: 35 }}>
                        <Text
                          style={[
                            styles.fontPrimary_Regular,
                            styles.textSize13,
                            { paddingTop: 8, color: 'grey' }
                          ]}
                        >
                          {this.state.endTime}
                        </Text>
                      </View>
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                          paddingRight: 10
                        }}
                      >
                        {Icons.clock()}
                      </View>
                    </View>
                  </TouchableOpacity>
                  <DateTimePicker
                    mode="time"
                    isVisible={this.state.endTimePickerVisible}
                    onConfirm={this._handleEndTimePicked}
                    onCancel={this._hideEndTimePicker}
                    datePickerModeAndroid={'spinner'}
                  />
                </View>
                <View style={{ flex: 1 }} />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  paddingTop: 20,
                  paddingLeft: 20
                }}
              >
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center'
                  }}
                >
                  <CheckBox
                    value={this.state.checked}
                    onValueChange={() => {
                      this.setState({ checked: !this.state.checked });
                      this.props.updateAddEventState(
                        'all_day',
                        !this.state.checked
                      );
                    }}
                  />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingLeft: 10
                  }}
                >
                  <Text
                    style={{
                      color: 'grey',
                      fontSize: 14,
                      fontWeight: '300'
                    }}
                  >
                    All day
                  </Text>
                </View>
              </View>
              <View style={{ flexDirection: 'row', paddingTop: 10 }}>
                <View style={{ flex: 1 }} />
                <View style={{ flex: 4, justifyContent: 'center' }}>
                  <Text numberOfLines={1}>Guest Contact</Text>
                </View>
                <View style={{ flex: 1 }} />
                <View style={{ flex: 7, height: 40 }}>
                  <View
                    onStartShouldSetResponder={() => {
                      this.setState({
                        modalVisibleContact: true
                      });
                    }}
                    style={{
                      flex: 2,
                      height: 40,
                      padding: 0,
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginRight: 15,
                      borderColor: 'grey',
                      borderWidth: 2,
                      borderRadius: 30
                    }}
                  >
                    <Text
                      style={[
                        styles.fontPrimary_Bold,
                        styles.textSize14,
                        {
                          color: '#7f7f7f',
                          alignItems: 'center'
                        }
                      ]}
                    >
                      {this.props.addEventState.guest_contacts == ''
                        ? 'select here'
                        : this.props.addEventState.guest_contacts.first_name}
                    </Text>
                  </View>
                </View>
                <View style={{ flex: 1 }} />
              </View>

              <View style={{ flexDirection: 'row', paddingTop: 10 }}>
                <View style={{ flex: 1 }} />
                <View style={{ flex: 4, justifyContent: 'center' }}>
                  <Text numberOfLines={1}>Company</Text>
                </View>
                <View style={{ flex: 1 }} />
                <View style={{ flex: 7, height: 40 }}>
                  <View
                    onStartShouldSetResponder={() => {
                      this.setState({
                        modalVisibleCompany: true
                      });
                    }}
                    style={{
                      flex: 2,
                      height: 40,
                      padding: 0,
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginRight: 15,
                      borderColor: 'grey',
                      borderWidth: 2,
                      borderRadius: 30
                    }}
                  >
                    <Text
                      style={[
                        styles.fontPrimary_Bold,
                        styles.textSize14,
                        {
                          color: '#7f7f7f',
                          alignItems: 'center'
                        }
                      ]}
                    >
                      {this.props.addEventState.companies == ''
                        ? 'select here'
                        : this.props.addEventState.companies.name}
                    </Text>
                  </View>
                </View>
                <View style={{ flex: 1 }} />
              </View>

              <View style={{ flexDirection: 'row', paddingTop: 10 }}>
                <View style={{ flex: 1 }} />
                <View style={{ flex: 4, justifyContent: 'center' }}>
                  <Text numberOfLines={1}>Deals</Text>
                </View>
                <View style={{ flex: 1 }} />
                <View style={{ flex: 7, height: 40 }}>
                  <View
                    onStartShouldSetResponder={() => {
                      this.setState({
                        modalVisibleDeals: true
                      });
                    }}
                    style={{
                      flex: 2,
                      height: 40,
                      padding: 0,
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginRight: 15,
                      borderColor: 'grey',
                      borderWidth: 2,
                      borderRadius: 30
                    }}
                  >
                    <Text
                      style={[
                        styles.fontPrimary_Bold,
                        styles.textSize14,
                        {
                          color: '#7f7f7f',
                          alignItems: 'center'
                        }
                      ]}
                    >
                      {this.props.addEventState.deals == ''
                        ? 'select here'
                        : this.props.addEventState.deals.name}
                    </Text>
                  </View>
                </View>
                <View style={{ flex: 1 }} />
              </View>
              <Spacer size={10} />

              <Spacer />
              <View
                style={{
                  marginRight: 15,
                  marginLeft: 15,
                  flexDirection: 'row'
                }}
              >
                <View
                  style={{
                    flex: 1,
                    marginRight: 5,
                    borderRadius: 30,
                    borderWidth: 1,
                    borderColor: 'grey'
                  }}
                >
                  <ButtonBox
                    btnText="Cancel"
                    btnColor="white"
                    btnTextColor="#000"
                    onPress={this.goToHome}
                  />
                </View>
                <View style={{ flex: 1, marginLeft: 5 }}>
                  <ButtonBox
                    btnText="Save "
                    btnColor="#f5921e"
                    onPress={this.submitAddDeal}
                  />
                </View>
              </View>
              <Spacer size={30} />
            </View>
          </ScrollView>
        </View>
      </Container>
    );
  }
}
const mapStateToProps = state => {
  return state;
};
const mapDispatchToProps = {
  addEventSubmit,
  updateAddEventState,
  updateContactState
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddEventActivity);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});
