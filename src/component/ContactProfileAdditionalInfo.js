import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  FlatList,
  TouchableOpacity
} from 'react-native';
import Spacer from './common/Spacer';
import Icons from './common/Icons';
import styless from './common/styles';
import { Colors } from 'react-native-paper';
import { connect } from 'react-redux';
import {
  getDealDetailsData,
  updateDealDetialsData
} from '../Actions/DealDetailsActions';

class ContactProfileAdditionalInfo extends Component {
  constructor() {
    super();
    this.state = {};
  }

  DealsList = (item, index) => {
    return (
      <View style={{ margin: 8 }}>
        <TouchableOpacity
          onPress={() => {
            this.props.getDealDetailsData(
              item.id,
              this.props.ToastProvider.showToast,
              false
            );
          }}
        >
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ flex: 2.5 }}>
              <Text style={{ fontWeight: 'bold', fontSize: 13 }}>
                {item.name}
              </Text>
            </View>
            <View style={{ flex: 1 }}>
              <Text style={{ fontSize: 11 }}>
                Due Date : {item.expected_closed_date}
              </Text>
            </View>
          </View>

          <View style={{ flex: 1 }}>
            <Text style={{ fontSize: 11 }}>{item.status_desc}</Text>
          </View>

          <View style={{ flex: 1 }}>
            <Text style={{ fontSize: 11, color: '#00b2e2' }}>
              {item.amount}
            </Text>
          </View>
          <View
            style={{
              height: 1,
              width: '100%',
              backgroundColor: 'grey',
              marginTop: 7
            }}
          />
        </TouchableOpacity>
      </View>
    );
  };

  TaskList = (item, index) => {
    return (
      <View style={{ margin: 8 }}>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={{ flex: 2.5 }}>
            <Text style={{ fontWeight: 'bold', fontSize: 13 }}>
              {item.name}
            </Text>
          </View>
          <View style={{ flex: 1 }}>
            <Text style={{ fontSize: 11 }}>
              Due Date :{' '}
              {new Date(item.due_date).getFullYear() +
                '-' +
                (new Date(item.due_date).getMonth() + 1) +
                '-' +
                new Date(item.due_date).getDate()}
            </Text>
          </View>
        </View>

        <View style={{ flex: 1 }}>
          <Text style={{ fontSize: 11 }}>{item.owner}</Text>
        </View>

        <View
          style={{
            height: 1,
            width: '100%',
            backgroundColor: 'grey',
            marginTop: 7
          }}
        />
      </View>
    );
  };

  EventsList = (item, index) => {
    return (
      <View style={{ margin: 8 }}>
        <TouchableOpacity onPress={() => {}}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ flex: 2.5 }}>
              <Text style={{ fontWeight: 'bold', fontSize: 13 }}>
                {item.name}
              </Text>
            </View>
            <View style={{ flex: 1 }}>
              <Text style={{ fontSize: 11 }}>
                {new Date(item.from_datetime).getFullYear() +
                  '-' +
                  (new Date(item.from_datetime).getMonth() + 1) +
                  '-' +
                  new Date(item.from_datetime).getDate()}
              </Text>
              <Text style={{ fontSize: 11, color: '#00b2e2' }}>
                to :
                {new Date(item.to_datetime).getFullYear() +
                  '-' +
                  (new Date(item.to_datetime).getMonth() + 1) +
                  '-' +
                  new Date(item.to_datetime).getDate()}
              </Text>
            </View>
          </View>

          <View style={{ flex: 1 }}>
            <Text style={{ fontSize: 11 }}>Location : {item.owner}</Text>
          </View>

          <View style={{ flex: 1 }}>
            <Text style={{ fontSize: 11 }}>Location : {item.location}</Text>
          </View>
          <View
            style={{
              height: 1,
              width: '100%',
              backgroundColor: 'grey',
              marginTop: 7
            }}
          />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    let listData = this.props.newListArray;
    console.log('profile Contact Info pageData - ', listData);
    return (
      <View style={{ padding: 15 }}>
        <View
          style={{
            height: 40,
            paddingLeft: 15,
            backgroundColor: '#00b2e2',
            justifyContent: 'center',
            alignItems: 'flex-start'
          }}
        >
          <Text style={[styless.fontPrimary_Bold, styless.textSize17, {}]}>
            Deals
          </Text>
        </View>
        <View style={{ flex: 1 }}>
          {listData.deals == [] ||
          listData.deals == null ||
          listData.deals == '' ? (
            <Text
              style={[
                styless.fontPrimary_Regular,
                styless.textSize12,
                { paddingLeft: 15 }
              ]}
            >
              No Deals
            </Text>
          ) : (
            <FlatList
              style={{ flex: 1 }}
              data={listData.deals}
              renderItem={({ item, index }) => this.DealsList(item, index)}
              keyExtractor={(item, index) => index.toString()}
            />
          )}
        </View>

        <View
          style={{
            height: 40,
            paddingLeft: 15,
            backgroundColor: '#00b2e2',
            justifyContent: 'center',
            alignItems: 'flex-start'
          }}
        >
          <Text
            style={[
              styless.fontPrimary_Bold,
              styless.textSize17,
              { marginTop: 5, marginBottom: 5 }
            ]}
          >
            related_tasks
          </Text>
        </View>
        <View style={{ flex: 1 }}>
          {listData.related_tasks == [] ||
          listData.related_tasks == null ||
          listData.related_tasks == '' ? (
            <Text
              style={[
                styless.fontPrimary_Regular,
                styless.textSize12,
                { paddingLeft: 15 }
              ]}
            >
              No related tasks
            </Text>
          ) : (
            <FlatList
              style={{ flex: 1 }}
              data={listData.related_tasks}
              renderItem={({ item, index }) => this.TaskList(item, index)}
              keyExtractor={(item, index) => index.toString()}
            />
          )}
        </View>

        <View
          style={{
            height: 40,
            paddingLeft: 15,
            backgroundColor: '#00b2e2',
            justifyContent: 'center',
            alignItems: 'flex-start'
          }}
        >
          <Text
            style={[
              styless.fontPrimary_Bold,
              styless.textSize17,
              { marginTop: 5, marginBottom: 5 }
            ]}
          >
            tags
          </Text>
        </View>
        <View style={{ paddingLeft: 15 }}>
          <Text style={[styless.fontPrimary_Regular, styless.textSize12, {}]}>
            {listData.tags == [] || listData.tags == null || listData.tags == ''
              ? 'No Tags'
              : listData.tags.toString()}
          </Text>
        </View>

        <View
          style={{
            height: 40,
            paddingLeft: 15,
            backgroundColor: '#00b2e2',
            justifyContent: 'center',
            alignItems: 'flex-start'
          }}
        >
          <Text
            style={[
              styless.fontPrimary_Bold,
              styless.textSize17,
              { marginTop: 5, marginBottom: 5 }
            ]}
          >
            Events
          </Text>
        </View>
        <View style={{ flex: 1, paddingLeft: 15 }}>
          <Text style={[styless.fontPrimary_Regular, styless.textSize12, {}]}>
            {listData.events == [] ||
            listData.events == null ||
            listData.events == '' ? (
              'No Events'
            ) : (
              <FlatList
                style={{ flex: 1 }}
                data={listData.events}
                renderItem={({ item, index }) => this.EventsList(item, index)}
                keyExtractor={(item, index) => index.toString()}
              />
            )}
          </Text>
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = {
  getDealDetailsData,
  updateDealDetialsData
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContactProfileAdditionalInfo);
