import React, { Component } from 'react';
import {
  Dimensions,
  Keyboard,
  TouchableOpacity,
  Text,
  View,
  Platform
} from 'react-native';

import { TextInput, Button } from 'react-native-paper';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { ScrollView } from 'react-native-gesture-handler';
import InputBox from './common/InputBox';
import Spacer from './common/Spacer';
import ButtonBox from './common/ButtonBox';
import Line from './common/Line';
import Icons from './common/Icons';
import styles from './common/styles';

const LoginForm = props => {
  return (
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 30
      }}
    >
      <InputBox
        value={props.loginState.email}
        placeholder="test@test.com"
        imageName="user"
        name="email"
        updateLocalState={props.updateLocalState}
      />
      <Spacer />
      <InputBox
        value={props.loginState.password}
        name="password"
        placeholder="Password"
        imageName="lock"
        updateLocalState={props.updateLocalState}
      />
      <Spacer />
      <Text style={[styles.fontPrimary_semiBold, { color: '#000' }]}>
        Forgot a Password?
      </Text>
      <Spacer />
      <ButtonBox
        btnText="LOG IN "
        btnColor="#f5921e"
        onPress={props.validateData}
        btnLoaderBoolean={props.loginState.isLoadingLoginRequest}
      />
      <Spacer />
      {/* <View style={{ flexDirection: 'row' }}>
        <View
          style={{
            flex: 6,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <Line />
        </View>
        <View
          style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}
        >
          <Text style={{ color: '#000', fontSize: 14 }}> or </Text>
        </View>
        <View
          style={{
            flex: 6,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <Line />
        </View>
      </View> */}
      <Spacer size={15} />
      {/* <ButtonBox
        btnImage={Icons.google}
        btnText="Sign in with Google "
        btnColor="#dc4e41"
        onPress={props.googleLogin}
        btnTextColor="#fff"
      /> */}
    </View>
  );
};

export default LoginForm;
