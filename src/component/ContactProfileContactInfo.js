import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Linking,
  Platform
} from 'react-native';
import Spacer from './common/Spacer';
import Icons from './common/Icons';
import styless from './common/styles';

class ContactProfileContactInfo extends Component {
  firstArrayElement(ArrayElement, arrayType) {
    if (ArrayElement == null || ArrayElement == '') {
      if (arrayType == 'emails') {
        return (
          <Text style={[styless.fontPrimary_Regular, styless.textSize15, {}]}>
            No email
          </Text>
        );
      } else if (arrayType == 'phones') {
        return (
          <Text style={[styless.fontPrimary_Regular, styless.textSize15, {}]}>
            No phone no
          </Text>
        );
      }
    } else {
      // console.log('val of elm is  ' + ArrayElement);
      if (arrayType == 'emails') {
        return (
          <Text style={[styless.fontPrimary_Regular, styless.textSize15, {}]}>
            {ArrayElement[0].email}
          </Text>
        );
      } else if (arrayType == 'phones') {
        return (
          <Text style={[styless.fontPrimary_Regular, styless.textSize15, {}]}>
            {ArrayElement[0].phone}
          </Text>
        );
      } else if (arrayType == 'addresses') {
        let textField =
          ArrayElement[0].street +
          ', ' +
          ArrayElement[0].city +
          ', ' +
          ArrayElement[0].state +
          ', ' +
          ArrayElement[0].country +
          ', ' +
          ArrayElement[0].postal_code;
        return (
          <Text style={[styless.fontPrimary_Regular, styless.textSize12, {}]}>
            {textField}
          </Text>
        );
      }
    }
  }

  custom_fieldsList = (item, index) => {
    return (
      <View style={{ margin: 8 }}>
        <TouchableOpacity
          onPress={() => {
            this.props.getDealDetailsData(
              item.id,
              this.props.ToastProvider.showToast,
              false
            );
          }}
        >
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ flex: 2.5 }}>
              <Text style={{ fontWeight: 'bold', fontSize: 15 }}>
                {item.name}
              </Text>
            </View>
            <View style={{ flex: 1 }}>
              {/* <Text style={{ fontSize: 12 }}>
                Due Date : {item.expected_closed_date}
              </Text> */}
            </View>
          </View>

          <View style={{ flex: 1 }}>
            {/* <Text style={{ fontSize: 12 }}>{item.status_desc}</Text> */}
          </View>

          <View style={{ flex: 1 }}>
            <Text style={{ fontSize: 12, color: '#00b2e2' }}>
              {/* {item.amount} */}
            </Text>
          </View>
          <View
            style={{
              height: 1,
              width: '100%',
              backgroundColor: 'grey',
              marginTop: 7
            }}
          />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    let listData = this.props.newListArray;
    return (
      <View style={{ padding: 20 }}>
        <View style={{ flex: 1, paddingTop: 8 }}>
          <View style={{ flex: 4 }}>
            <Text style={[styless.fontPrimary_Bold, styless.textSize13, {}]}>
              Company Name
            </Text>
          </View>
          <View style={{ flex: 4, paddingLeft: 5, paddingTop: 3 }}>
            <Text style={[styless.fontPrimary_Regular, styless.textSize15, {}]}>
              {listData.company == [] ||
              listData.company == null ||
              listData.company == ''
                ? 'No company'
                : listData.company}
            </Text>
          </View>
        </View>

        <View style={{ flex: 1, paddingTop: 10 }}>
          <View style={{ flex: 4 }}>
            <Text style={[styless.fontPrimary_Bold, styless.textSize13, {}]}>
              Phone Number
            </Text>
          </View>
          <View style={{ flex: 4, paddingLeft: 5, paddingTop: 3 }}>
            <TouchableOpacity
              onPress={() => {
                let phoneNumber = listData.phones[0].phone;
                if (Platform.OS !== 'android') {
                  phoneNumber = `telprompt:${phoneNumber}`;
                } else {
                  phoneNumber = `tel:${phoneNumber}`;
                }
                Linking.canOpenURL(phoneNumber)
                  .then(supported => {
                    if (!supported) {
                      Alert.alert('Phone number is not available');
                    } else {
                      return Linking.openURL(phoneNumber);
                    }
                  })
                  .catch(err => console.log(err));
              }}
            >
              {this.firstArrayElement(listData.phones, 'phones')}
            </TouchableOpacity>
          </View>
        </View>

        <View style={{ flex: 1, paddingTop: 10 }}>
          <View style={{ flex: 4 }}>
            <Text style={[styless.fontPrimary_Bold, styless.textSize13, {}]}>
              Email Address
            </Text>
          </View>
          <View style={{ flex: 4, paddingLeft: 5, paddingTop: 3 }}>
            <TouchableOpacity
              onPress={() => {
                Linking.openURL(
                  'mailto:' +
                    listData.emails[0].email +
                    '?subject=abcdefg&body=body'
                );
              }}
            >
              {this.firstArrayElement(listData.emails, 'emails')}
            </TouchableOpacity>
          </View>
        </View>

        <View style={{ flex: 1, paddingTop: 10 }}>
          <View style={{ flex: 4 }}>
            <Text style={[styless.fontPrimary_Bold, styless.textSize13, {}]}>
              Address
            </Text>
          </View>
          <View style={{ flex: 4, paddingLeft: 5, paddingTop: 3 }}>
            <Text style={[styless.fontPrimary_Regular, styless.textSize15, {}]}>
              {listData.addresses == [] ||
              listData.addresses == null ||
              listData.addresses == ''
                ? 'No addresses'
                : this.firstArrayElement(listData.addresses, 'addresses')}
            </Text>
          </View>
        </View>

        <View style={{ flex: 1, paddingTop: 10 }}>
          <View style={{ flex: 4 }}>
            <Text style={[styless.fontPrimary_Bold, styless.textSize13, {}]}>
              Custem Fields
            </Text>
          </View>
          <View style={{ flex: 4, paddingLeft: 5, paddingTop: 3 }}>
            <Text style={[styless.fontPrimary_Regular, styless.textSize15, {}]}>
              {listData.custom_fields == [] ||
              listData.custom_fields == null ||
              listData.custom_fields == '' ? (
                'No custom_fields'
              ) : (
                <FlatList
                  style={{ flex: 1 }}
                  data={listData.custom_fields}
                  renderItem={({ item, index }) =>
                    this.custom_fieldsList(item, index)
                  }
                  keyExtractor={(item, index) => index.toString()}
                />
              )}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}
export default ContactProfileContactInfo;
