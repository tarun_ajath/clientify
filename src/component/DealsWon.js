import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity
} from 'react-native';
import Icons from '../component/common/Icons';
import styless from '../component/common/styles';

class DealsWon extends Component {
  wonList(item, i) {
    return (
      <TouchableOpacity
        onPress={() =>
          this.props.getDealDetailsData(item.id, this.props.showToast, true)
        }
      >
        <View style={{ marginTop: 15, marginBottom: 10 }}>
          <View
            onStartShouldSetResponder={() => {
              // this.props.navigation.navigate('DealsDetailsScreen');
            }}
            style={{ flexDirection: 'row' }}
          >
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Name
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {item.name}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Contact no.
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {item.name}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Process
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {item.owner}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Close date
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {item.expected_closed_date}
              </Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 4, paddingLeft: 20 }}>
              <Text
                style={[styless.fontPrimary_Regular, styless.textSize13, {}]}
              >
                Amount
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#000'
                  }
                ]}
                numberOfLines={1}
              >
                {item.amount}
              </Text>
            </View>
          </View>

          <View
            style={{
              flexDirection: 'row',
              padding: 10,
              backgroundColor: 'green'
            }}
          >
            <View style={{ flex: 4, paddingLeft: 10, flexDirection: 'row' }}>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'flex-start',
                  paddingRight: 10
                }}
              >
                {Icons.clock_status_open()}
              </View>
              <Text
                style={[
                  styless.fontPrimary_Regular,
                  styless.textSize13,
                  { color: '#fff' }
                ]}
                numberOfLines={1}
              >
                Status
              </Text>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <Text style={styles.textWhite}>:</Text>
            </View>
            <View style={{ flex: 4, paddingRight: 20 }}>
              <Text
                style={[
                  styless.fontPrimary_semiBold,
                  styless.textSize14,
                  {
                    color: '#fff'
                  }
                ]}
                numberOfLines={1}
              >
                {item.status_desc}
              </Text>
            </View>
          </View>

          <View style={{ height: 1, width: '100%', backgroundColor: '#000' }} />
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.props.dealWonList == [] || this.props.dealWonList == '' ? (
          <View
            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
          >
            {Icons.event_blue()}
            <Text style={{ color: 'red', fontSize: 16 }}>No Data</Text>
          </View>
        ) : (
          <FlatList
            data={this.props.dealWonList}
            renderItem={({ item, index }) => this.wonList(item, index)}
            keyExtractor={(item, index) => index.toString()}
          />
        )}
      </View>
    );
  }
}

export default DealsWon;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    color: '#000',
    fontSize: 15,
    fontFamily: 'Raleway-SemiBold'
  },
  textWhite: {
    color: '#fff',
    fontSize: 15,
    fontFamily: 'Raleway-SemiBold'
  }
});
