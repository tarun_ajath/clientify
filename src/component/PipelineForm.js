import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  Dimensions,
  FlatList,
  StyleSheet,
  ActivityIndicator
} from 'react-native';
import InputBox from '../component/common/InputBox';
import { ScrollView } from 'react-native-gesture-handler';
import Container from '../component/common/Container';
import styless from '../component/common/styles';
import HttpClient from '../utilities/HttpClient';
import Icons from '../component/common/Icons';
import { connect } from 'react-redux';
import {
  getPipelineList,
  updatePipelineState
} from '../Actions/pipelineFormAction';

const { height } = Dimensions.get('window');

class PipelineForm extends Component {
  constructor() {
    super();
    this.state = {};
  }

  componentDidMount() {
    this.props.getPipelineList();
  }

  checkForImageOrFirstLetterName(first_Name, i) {
    if (first_Name == '' || first_Name == null) {
    } else if (first_Name != null) {
      const first = this.getFirstLetterOfCompany(first_Name);
      return (
        <View>
          <Text
            style={[
              styles.fontPrimary_monsMedium,
              { color: '#fff', fontSize: 17 }
            ]}
          >
            {first.substr(0, 1)}
          </Text>
        </View>
      );
    } else {
      return;
    }
  }

  splittedName = '';
  getFirstLetterOfCompany(hey) {
    this.splittedName = hey
      .split(/\W/)
      .map(item => {
        for (var i = 0; i < item.length; i++) {
          return item[i];
        }
      })
      .join('');
    firstName = this.splittedName;
    return firstName;
  }

  pipelineList(item, i) {
    return (
      <View
        onStartShouldSetResponder={() => {
          if (this.props.isComeFromModel == true) {
            this.props.closePipelineModel(item);
          } else {
            alert('in Pipeline');
          }
        }}
        style={{ flex: 1, flexDirection: 'row', paddingHorizontal: 10 }}
      >
        <View style={{ flex: 2, padding: 5 }}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#00bfff',
              height: 50,
              width: 50,
              borderRadius: 25
            }}
          >
            {this.checkForImageOrFirstLetterName(item.name, i)}
          </View>
        </View>

        <View
          style={{
            flex: 10,
            alignContent: 'center',
            justifyContent: 'center',
            padding: 5
          }}
        >
          <View style={styles.name}>
            <Text style={[styless.fontPrimary_monsMedium, {}]}>
              {item.name}
            </Text>
          </View>
        </View>
      </View>
    );
  }
  renderPipelineList() {
    console.log('pipeline list :  ', this.props.pipelineFormState);
    const { pipelineFormState } = this.props;
    if (pipelineFormState.isLoadingPipelineList) {
      return (
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
            alignSelf: 'center'
          }}
        >
          <ActivityIndicator size="large" color="#00b2e2" animating={true} />
        </View>
      );
    } else {
      if (
        pipelineFormState.pipelineInputText == '' ||
        !pipelineFormState.pipelineInputText.trim().length
      ) {
        return (
          <FlatList
            data={pipelineFormState.pipelineListArray}
            renderItem={({ item, index }) => this.pipelineList(item, index)}
            keyExtractor={(item, index) => index.toString()}
          />
        );
      } else {
        let updatePipelineList = pipelineFormState.pipelineListArray.filter(
          item => {
            return item['name']
              .toLowerCase()
              .includes(
                pipelineFormState.pipelineInputText.trim().toLowerCase()
              );
          }
        );
        return (
          <FlatList
            data={updatePipelineList}
            renderItem={({ item, index }) => this.pipelineList(item, index)}
            keyExtractor={(item, index) => index.toString()}
          />
        );
      }
    }
  }

  render() {
    const { pipelineFormState } = this.props;
    // console.log(
    //   'pipelineListArray   ' +
    //     JSON.stringify(pipelineFormState.pipelineListArray)
    // );

    // console.log(
    //   'pipelineList   ' +
    //     JSON.stringify(pipelineFormState.isLoadingPipelineList)
    // );
    return (
      <View style={{ flex: 9 }}>
        <View
          style={{
            height: 55,
            marginLeft: 25,
            marginRight: 25,
            paddingTop: 10,
            paddingBottom: 5
          }}
        >
          <InputBox
            placeholder="Search by Pipeline Name"
            rightImageName={Icons.search}
            value={pipelineFormState.pipelineInputText}
            name="pipelineInputText"
            updateLocalState={this.props.updatePipelineState}
          />
        </View>
        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
          <View style={{ flex: 9, paddingTop: 10 }}>
            {this.renderPipelineList()}
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return state;
};
const mapDispatchToProps = dispatch => {
  return {
    getPipelineList: (name, value) => dispatch(getPipelineList(name, value)),
    updatePipelineState: (name, value) =>
      dispatch(updatePipelineState(name, value))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PipelineForm);

const styles = StyleSheet.create({
  name: {
    justifyContent: 'center',
    alignItems: 'flex-start'
    // height: 50
  },
  text: {
    // fontWeight: 'bold'
  },
  textContainer: {
    color: '#000',
    fontWeight: 'bold',
    fontSize: 20
  }
});
