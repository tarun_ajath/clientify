import React, { Component, Fragment } from 'react';
import Toast from './component/common/Toast';
import RootStackNavigator from './navigation/RootStackNavigator';
import ContextProvider from './context/ContextProvider';
// import { Provider } from 'react-redux';
import ReactContext from './context/ReactContext';
import { ScrollView, StatusBar } from 'react-native';
import NavigationService from './utilities/NavigationService';
import ModalComponent from './component/common/ModalComponent';
import { connect } from 'react-redux';
import { updateActionState } from './Actions/AppAction';

class App extends Component {
  render() {
    return (
      <ContextProvider>
        <ReactContext.Consumer>
          {context => (
            <Fragment>
              <StatusBar
                barStyle={'dark-content'}
                backgroundColor={'#00b2e2'}
              />
              <ScrollView contentContainerStyle={{ flex: 1 }}>
                <RootStackNavigator
                  ref={navigatorRef => {
                    NavigationService.setTopLevelNavigator(navigatorRef);
                  }}
                  screenProps={context}
                />
                <Toast context={context} />
                <ModalComponent visible={this.props.appState.appLoading} />
              </ScrollView>
            </Fragment>
          )}
        </ReactContext.Consumer>
      </ContextProvider>
    );
  }
}

const mapStateToProps = state => {
  return state;
};

const mapDispatchToProps = {
  updateActionState
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
