import { createStore, applyMiddleware } from 'redux';
import RootReducer from '../reducers';
import thunk from 'redux-thunk';

export default (Appstore = createStore(RootReducer, applyMiddleware(thunk)));
