import { UPDATE_ADDTASK_STATE } from '../Actions/actionTypes';

const AddTaskReducer = (
  state = {
    name: '',
    due_date: '',
    type: '',
    status: '',
    assigned_to: '',
    deals: [],
    related_companies: [],

    addTaskSubmitResponce: [],
    isLoadingAddTaskData: false
  },
  action
) => {
  switch (action.type) {
    case UPDATE_ADDTASK_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default AddTaskReducer;
