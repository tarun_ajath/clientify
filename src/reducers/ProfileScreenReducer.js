import { UPDATE_PROFILE_STATE } from '../Actions/actionTypes';

const profileScreenReducer = (
  state = {
    isLoadingProfile: null,
    profileListArray: [],
    profileIdOfData: ''
  },
  action
) => {
  switch (action.type) {
    case UPDATE_PROFILE_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default profileScreenReducer;
