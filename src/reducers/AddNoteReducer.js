import { UPDATE_ADDNOTE_STATE } from '../Actions/actionTypes';

const AddNoteReducer = (
  state = {
    name: '',
    comment: '',

    addNoteSubmitResponce: [],
    isLoadingAddNoteData: false
  },
  action
) => {
  switch (action.type) {
    case UPDATE_ADDNOTE_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default AddNoteReducer;
