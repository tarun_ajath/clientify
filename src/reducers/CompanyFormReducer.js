import { UPDATE_COMPANY_STATE } from '../Actions/actionTypes';

const companyFormReducer = (
  state = {
    isLoadingCompanyList: false,
    companyInputText: '',
    companyListArray: [],

    isLoadingCompanyDetails: null,
    companyDetailsListArray: []
  },
  action
) => {
  switch (action.type) {
    case UPDATE_COMPANY_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default companyFormReducer;
