import { combineReducers } from 'redux';
import LoginReducer from './LoginReducer';
import AppReducer from './AppReducer';
import ContactFormReducer from './ContactFormReducer';
import CompanyFormReducer from './CompanyFormReducer';
import DealReducer from './DealReducer';
import ProfileScreenReducer from './ProfileScreenReducer';
import DealDetailsReducer from './DealDetailsReducer';
import ActivitiesReducer from './ActivitiesReducer';
import AddDealReducer from './AddDealReducer';
import AddContactReducer from './AddContactReducer';
import AddCompanyReducer from './AddCompanyReducer';
import AddTaskReducer from './AddTaskReducer';
import AddEventReducer from './AddEventReducer';
import pipelineFormReducer from './pipelineFormReducer';
import PipelineStageFormReducer from './PipelineStageFormReducer';
import CompanySectorReducer from './CompanySectorReducer';
import AddNoteReducer from './AddNoteReducer';
import SettingReducer from './SettingReducer';

export default combineReducers({
  loginState: LoginReducer,
  appState: AppReducer,
  contactFormState: ContactFormReducer,
  companyFormState: CompanyFormReducer,
  dealFormState: DealReducer,
  profileScreenState: ProfileScreenReducer,
  dealDetailsState: DealDetailsReducer,
  activitiesFormState: ActivitiesReducer,
  addDealState: AddDealReducer,
  addContactState: AddContactReducer,
  addCompanyState: AddCompanyReducer,
  addTaskState: AddTaskReducer,
  addEventState: AddEventReducer,
  pipelineFormState: pipelineFormReducer,
  PipelineStageFormState: PipelineStageFormReducer,
  companySectorState: CompanySectorReducer,
  AddNoteState: AddNoteReducer,
  SettingState: SettingReducer
});
