import { UPDATE_COMPANYSECTOR_STATE } from '../Actions/actionTypes';

const CompanySectorReducer = (
  state = {
    isLoadingCompanySectorList: false,
    companySectorInputText: '',
    companySectorListArray: []
  },
  action
) => {
  switch (action.type) {
    case UPDATE_COMPANYSECTOR_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default CompanySectorReducer;
