import { UPDATE_PIPELINE_STATE } from '../Actions/actionTypes';

const pipelineFormReducer = (
  state = {
    isLoadingPipelineList: false,
    pipelineInputText: '',
    pipelineListArray: []
  },
  action
) => {
  switch (action.type) {
    case UPDATE_PIPELINE_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default pipelineFormReducer;
