import { UPDATE_LOGIN_STATE } from '../Actions/actionTypes';

const LoginReducer = (
  state = {
    email: 'mcoderz87@gmail.com',
    password: '12345678',
    loginSubmitResponce: [],
    isLoadingLoginRequest: false
  },
  action
) => {
  switch (action.type) {
    case UPDATE_LOGIN_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default LoginReducer;
