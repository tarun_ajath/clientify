import { UPDATE_PIPELINESTAGE_STATE } from '../Actions/actionTypes';

const PipelineStageFormReducer = (
  state = {
    isLoadingPipelineStageList: false,
    pipelineStageInputText: '',
    pipelineStageListArray: []
  },
  action
) => {
  switch (action.type) {
    case UPDATE_PIPELINESTAGE_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default PipelineStageFormReducer;
