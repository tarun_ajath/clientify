import { UPDATE_CONTACT_STATE } from '../Actions/actionTypes';

const contactFormReducer = (
  state = {
    isLoading: false,
    contactTextInput: '',
    contactListArray: [],

    ImportContactSearchText: '',
    ImportContactsList: []
  },
  action
) => {
  switch (action.type) {
    case UPDATE_CONTACT_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default contactFormReducer;
