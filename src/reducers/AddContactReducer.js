import { UPDATE_ADDCONTACT_STATE } from '../Actions/actionTypes';

const AddContactReducer = (
  state = {
    first_name: '',
    last_name: '',
    email: '',
    phone: '',
    title: '',
    company: [],

    addContactSubmitResponce: [],
    addContactUpdateResponce: [],
    isLoadingAddContactData: false
  },
  action
) => {
  switch (action.type) {
    case UPDATE_ADDCONTACT_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default AddContactReducer;
