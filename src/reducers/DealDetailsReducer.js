import { UPDATE_DEALDETAILS_STATE } from '../Actions/actionTypes';

const dealDetailsReducer = (
  state = {
    isLoadingDealDetails: null,
    dealDetailsArray: [],

    dealIdOfData: ''
  },
  action
) => {
  switch (action.type) {
    case UPDATE_DEALDETAILS_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default dealDetailsReducer;
