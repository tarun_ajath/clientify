import { UPDATE_ADDEVENT_STATE } from "../Actions/actionTypes";

const AddEventReducer = (
  state = {
    name: "",
    location: "",
    from_datetime: "",
    to_datetime: "",
    all_day: false,
    guest_contacts: [],
    deals: [],
    companies: [],

    addEventSubmitResponce: [],
    isLoadingAddEventData: false
  },
  action
) => {
  switch (action.type) {
    case UPDATE_ADDEVENT_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default AddEventReducer;
