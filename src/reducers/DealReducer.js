import { UPDATE_DEAL_STATE } from '../Actions/actionTypes';

const dealFormReducer = (
  state = {
    isLoadingDealList: false,
    dealListArray: [],
    tempData: 'hello'
  },
  action
) => {
  switch (action.type) {
    case UPDATE_DEAL_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default dealFormReducer;
