import { UPDATE_ADDDEAL_STATE } from '../Actions/actionTypes';

const AddDealReducer = (
  state = {
    name: '',
    amount: '',
    selectedContactDetails: [],
    expectedCloseDate: 'YYYY/MM/DD',
    selectedCompanyDetalis: [],
    selectedPipelineDetalis: [],
    selectedPipelineStageDetalis: [],

    addDealSubmitResponce: [],
    isLoadingAddDealData: false
  },
  action
) => {
  switch (action.type) {
    case UPDATE_ADDDEAL_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default AddDealReducer;
