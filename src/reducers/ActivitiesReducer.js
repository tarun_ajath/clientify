import { UPDATE_ACTION_STATE } from '../Actions/actionTypes';

const actionFormReducer = (
  state = {
    isLoadingActionList: false,
    actionListArray: []
  },
  action
) => {
  switch (action.type) {
    case UPDATE_ACTION_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default actionFormReducer;
