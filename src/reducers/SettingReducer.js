import { UPDATE_SETTING_STATE } from '../Actions/actionTypes';

const SettingReducer = (
  state = {
    isLoadingSetting: null,
    UserDetailsArray: []
  },
  action
) => {
  switch (action.type) {
    case UPDATE_SETTING_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default SettingReducer;
