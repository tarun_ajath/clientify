import { UPDATE_ADDCOMPANY_STATE } from '../Actions/actionTypes';

const AddCompanyReducer = (
  state = {
    name: '',
    selectedCompanySectorDetails: '',
    phones: '',
    websites: '',

    addCompanySubmitResponce: [],
    isLoadingAddCompanyData: false
  },
  action
) => {
  switch (action.type) {
    case UPDATE_ADDCOMPANY_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default AddCompanyReducer;
