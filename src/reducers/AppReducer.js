import { UPDATE_APP_STATE } from '../Actions/actionTypes';

const AppReducer = (
  state = {
    appLoading: false,
    isLoggedIn: 0
  },
  action
) => {
  switch (action.type) {
    case UPDATE_APP_STATE: {
      const { name, value } = action.payload;
      const newState = { ...state, [name]: value };
      return newState;
    }
    default:
      return state;
  }
};

export default AppReducer;
