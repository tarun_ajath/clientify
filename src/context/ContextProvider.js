import React, { Component } from 'react';
import ReactContext from './ReactContext';
import HttpClient from '../utilities/HttpClient';

class ContextProvider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toastShow: false,
      toastMessage: '',
      toastColor: ''
    };
  }

  hideToast = () => {
    this.setState({
      toastShow: false
    });
  };

  showToast = (message, color) => {
    this.setState({
      toastShow: true,
      toastMessage: message,
      toastColor: color
    });
  };

  render() {
    const { toastMessage, toastColor, toastShow } = this.state;
    return (
      <ReactContext.Provider
        value={{
          ToastProvider: {
            toastMessage,
            toastColor,
            toastShow,
            hideToast: this.hideToast,
            showToast: this.showToast
          }
        }}
      >
        {this.props.children}
      </ReactContext.Provider>
    );
  }
}
export default ContextProvider;
