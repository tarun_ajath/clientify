import { UPDATE_APP_STATE } from './actionTypes';

export const updateActionState = (name, value) => ({
  type: UPDATE_APP_STATE,
  payload: { name, value }
});
