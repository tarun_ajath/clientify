import { UPDATE_ADDTASK_STATE } from './actionTypes';
import { UPDATE_APP_STATE } from './actionTypes';
import HttpClient from '../utilities/HttpClient';
import NavigationService from '../utilities/NavigationService';

export const addTaskSubmit = (
  reqBody,
  showToast,
  isComeFrom
) => async dispatch => {
  console.log('tsk rq body', reqBody);
  try {
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: true }
    });
    let responseData = await HttpClient.addTask(reqBody);
    console.log('Hello/ addTask///', responseData);
    let value = responseData.data;
    let name = 'addTaskSubmitResponce';

    dispatch({
      type: UPDATE_ADDTASK_STATE,
      payload: { name, value }
    });
    console.log('isComeFromDeal : ' + isComeFrom);
    if (responseData.status == 200) {
      showToast('You have add the Deal', 'success');
      if (isComeFrom == 'profile') {
        NavigationService.navigate('Profile');
      } else if (isComeFrom == 'activity') {
        NavigationService.navigate('ActivitiesForm');
      }
    } else {
      showToast('something went wrong', 'error');
    }

    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  } catch (error) {
    console.log('in addTask form error response ', error);
    console.log('in addTask form error ', error.response);
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  }
};

export const updateAddTaskState = (name, value) => ({
  type: UPDATE_ADDTASK_STATE,
  payload: { name, value }
});
