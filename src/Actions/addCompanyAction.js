import { UPDATE_ADDCOMPANY_STATE } from './actionTypes';
import { UPDATE_APP_STATE } from './actionTypes';
import HttpClient from '../utilities/HttpClient';
import NavigationService from '../utilities/NavigationService';

export const addCompanySubmit = (requestBody, showToast) => async dispatch => {
  try {
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: true }
    });
    let responseData = await HttpClient.addCompany(JSON.stringify(requestBody));

    console.log('Hello/ addCompany///' + JSON.stringify(responseData));
    let value = responseData.data;
    let name = 'addCompanySubmitResponce';

    dispatch({
      type: UPDATE_ADDCOMPANY_STATE,
      payload: { name, value }
    });
    if (responseData.status == 201) {
      showToast('You have add the company', 'success');
      NavigationService.navigate('ContactForm');
      // alert('You have add the company');
    } else {
      showToast('something went wrong', 'error');
    }

    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  } catch (error) {
    console.log('in addCompany form error ', error.response);
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  }
};

export const addCompanyUpdate = (requestBody, companyId) => async dispatch => {
  console.log('reqbody', requestBody);
  console.log('reqbody id', companyId);
  try {
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: true }
    });
    let responseData = await HttpClient.addCompanyUpdate(
      JSON.stringify(requestBody),
      companyId
    );

    console.log('Hello/ updateCompany///', responseData);
    let value = responseData.data;
    let name = 'addCompanySubmitResponce';

    dispatch({
      type: UPDATE_ADDCOMPANY_STATE,
      payload: { name, value }
    });
    if (responseData.status == 201) {
      showToast('You have add the company', 'success');
      NavigationService.navigate('CompanyContactDetails');
      // alert('You have add the company');
    } else {
      showToast('something went wrong', 'error');
    }

    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  } catch (error) {
    console.log('in updateCompany form error ', error.response);
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  }
};

export const updateAddCompanyState = (name, value) => ({
  type: UPDATE_ADDCOMPANY_STATE,
  payload: { name, value }
});
