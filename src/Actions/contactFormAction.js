import { UPDATE_CONTACT_STATE } from './actionTypes';
import HttpClient from '../utilities/HttpClient';

export const getContactList = (pageNo, oldData) => async dispatch => {
  try {
    dispatch({
      type: UPDATE_CONTACT_STATE,
      payload: { name: 'isLoading', value: true }
    });
    let responseData = await HttpClient.userContactList(pageNo);
    // console.log('Hello contactList////', responseData);
    let value = [...oldData, ...responseData.data.results];
    let name = 'contactListArray';

    dispatch({
      type: UPDATE_CONTACT_STATE,
      payload: { name, value }
    });

    dispatch({
      type: UPDATE_CONTACT_STATE,
      payload: { name: 'isLoading', value: false }
    });
  } catch (error) {
    // console.log('in contact form error ', error.response);
    dispatch({
      type: UPDATE_CONTACT_STATE,
      payload: { name: 'isLoading', value: false }
    });
  }
};

export const updateContactState = (name, value) => ({
  type: UPDATE_CONTACT_STATE,
  payload: { name, value }
});
