import { UPDATE_ACTION_STATE } from './actionTypes';
import HttpClient from '../utilities/HttpClient';

export const getActionList = () => async dispatch => {
  try {
    dispatch({
      type: UPDATE_ACTION_STATE,
      payload: { name: 'isLoadingActionList', value: true }
    });
    let responseData = await HttpClient.action();
    // console.log('Hello/ action///' + JSON.stringify(responseData));
    let value = responseData.data.results;
    let name = 'actionListArray';

    dispatch({
      type: UPDATE_ACTION_STATE,
      payload: { name, value }
    });

    dispatch({
      type: UPDATE_ACTION_STATE,
      payload: { name: 'isLoadingActionList', value: false }
    });
  } catch (error) {
    console.log('in action form error ' + error);
    dispatch({
      type: UPDATE_ACTION_STATE,
      payload: { name: 'isLoadingActionList', value: false }
    });
  }
};

export const updateActionState = (name, value) => ({
  type: UPDATE_ACTION_STATE,
  payload: { name, value }
});
