import { UPDATE_PIPELINESTAGE_STATE } from './actionTypes';
import HttpClient from '../utilities/HttpClient';

export const getPipelineStageList = () => async dispatch => {
  try {
    dispatch({
      type: UPDATE_PIPELINESTAGE_STATE,
      payload: { name: 'isLoadingPipelineStageList', value: true }
    });
    let responseData = await HttpClient.userPipelineStageList();
    console.log('Hello/ pipelineStage///', responseData);
    let value = responseData.data.results;
    let name = 'pipelineStageListArray';

    dispatch({
      type: UPDATE_PIPELINESTAGE_STATE,
      payload: { name, value }
    });

    dispatch({
      type: UPDATE_PIPELINESTAGE_STATE,
      payload: { name: 'isLoadingPipelineStageList', value: false }
    });
  } catch (error) {
    console.log('in pipelineStage form error ' + error);
    dispatch({
      type: UPDATE_PIPELINESTAGE_STATE,
      payload: { name: 'isLoadingPipelineStageList', value: false }
    });
  }
};

export const updatePipelineStageState = (name, value) => ({
  type: UPDATE_PIPELINESTAGE_STATE,
  payload: { name, value }
});
