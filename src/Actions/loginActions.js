import { UPDATE_LOGIN_STATE } from './actionTypes';
import HttpClient from '../utilities/HttpClient';
import NavigationService from '../utilities/NavigationService';
import DataStore from '../utilities/DataStore';

export const loginReqSubmit = () => async dispatch => {
  const reqBody = {
    username: 'mcoderz87@gmail.com',
    password: '12345678'
  };

  try {
    dispatch({
      type: UPDATE_LOGIN_STATE,
      payload: { name: 'isLoadingLoginRequest', value: true }
    });
    let responseData = await HttpClient.loginSubmit(JSON.stringify(reqBody));
    // console.log('Hello/ login Token is  ///', responseData.data.token);

    await DataStore.store('token', JSON.stringify(responseData.data.token));
    DataStore.token = responseData.data.token;
    // await DataStore.store('token', 'a981d9f9f4519c2023ec990cf8bcdf962fd5fd94');
    // DataStore.token = 'a981d9f9f4519c2023ec990cf8bcdf962fd5fd94';
    let value = responseData.data;
    let name = 'loginSubmitResponce';

    dispatch({
      type: UPDATE_LOGIN_STATE,
      payload: { name, value }
    });

    if (responseData.status == 200) {
      NavigationService.navigate('Home');
      // alert('You have successfully logIn');
    } else {
      alert('something went wrong');
    }

    dispatch({
      type: UPDATE_LOGIN_STATE,
      payload: { name: 'isLoadingLoginRequest', value: false }
    });
  } catch (error) {
    console.log('in Login form error ', error);
    dispatch({
      type: UPDATE_LOGIN_STATE,
      payload: { name: 'isLoadingLoginRequest', value: false }
    });
  }
};

export const updateLoginState = (name, value) => ({
  type: UPDATE_LOGIN_STATE,
  payload: { name, value }
});
