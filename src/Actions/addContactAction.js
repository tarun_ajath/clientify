import { UPDATE_ADDCONTACT_STATE } from './actionTypes';
import { UPDATE_APP_STATE } from './actionTypes';
import HttpClient from '../utilities/HttpClient';
import NavigationService from '../utilities/NavigationService';

export const addContactSubmit = (requestBody, showToast) => async dispatch => {
  try {
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: true }
    });
    let responseData = await HttpClient.addContact(requestBody);
    console.log('Hello/ addContact///', responseData);
    let value = responseData.data;
    let name = 'addContactSubmitResponce';

    dispatch({
      type: UPDATE_ADDCONTACT_STATE,
      payload: { name, value }
    });
    if (responseData.status == 201) {
      showToast('You have add the contact', 'success');
      NavigationService.navigate('ContactForm');
      // alert('You have add the Contact');
    } else {
      showToast('something went wrong', 'error');
    }

    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  } catch (error) {
    console.log('in addContact form error ', error.response);
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  }
};

export const addContactUpdate = (
  requestBody,
  contactId,
  showToast
) => async dispatch => {
  try {
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: true }
    });
    let responseData = await HttpClient.addContactUpdate(
      requestBody,
      contactId
    );
    console.log('Hello/ addContactUpdate///', responseData);
    let value = responseData.data;
    let name = 'addContactUpdateResponce';

    dispatch({
      type: UPDATE_ADDCONTACT_STATE,
      payload: { name, value }
    });
    if (responseData.status == 200) {
      showToast('You have Update the contact', 'success');
      NavigationService.navigate('Profile');
    } else {
      showToast('something went wrong', 'error');
    }

    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  } catch (error) {
    console.log('in addContactUpdate form error ', error.response);
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  }
};

export const updateAddContactState = (name, value) => ({
  type: UPDATE_ADDCONTACT_STATE,
  payload: { name, value }
});
