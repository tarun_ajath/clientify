import { UPDATE_ADDNOTE_STATE } from './actionTypes';
import { UPDATE_APP_STATE } from './actionTypes';
import HttpClient from '../utilities/HttpClient';
import NavigationService from '../utilities/NavigationService';

export const addNoteSubmit = (
  relatedId,
  reqBody,
  showToast,
  isComeFromContact
) => async dispatch => {
  // console.log('req body note ', reqBody);
  try {
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: true }
    });

    let responseData = await HttpClient.addNote(
      relatedId,
      reqBody,
      isComeFromContact
    );
    console.log('Hello/ addNote///', responseData);
    let value = responseData.data;
    let name = 'addNoteSubmitResponce';

    dispatch({
      type: UPDATE_ADDNOTE_STATE,
      payload: { name, value }
    });
    if (responseData.status == 200) {
      showToast('You have add the Note', 'success');
      if (isComeFromContact) {
        NavigationService.navigate('Profile');
      } else {
        NavigationService.navigate('DealsDetailsScreen');
      }
    } else {
      showToast('something went wrong', 'error');
    }

    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  } catch (error) {
    console.log('in addNote form error ', error.response, error);
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  }
};

export const updateAddNoteState = (name, value) => ({
  type: UPDATE_ADDNOTE_STATE,
  payload: { name, value }
});
