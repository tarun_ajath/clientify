import { UPDATE_DEALDETAILS_STATE } from './actionTypes';
import { UPDATE_APP_STATE } from './actionTypes';
import HttpClient from '../utilities/HttpClient';
import NavigationService from '../utilities/NavigationService';

export const getDealDetailsData = (
  dealId,
  showToast,
  isComeFromDeal
) => async dispatch => {
  try {
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: true }
    });
    let responseData = await HttpClient.dealDetailsData(dealId);
    console.log('Hello/ DealDetailsssss///', responseData);

    let dealdetailsArr = responseData.data;

    if (responseData.status == 200) {
      let wallEntries = dealdetailsArr.wall_entries;
      for (let i = 0; i < Object.keys(wallEntries).length; i++) {
        let ListArray = JSON.parse(wallEntries[i].extra);
        let timlinefilterJson = this.filterDataForTimelineProfile(ListArray);
        dealdetailsArr.wall_entries[i].extraArray = timlinefilterJson;
      }
      let value = dealdetailsArr;
      let name = 'dealDetailsArray';
      dispatch({
        type: UPDATE_DEALDETAILS_STATE,
        payload: { name, value }
      });

      dispatch({
        type: UPDATE_DEALDETAILS_STATE,
        payload: { name: 'dealIdOfData', value: dealId }
      });
      console.log('isComeFromDeal dealdetails : ', isComeFromDeal);
      // showToast('Deal Details page', 'success');
      NavigationService.navigate('DealsDetailsScreen', {
        isComeFromDeal: true
      });
    } else {
      showToast('something went wrong', 'error');
    }

    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  } catch (error) {
    // console.log('in DealDetails form errorrrrrr ' + error);
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  }
};

export const updateDealDetialsData = (name, value) => ({
  type: UPDATE_DEALDETAILS_STATE,
  payload: { name, value }
});
