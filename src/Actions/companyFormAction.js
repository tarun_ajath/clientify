import { UPDATE_COMPANY_STATE } from './actionTypes';
import { UPDATE_APP_STATE } from './actionTypes';
import HttpClient from '../utilities/HttpClient';
import NavigationService from '../utilities/NavigationService';

export const getCompanyList = (pageNo, oldData) => async dispatch => {
  try {
    dispatch({
      type: UPDATE_COMPANY_STATE,
      payload: { name: 'isLoadingCompanyList', value: true }
    });
    let responseData = await HttpClient.userCompanyList(pageNo);
    console.log('Hello/ company///', responseData);
    let value = [...oldData, ...responseData.data.results];
    let name = 'companyListArray';

    dispatch({
      type: UPDATE_COMPANY_STATE,
      payload: { name, value }
    });

    dispatch({
      type: UPDATE_COMPANY_STATE,
      payload: { name: 'isLoadingCompanyList', value: false }
    });
  } catch (error) {
    console.log('in company form error ' + error);
    dispatch({
      type: UPDATE_COMPANY_STATE,
      payload: { name: 'isLoadingCompanyList', value: false }
    });
  }
};

export const getCompanyDetails = companyId => async dispatch => {
  try {
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: true }
    });
    let responseData = await HttpClient.companyDetails(companyId);
    // console.log('Hello/ companyDetails///', responseData);

    let value = responseData.data;
    let name = 'companyDetailsListArray';
    dispatch({
      type: UPDATE_COMPANY_STATE,
      payload: { name, value }
    });
    if (responseData.status == 200) {
      NavigationService.navigate('CompanyContactDetails');
    } else {
      alert('something went wrong');
    }

    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  } catch (error) {
    console.log('in companyDetails form error ' + error);
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  }
};

export const deleteCompany = companyId => async dispatch => {
  // console.log('id is   ', companyId);
  try {
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
    let responseData = await HttpClient.deleteCompany(companyId);

    // console.log('Hello/ addCompany///', responseData);
    // let value = responseData;
    // let name = 'addCompanySubmitResponce';

    // dispatch({
    //   type: UPDATE_ADDCOMPANY_STATE,
    //   payload: { name, value }
    // });
    if (responseData.status == 204) {
      alert('Company deleted.');
      NavigationService.navigate('ContactForm');
    } else {
      alert('You have already deleted.');
    }

    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  } catch (error) {
    console.log('in addCompany form error ', error.response);
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  }
};

export const updateCompanyState = (name, value) => ({
  type: UPDATE_COMPANY_STATE,
  payload: { name, value }
});
