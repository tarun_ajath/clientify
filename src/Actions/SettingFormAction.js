import { UPDATE_SETTING_STATE } from './actionTypes';
import { UPDATE_APP_STATE } from './actionTypes';
import HttpClient from '../utilities/HttpClient';

export const getUserInfo = () => async dispatch => {
  try {
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: true }
    });
    let responseData = await HttpClient.UserInfo();
    console.log('Hello/ Setting///', responseData);
    let value = responseData.data.results[0];
    let name = 'UserDetailsArray';

    dispatch({
      type: UPDATE_SETTING_STATE,
      payload: { name, value }
    });

    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  } catch (error) {
    console.log('in setting form error ' + error);
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  }
};

export const updateSettingState = (name, value) => ({
  type: UPDATE_SETTING_STATE,
  payload: { name, value }
});
