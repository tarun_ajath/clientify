import { UPDATE_ADDDEAL_STATE } from './actionTypes';
import { UPDATE_APP_STATE } from './actionTypes';
import HttpClient from '../utilities/HttpClient';
import NavigationService from '../utilities/NavigationService';

export const addDealDataSubmit = (requestBody, showToast) => async dispatch => {
  try {
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: true }
    });
    let responseData = await HttpClient.addDeal(requestBody);
    // console.log('Hello/ AddDeal///' + JSON.stringify(responseData));
    let value = responseData.data;
    let name = 'addDealSubmitResponce';

    dispatch({
      type: UPDATE_ADDDEAL_STATE,
      payload: { name, value }
    });
    if (responseData.status == 201) {
      showToast('You have add the Deal', 'success');
      NavigationService.navigate('DealsForm');
    } else {
      showToast('something went wrong', 'error');
    }

    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  } catch (error) {
    console.log('in addDeal form error ', error.response);
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  }
};

export const addDealUpdateSubmit = (
  requestBody,
  dealId,
  showToast
) => async dispatch => {
  try {
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: true }
    });
    let responseData = await HttpClient.addDealUpdate(requestBody, dealId);
    console.log('Hello/ AddDealUpdate///', responseData);
    let value = responseData.data;
    let name = 'addDealSubmitResponce';

    dispatch({
      type: UPDATE_ADDDEAL_STATE,
      payload: { name, value }
    });
    if (responseData.status == 200) {
      showToast('You have add the Deal', 'success');
      // NavigationService.navigate('DealsForm');
    } else {
      showToast('something went wrong', 'error');
    }

    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  } catch (error) {
    console.log('in addDealUpdate form error ', error.response);
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  }
};

export const updateAddDeaState = (name, value) => ({
  type: UPDATE_ADDDEAL_STATE,
  payload: { name, value }
});
