import { UPDATE_ADDEVENT_STATE } from './actionTypes';
import { UPDATE_APP_STATE } from './actionTypes';
import HttpClient from '../utilities/HttpClient';
import NavigationService from '../utilities/NavigationService';

export const addEventSubmit = (
  reqBody,
  showToast,
  isComeFromDeal
) => async dispatch => {
  try {
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: true }
    });
    let responseData = await HttpClient.addEvents(JSON.stringify(reqBody));
    // console.log('Hello/ addEVENTS///', responseData);
    let value = responseData.data;
    let name = 'addEventSubmitResponce';

    dispatch({
      type: UPDATE_ADDEVENT_STATE,
      payload: { name, value }
    });
    if (responseData.status == 201) {
      showToast('You have add the event', 'success');
      console.log('isComeFromDeal : ' + isComeFromDeal);
      if (isComeFromDeal == 'deal') {
        NavigationService.navigate('DealsDetailsScreen');
      } else if (isComeFromDeal == 'profile') {
        NavigationService.navigate('Profile');
      } else if (isComeFromDeal == 'company') {
        NavigationService.navigate('CompanyContactDetails');
      } else if (isComeFromDeal == 'activity') {
        NavigationService.navigate('ActivitiesForm');
      }
    } else {
      showToast('something went wrong', 'error');
    }

    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  } catch (error) {
    console.log('in addEvent form error ', error);
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  }
};

export const updateAddEventState = (name, value) => ({
  type: UPDATE_ADDEVENT_STATE,
  payload: { name, value }
});
