import { UPDATE_PROFILE_STATE } from './actionTypes';
import { UPDATE_APP_STATE } from './actionTypes';
import HttpClient from '../utilities/HttpClient';
import NavigationService from '../utilities/NavigationService';

export const getProfileDetails = contactId => async dispatch => {
  try {
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: true }
    });
    let responseData = await HttpClient.contactProfileData(contactId);
    console.log('Hello/ profile///', responseData);
    let profileArr = responseData.data;
    if (responseData.status == 200) {
      let wallEntries = profileArr.wall_entries;
      for (let i = 0; i < Object.keys(wallEntries).length; i++) {
        let ListArray = JSON.parse(wallEntries[i].extra);
        let timlinefilterJson = this.filterDataForTimelineProfile(ListArray);
        profileArr.wall_entries[i].extraArray = timlinefilterJson;
      }
      let value = profileArr;
      let name = 'profileListArray';
      dispatch({
        type: UPDATE_PROFILE_STATE,
        payload: { name, value }
      });
      dispatch({
        type: UPDATE_PROFILE_STATE,
        payload: { name: 'profileIdOfData', value: contactId }
      });
      NavigationService.navigate('Profile');
    } else {
      alert('something went wrong');
    }
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  } catch (error) {
    console.log('in profile form error ', error);
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  }
};

filterDataForTimelineProfile = ListArray => {
  if (ListArray == null) {
  } else {
    let newListArray = [];
    Object.keys(ListArray).forEach(key => {
      newListArray.push({ key: key, value: ListArray[key] });
    });
    // console.log(
    //   'new array of timeline are now ' + JSON.stringify(newListArray)
    // );
    return newListArray;
  }
};

export const deleteContactProfile = contactId => async dispatch => {
  try {
    dispatch({
      type: UPDATE_PROFILE_STATE,
      payload: { name: 'isLoadingProfile', value: true }
    });
    let responseData = await HttpClient.deleteContactProfile(contactId);
    // console.log('Hello/ profile delete ///', responseData);

    if (responseData.status == 204) {
      alert('Contact deleted.');
      NavigationService.navigate('ContactForm');
    } else if (responseData.status == 404) {
      alert('You have already deleted.');
    }

    dispatch({
      type: UPDATE_PROFILE_STATE,
      payload: { name: 'isLoadingProfile', value: false }
    });
  } catch (error) {
    console.log('in profiledelete form error ', error.response);
    dispatch({
      type: UPDATE_PROFILE_STATE,
      payload: { name: 'isLoadingProfile', value: false }
    });
  }
};

export const updateProfileState = (name, value) => ({
  type: UPDATE_PROFILE_STATE,
  payload: { name, value }
});
