import { UPDATE_DEAL_STATE } from './actionTypes';
import { UPDATE_APP_STATE } from './actionTypes';
import HttpClient from '../utilities/HttpClient';

export const getDealList = () => async dispatch => {
  try {
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: true }
    });
    let responseData = await HttpClient.deals();
    // console.log('Hello/ Deal///' + JSON.stringify(responseData));
    let value = responseData.data.results;
    let name = 'dealListArray';

    dispatch({
      type: UPDATE_DEAL_STATE,
      payload: { name, value }
    });

    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  } catch (error) {
    // console.log('in deals form error ' + error);
    dispatch({
      type: UPDATE_APP_STATE,
      payload: { name: 'appLoading', value: false }
    });
  }
};

export const updateDealState = (name, value) => ({
  type: UPDATE_DEAL_STATE,
  payload: { name, value }
});
