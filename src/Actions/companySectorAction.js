import { UPDATE_COMPANYSECTOR_STATE } from './actionTypes';
import HttpClient from '../utilities/HttpClient';

export const getCompanySectorList = () => async dispatch => {
  try {
    dispatch({
      type: UPDATE_COMPANYSECTOR_STATE,
      payload: { name: 'isLoadingCompanySectorList', value: true }
    });
    let responseData = await HttpClient.loadCompanySectors();
    // console.log('Hello/ companySector//', responseData);
    let value = responseData.data.results;
    let name = 'companySectorListArray';

    dispatch({
      type: UPDATE_COMPANYSECTOR_STATE,
      payload: { name, value }
    });

    dispatch({
      type: UPDATE_COMPANYSECTOR_STATE,
      payload: { name: 'isLoadingCompanySectorList', value: false }
    });
  } catch (error) {
    console.log('in companySector form error ' + error);
    dispatch({
      type: UPDATE_COMPANYSECTOR_STATE,
      payload: { name: 'isLoadingCompanySectorList', value: false }
    });
  }
};

export const updateCompanySectorState = (name, value) => ({
  type: UPDATE_COMPANYSECTOR_STATE,
  payload: { name, value }
});
