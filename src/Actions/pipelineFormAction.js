import { UPDATE_PIPELINE_STATE } from './actionTypes';
import HttpClient from '../utilities/HttpClient';

export const getPipelineList = () => async dispatch => {
  try {
    dispatch({
      type: UPDATE_PIPELINE_STATE,
      payload: { name: 'isLoadingPipelineList', value: true }
    });
    let responseData = await HttpClient.userPipelineList();
    console.log('Hello/ pipeline///', responseData);
    let value = responseData.data.results;
    let name = 'pipelineListArray';

    dispatch({
      type: UPDATE_PIPELINE_STATE,
      payload: { name, value }
    });

    dispatch({
      type: UPDATE_PIPELINE_STATE,
      payload: { name: 'isLoadingPipelineList', value: false }
    });
  } catch (error) {
    console.log('in pipeline form error ' + error);
    dispatch({
      type: UPDATE_PIPELINE_STATE,
      payload: { name: 'isLoadingPipelineList', value: false }
    });
  }
};

export const updatePipelineState = (name, value) => ({
  type: UPDATE_PIPELINE_STATE,
  payload: { name, value }
});
