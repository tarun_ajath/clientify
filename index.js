import React from 'react';
import { AppRegistry } from 'react-native';
import App from './src/App';
import { name as appName } from './app.json';
import { Provider as PaperProvider } from 'react-native-paper';
import ThemeHandler from './src/utilities/ThemeHandler';
import Appstore from './src/store';
import { Provider } from 'react-redux';

const clientify = () => (
  <Provider store={Appstore}>
    <PaperProvider theme={ThemeHandler.theme}>
      <App />
    </PaperProvider>
  </Provider>
);

AppRegistry.registerComponent(appName, () => clientify);
